// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_session_proxy.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserSessionProxy _$UserSessionProxyFromJson(Map<String, dynamic> json) =>
    UserSessionProxy(
      userId: json['userId'] as int,
      clientId: json['clientId'] as int?,
      token: json['token'] as String?,
      isAdmin: json['isAdmin'] as bool?,
      siteId: json['siteId'] as int?,
      eMail: json['eMail'] as String? ?? "",
      staySignIn: json['staySignIn'] as bool?,
    )..status = json['status'];

Map<String, dynamic> _$UserSessionProxyToJson(UserSessionProxy instance) =>
    <String, dynamic>{
      'status': instance.status,
      'userId': instance.userId,
      'clientId': instance.clientId,
      'token': instance.token,
      'isAdmin': instance.isAdmin,
      'siteId': instance.siteId,
      'staySignIn': instance.staySignIn,
      'eMail': instance.eMail,
    };
