import 'package:json_annotation/json_annotation.dart';

part 'user_session.g.dart';

@JsonSerializable()
class UserSession {
  int? userId;
  int? clientId;
  bool? isAdmin;
  bool? staySignIn;
  double? lastRequestTime;
  int? userType;
  Map<String, String>? permissions;

  //Application wise variables
  double? kriyaExpiryDate;
  bool? isActiveKriya;

  double? greenboxExpiryDate;
  bool? isActiveGreenbox;

  // These are all variable names in userSession which are used in setting and getting
  static String constUserId = "userId";
  static String constClientId = "clientId";
  static String constIsAdmin = "isAdmin";
  static String constUserType = "userType";
  static String constPermissions = "permissions";
  static String constStaySignIn = "staySignIn";
  static String constIsActiveAdmin = "isActiveAdmin";
  static String constIsActiveKriya = "isActiveKriya";
  static String constIsActiveGreenbox = "isActiveGreenbox";
  static String consKriyaExpiryDate = "kriyaExpiryDate";
  static String constGreenboxExpiryDate = "greenboxExpiryDate";

  // Json
  factory UserSession.fromJson(Map<String, dynamic> json) =>
      _$UserSessionFromJson(json);
  Map<String, dynamic> toJson() => _$UserSessionToJson(this);

  UserSession(
      {this.userId,
        this.clientId,
        this.isAdmin,
        this.staySignIn,
        this.lastRequestTime,
        this.userType,
        this.permissions,
        this.kriyaExpiryDate,
        this.isActiveKriya,
        this.greenboxExpiryDate,
        this.isActiveGreenbox});
}
