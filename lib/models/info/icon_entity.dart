import 'package:flutter/material.dart';

class IconDetails {
  String? iconValue;
  String? title;
  int? colourValue;
  String? type;
  bool select = false;

  IconDetails({
    required this.iconValue,
    this.title,
    this.colourValue,
    this.type,
  });

  IconData toIconData() {
    return IconData(
      int.parse(iconValue!),
      fontFamily: "MaterialIcons",
    );
  }
}

class IconType {
  List<IconDetails> icons;
  String iconType;

  IconType({
    required this.icons,
    required this.iconType,
  });
}
