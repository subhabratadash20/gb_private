import 'package:flutter/material.dart';
import 'package:gb_private/models/discus_form/discus_form_element.dart';
import 'package:json_annotation/json_annotation.dart';

part 'category.g.dart';

@JsonSerializable()
class Category {
  int id = -1;
  String? icon;
  int? colorValue;
  int? iconType;
  String name = '';
  String? date;
  bool isFavourite = false;

  List<DiscusFormElement>? fieldList;
  String? subTitle = '';
  String? imagePath = '';

  @JsonKey(includeFromJson: false, includeToJson: false)
  List<Category>? categoryItemList;

  @JsonKey(includeFromJson: false, includeToJson: false)
  Category? parentCategory;

  bool isEditCategory = false;

  // Json
  factory Category.fromJson(Map<String, dynamic> json) =>
      _$CategoryFromJson(json);

  Map<String, dynamic> toJson() => _$CategoryToJson(this);

  IconData? toIconData() {
    if (icon == null) return null;
    return IconData(int.parse(icon!), fontFamily: "MaterialIcons");
  }

  Category(
      {required this.id,
      this.icon,
      this.colorValue,
      required this.name,
      this.date,
      this.isFavourite = false,
      this.fieldList,
      this.categoryItemList,
      this.subTitle = '',
      this.imagePath});
}

enum IconTypeEnum { materialIcon, svgIcon, imageIcon }
