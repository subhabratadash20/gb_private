// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Category _$CategoryFromJson(Map<String, dynamic> json) => Category(
      id: json['id'] as int,
      icon: json['icon'] as String?,
      colorValue: json['colorValue'] as int?,
      name: json['name'] as String,
      date: json['date'] as String?,
      isFavourite: json['isFavourite'] as bool? ?? false,
      fieldList: (json['fieldList'] as List<dynamic>?)
          ?.map((e) => DiscusFormElement.fromJson(e as Map<String, dynamic>))
          .toList(),
      subTitle: json['subTitle'] as String? ?? '',
      imagePath: json['imagePath'] as String?,
    )
      ..iconType = json['iconType'] as int?
      ..isEditCategory = json['isEditCategory'] as bool;

Map<String, dynamic> _$CategoryToJson(Category instance) => <String, dynamic>{
      'id': instance.id,
      'icon': instance.icon,
      'colorValue': instance.colorValue,
      'iconType': instance.iconType,
      'name': instance.name,
      'date': instance.date,
      'isFavourite': instance.isFavourite,
      'fieldList': instance.fieldList,
      'subTitle': instance.subTitle,
      'imagePath': instance.imagePath,
      'isEditCategory': instance.isEditCategory,
    };
