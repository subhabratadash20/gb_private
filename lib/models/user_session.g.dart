// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_session.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserSession _$UserSessionFromJson(Map<String, dynamic> json) => UserSession(
      userId: json['userId'] as int?,
      clientId: json['clientId'] as int?,
      isAdmin: json['isAdmin'] as bool?,
      staySignIn: json['staySignIn'] as bool?,
      lastRequestTime: (json['lastRequestTime'] as num?)?.toDouble(),
      userType: json['userType'] as int?,
      permissions: (json['permissions'] as Map<String, dynamic>?)?.map(
        (k, e) => MapEntry(k, e as String),
      ),
      kriyaExpiryDate: (json['kriyaExpiryDate'] as num?)?.toDouble(),
      isActiveKriya: json['isActiveKriya'] as bool?,
      greenboxExpiryDate: (json['greenboxExpiryDate'] as num?)?.toDouble(),
      isActiveGreenbox: json['isActiveGreenbox'] as bool?,
    );

Map<String, dynamic> _$UserSessionToJson(UserSession instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'clientId': instance.clientId,
      'isAdmin': instance.isAdmin,
      'staySignIn': instance.staySignIn,
      'lastRequestTime': instance.lastRequestTime,
      'userType': instance.userType,
      'permissions': instance.permissions,
      'kriyaExpiryDate': instance.kriyaExpiryDate,
      'isActiveKriya': instance.isActiveKriya,
      'greenboxExpiryDate': instance.greenboxExpiryDate,
      'isActiveGreenbox': instance.isActiveGreenbox,
    };
