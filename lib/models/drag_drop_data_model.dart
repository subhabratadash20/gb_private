import 'dart:convert';

import 'package:flutter/material.dart';

class DragDropData {
  final String imagePath;
  String name;
  final bool editable;
  bool isEditing;
  bool isObscure;
  String type;
  final TextEditingController controller;

  DragDropData({
    this.isObscure = false,
    this.isEditing = false,
    this.type = "text",
    required this.controller,
    required this.imagePath,
    required this.name,
    this.editable = true,
  });

  factory DragDropData.fromMap(Map<String, dynamic> map) {
    return DragDropData(
      imagePath: map['imagePath'] ?? '',
      name: map['name'] ?? '',
      editable: map['editable'] ?? false,
      isEditing: map['isEditing'] ?? false,
      isObscure: map['isObscure'] ?? false,
      type: map['type'] ?? '',
      controller: TextEditingController(text: map['controller']),
    );
  }

  // String toJson() => json.encode(toMap());

  factory DragDropData.fromJson(String source) =>
      DragDropData.fromMap(json.decode(source));
}
