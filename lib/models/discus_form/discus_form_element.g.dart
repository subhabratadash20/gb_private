// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'discus_form_element.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DiscusFormElement _$DiscusFormElementFromJson(Map<String, dynamic> json) =>
    DiscusFormElement(
      type: json['type'] as String,
      key: json['key'] as String? ?? "",
      dateFormat: json['dateFormat'] as String?,
      id: json['id'] as int? ?? -1,
      width: json['width'] as String? ?? "Half",
      widthOptions: (json['widthOptions'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      order: json['order'] as int? ?? -1,
      seqNo: json['seqNo'] as int?,
      placeholderText: json['placeholderText'] as String?,
      fieldLabel: json['fieldLabel'] as String?,
      fieldValue: json['fieldValue'],
      fileData: json['fileData'] as String?,
      helperText: json['helperText'] as String?,
      toolTip: json['toolTip'] as String?,
      fieldEditable: json['fieldEditable'] as bool?,
      active: json['active'] as bool? ?? false,
      hidden: json['hidden'] as bool? ?? false,
      isEnabled: json['isEnabled'] as bool? ?? false,
      required: json['required'] as bool? ?? false,
      backGroundColour: json['backGroundColour'] as String? ?? "",
      showLabel: json['showLabel'] as bool? ?? true,
      colour: json['colour'] as String? ?? "",
      align: json['align'] as String? ?? "",
      alignPossibleOptions: (json['alignPossibleOptions'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      fontSize: json['fontSize'] as int?,
      fontStyleValue: json['fontStyleValue'] as List<dynamic>?,
      fontStyleOptions: (json['fontStyleOptions'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      choicePossibleOptions: json['choicePossibleOptions'] as List<dynamic>?,
      userFieldLabel: json['userFieldLabel'] as String?,
    )
      ..columnHeader = json['columnHeader'] as Map<String, dynamic>?
      ..rowCount = json['rowCount'] as int?
      ..columnCount = json['columnCount'] as int?
      ..regEx = json['regEx'] as String?
      ..maxLength = json['maxLength'] as int?
      ..maxCols = json['maxCols'] as int?
      ..maxRows = json['maxRows'] as int?
      ..category = json['category'] == null
          ? null
          : Category.fromJson(json['category'] as Map<String, dynamic>)
      ..subCategory = json['subCategory'] == null
          ? null
          : Category.fromJson(json['subCategory'] as Map<String, dynamic>);

Map<String, dynamic> _$DiscusFormElementToJson(DiscusFormElement instance) =>
    <String, dynamic>{
      'type': instance.type,
      'key': instance.key,
      'id': instance.id,
      'width': instance.width,
      'widthOptions': instance.widthOptions,
      'order': instance.order,
      'seqNo': instance.seqNo,
      'placeholderText': instance.placeholderText,
      'fieldLabel': instance.fieldLabel,
      'userFieldLabel': instance.userFieldLabel,
      'helperText': instance.helperText,
      'toolTip': instance.toolTip,
      'fieldEditable': instance.fieldEditable,
      'active': instance.active,
      'hidden': instance.hidden,
      'isEnabled': instance.isEnabled,
      'required': instance.required,
      'showLabel': instance.showLabel,
      'backGroundColour': instance.backGroundColour,
      'colour': instance.colour,
      'align': instance.align,
      'alignPossibleOptions': instance.alignPossibleOptions,
      'fontSize': instance.fontSize,
      'fontStyleValue': instance.fontStyleValue,
      'fontStyleOptions': instance.fontStyleOptions,
      'fieldValue': instance.fieldValue,
      'fileData': instance.fileData,
      'choicePossibleOptions': instance.choicePossibleOptions,
      'columnHeader': instance.columnHeader,
      'rowCount': instance.rowCount,
      'columnCount': instance.columnCount,
      'regEx': instance.regEx,
      'dateFormat': instance.dateFormat,
      'maxLength': instance.maxLength,
      'maxCols': instance.maxCols,
      'maxRows': instance.maxRows,
      'category': instance.category,
      'subCategory': instance.subCategory,
    };
