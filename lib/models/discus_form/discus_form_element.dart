import 'package:flutter/material.dart';
import 'package:gb_private/models/info/category.dart';
import 'package:gb_private/utils/extension.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

part 'discus_form_element.g.dart';

/// Dynamic Form Element parent
@JsonSerializable()
class DiscusFormElement {
  /// Mandatory - for identification
  String type;
  String? key;
  int id;

  /// Layout & UI
  String? width;
  List<String>? widthOptions;
  int? order;
  int? seqNo;

  /// Helpers & labels
  // String placeHolder;
  String? placeholderText;

  ///mahershi
  ///changed lableText to fieldLabel acc to the json key
  String? fieldLabel; // -> Main label
  String? userFieldLabel;
  String? helperText; // -> extra text (for help)
  String? toolTip; // -> Tooltip for element

  /// Access options
  bool? fieldEditable = false;
  bool? active = false;
  bool? hidden = false;
  bool? isEnabled = false;
  bool? required = false;
  bool? showLabel = true;

  // UI items
  String? backGroundColour = "";
  String? colour;
  String? align;
  List<String>? alignPossibleOptions;

  /// Font style
  int? fontSize;
  List? fontStyleValue;
  List<String>? fontStyleOptions;

  /// Common
  dynamic fieldValue;
  String? fileData;

  /// for multidrop down and drop down possible choices
  List<dynamic>? choicePossibleOptions;

  ///facing an issue regarding abstraction.
  ///mahershi
  ///for table
  Map<String, dynamic>? columnHeader;
  int? rowCount;
  int? columnCount;

  /// Other info - for elements
  String? regEx;
  String? dateFormat;
  int? maxLength;

  /// Text Areas
  int? maxCols;
  int? maxRows;

  Category? category;
  Category? subCategory;
  // CCNumValidationResults? ccNumValidationResults;


  // Json
  factory DiscusFormElement.fromJson(Map<String, dynamic> json) =>
      _$DiscusFormElementFromJson(json);

  Map<String, dynamic> toJson() => _$DiscusFormElementToJson(this);

  DiscusFormElement(
      {required this.type,
        this.key = "",
        this.dateFormat,
        this.id = -1,
        this.width = "Half",
        this.widthOptions,
        this.order = -1,
        this.seqNo,
        this.placeholderText,
        required this.fieldLabel,
        required this.fieldValue,
        this.fileData,
        this.helperText,
        this.toolTip,
        this.fieldEditable,
        this.active = false,
        this.hidden = false,
        this.isEnabled = false,
        this.required = false,
        this.backGroundColour = "",
        this.showLabel = true,
        this.colour = "",
        this.align = "",
        this.alignPossibleOptions,
        this.fontSize,
        this.fontStyleValue,
        this.fontStyleOptions,
        this.choicePossibleOptions,
        this.userFieldLabel});

  FormElementTypes? toEnumType() {
    return FormElementTypes.values
        .firstWhereOrNull((FormElementTypes value) => value.name == type);
  }

  String? toEnumTitle() {
    return FormElementTypes.values
        .firstWhereOrNull((FormElementTypes value) => value.name == type)
        ?.title;
  }

  DiscusFormElement copy() {
    return DiscusFormElement.fromJson(toJson());
  }
}

enum FormElementTypes {
  string, // => StringTextField
  email, // => EmailTextField
  date, // => DateField
  number, // => NumberTextField
  password, // => PasswordTextField
  card, // => CardTextField
  website, // => WebsiteTextField
  note, // => NoteTextField
  name, // => NameTextField
  cvv, // => CvvTextField
  expiryDate, // => ExpiryDateTextField
  address, // => AddressTextField
  bankName, // => BankNameTextField
  documentType, // => DocumentTypeTextField
  account, // => AccountTextField
  location, // => LocationTextField
  trendingPsw,
  accountHolderName, // =>
  dateOfIssuance,
  dueDate,
  amount,
}

///style function where font,italic,bold,underline are handel.
TextStyle styleOfBackend(DiscusFormElement elem, BuildContext context) {
  String me = 'Style';
  final LogUtil logger = LogUtil(me);
  FontWeight? fontStyleWeight;
  FontStyle? fontStyleItalic;
  TextDecoration? textDecorationUnderLine;
  Color? textColor;

  try {
    textColor = chooseColor(elem.colour);
  } catch (e) {
    textColor = Theme.of(context).brightness == Brightness.dark
        ? Colors.white
        : Colors.black;
    logger.logInfo(e);
  }

  try {
    fontStyleWeight =
    elem.fontStyleValue!.contains("Bold") ? FontWeight.bold : null;
    fontStyleItalic =
    elem.fontStyleValue!.contains("Italic") ? FontStyle.italic : null;
    textDecorationUnderLine = elem.fontStyleValue!.contains("Underline")
        ? TextDecoration.underline
        : null;
  } catch (e) {
    logger.logInfo(e);
  }
  double? fontSize;

  try {
    fontSize = elem.fontSize as double?;
    final format = NumberFormat("#,##0.000");
    fontSize = format.format(fontSize) as double?;
  } catch (e) {
    fontSize = 16;
    logger.logInfo(e);
  }
  return TextStyle(
    color: textColor,
    fontSize: 14,
    fontWeight: fontStyleWeight,
    fontStyle: fontStyleItalic,
    decoration: textDecorationUnderLine,
  );
}

Color? chooseColor(String? colour) {
  switch (colour) {
    case "blue":
      return Colors.blue;
    case "black":
      return Colors.black;
    case "white":
      return Colors.white;
    case "yellow":
      return Colors.yellow;
    case "brown":
      return Colors.brown;
    case "green":
      return Colors.green;
    case "indigo":
      return Colors.indigo;
    case "orange":
      return Colors.orange;
    case "red":
      return Colors.red;
    case "purple":
      return Colors.purple;
    case "pink":
      return Colors.pink;
    case "grey":
      return Colors.grey;
    default:
      throw UnimplementedError();
  }
}

/// hide the widget element attribute
hiddenWidget(DiscusFormElement elem, BuildContext context) {
  return elem.hidden! ? false : true;
}

