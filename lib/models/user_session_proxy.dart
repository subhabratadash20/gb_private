import 'package:json_annotation/json_annotation.dart';

part 'user_session_proxy.g.dart';

/// Names match up with the Java code.
@JsonSerializable()
class UserSessionProxy {
  // Default status.
  dynamic status;

  int userId;
  int? clientId;
  String? token;
  bool? isAdmin;
  int? siteId;
  bool? staySignIn;
  String? eMail;

  // Json
  factory UserSessionProxy.fromJson(Map<String, dynamic> json) => _$UserSessionProxyFromJson(json);
  Map<String, dynamic> toJson() => _$UserSessionProxyToJson(this);

  UserSessionProxy(
      {required this.userId,
        this.clientId,
        this.token,
        this.isAdmin,
        this.siteId,
        this.eMail = "",
        this.staySignIn});

  @override
  String toString() {
    return 'UserSessionBO{userId: $userId, clientId: $clientId, token: <hidden>, isAdmin: $isAdmin, siteId: $siteId, staySignIn: $staySignIn}';
  }
}
