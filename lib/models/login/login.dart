// import 'package:json_annotation/json_annotation.dart';

import 'package:hive/hive.dart';

part 'login.g.dart';

//After any changes please run "dart run build_runner build"
@HiveType(typeId: 0)
class LoginModel extends HiveObject {
  @HiveField(0)
  final String title;
  @HiveField(1)
  final String website;
  @HiveField(2)
  final String userName;
  @HiveField(3)
  List<String?>? icons;
  @HiveField(4)
  String? password;
  @HiveField(5)
  String? category;
  @HiveField(6)
  bool isFavorite;
  @HiveField(7)
  List<Map<String, dynamic>>? additionalFields;
  @HiveField(8)
  final String id;
  @HiveField(9)
  String? profilePic;

  LoginModel({
    this.profilePic = '',
    required this.id,
    required this.title,
    required this.website,
    required this.userName,
    required this.icons,
    this.password,
    this.category,
    required this.isFavorite,
    this.additionalFields,
  });

  // Json
}
