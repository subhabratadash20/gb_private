// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class LoginModelAdapter extends TypeAdapter<LoginModel> {
  @override
  final int typeId = 0;

  @override
  LoginModel read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return LoginModel(
      profilePic: fields[9] as String?,
      id: fields[8] as String,
      title: fields[0] as String,
      website: fields[1] as String,
      userName: fields[2] as String,
      icons: (fields[3] as List?)?.cast<String?>(),
      password: fields[4] as String?,
      category: fields[5] as String?,
      isFavorite: fields[6] as bool,
      additionalFields: (fields[7] as List?)
          ?.map((dynamic e) => (e as Map).cast<String, dynamic>())
          ?.toList(),
    );
  }

  @override
  void write(BinaryWriter writer, LoginModel obj) {
    writer
      ..writeByte(10)
      ..writeByte(0)
      ..write(obj.title)
      ..writeByte(1)
      ..write(obj.website)
      ..writeByte(2)
      ..write(obj.userName)
      ..writeByte(3)
      ..write(obj.icons)
      ..writeByte(4)
      ..write(obj.password)
      ..writeByte(5)
      ..write(obj.category)
      ..writeByte(6)
      ..write(obj.isFavorite)
      ..writeByte(7)
      ..write(obj.additionalFields)
      ..writeByte(8)
      ..write(obj.id)
      ..writeByte(9)
      ..write(obj.profilePic);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is LoginModelAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
