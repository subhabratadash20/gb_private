import 'package:gb_private/models/login/login.dart';
import 'package:hive/hive.dart';

class Boxes {
  static Box<LoginModel> getLogInsData() => Hive.box<LoginModel>("Logins");
  // static Box<SetLimitModel> getLimit() => Hive.box<SetLimitModel>(setlimit);
}
