class ImageData {
  final String imagePath;
  final String name;
  final String type;

  ImageData({required this.imagePath, required this.name, this.type = "text"});
}
