import 'package:gb_private/models/login/login.dart';
import 'package:gb_private/screens/camera/open_camera_page.dart';
import 'package:gb_private/screens/widgets/icons_galary.dart';
import 'package:gb_private/screens/controllers/main_tab_screen.dart';
import 'package:gb_private/screens/intro/intro_page.dart';
import 'package:gb_private/screens/loading/loading_page.dart';
import 'package:gb_private/screens/login/add_login_page.dart';
import 'package:gb_private/screens/login/login_detail.dart';
import 'package:gb_private/screens/login/login_details_list.dart';
import 'package:gb_private/screens/login/login_list_page.dart';
import 'package:gb_private/screens/master_password/authentication.dart';
import 'package:gb_private/screens/master_password/master_password_page.dart';
import 'package:get/get.dart';

class Routers {
  static const String appPrefix = "/gbPvt";
  static const mainScreen = "$appPrefix/mainPage";

  // splash screen
  static const initialRoute = "$appPrefix/upgrade";
  static const introRoute = "$appPrefix/intro";
  static const splashRoute = "$appPrefix/splash";
  static const loginRoute = "$initialRoute/loginRoute";

  static const masterPassRoute = "$appPrefix/master";
  static const addLoginRoute = "$appPrefix/addLogin";
  static const openCameraRoute = "$appPrefix/openCamera";
  static const authenticationRoute = "$appPrefix/authentication";
  static const loginListRoute = "$appPrefix/loginList";
  static const loginDetails = "$appPrefix/loginDetails";
  static const loginDetail = "$appPrefix/loginDetail";
  static const iconGallery = "$appPrefix/iconGallery";
}

class Pages {
  static List<GetPage> getPages = [
    GetPage(name: Routers.introRoute, page: () => const IntroPage()),
    GetPage(name: Routers.splashRoute, page: () => const LoadingPage()),
    GetPage(
        name: Routers.masterPassRoute, page: () => const MasterPasswordPage()),
    GetPage(name: Routers.mainScreen, page: () => const MainTabScreen()),
    GetPage(name: Routers.addLoginRoute, page: () => const AddLoginPage()),
    GetPage(
      name: Routers.openCameraRoute,
      page: () => const OpenCameraPage(),
    ),
    GetPage(
      name: Routers.loginDetails,
      page: () => const LoginDetails(),
    ),
    GetPage(
        name: Routers.loginDetail,
        page: () => LoginDetail(
              loginModel: Get.arguments as LoginModel,
              boxItemIndex: Get.arguments as int,
            )),
    GetPage(
      name: Routers.authenticationRoute,
      page: () => AuthenticateUser(tokenValue: Get.arguments[0]),
    ),
    GetPage(
      name: Routers.loginListRoute,
      page: () => LoginListPage(logins: Get.arguments[0]),
    ),
    GetPage(
      name: Routers.iconGallery,
      page: () => IconsGalary(fileList: Get.arguments),
    )
  ];
}
