import 'dart:io';

import 'package:camera/camera.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:gb_private/models/login/login.dart';
import 'package:gb_private/routes.dart';
import 'package:gb_private/screens/controllers/main_tab_layout_controller.dart';
import 'package:gb_private/services/info_service.dart';
import 'package:gb_private/utils/themes.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:lifecycle/lifecycle.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive/hive.dart';
import 'package:permission_handler/permission_handler.dart';

List<CameraDescription> cameras = [];
// Future<void> _requestPermission() async {
//   if (await Permission.storage.request().isGranted) {
//     _selectFolder();
//   } else {
//     // Handle permission denied
//   }
// }

// Future<void> _selectFolder() async {
//   final directory = await getExternalStorageDirectory();
//   final FilePickerResult? selectedFolders = await FilePicker.platform.pickFiles(
//     type: FileType.directory,
//     allowedExtensions: null,
//     allowFolderCreation: true,
//   );

//   if (selectedFolders != null && selectedFolders.isNotEmpty) {
//     setState(() {
//       _selectedFolderPath = selectedFolders.first.path;
//     });
//   } else {
//     // Handle folder selection cancelled
//   }
// }

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // final status = await Permission.storage.request();
  // final filePicker = FilePicker.platform;

  // if (status.isGranted) {
  // showDialog(context:, builder: builder)

  await Permission.manageExternalStorage
      .request(); // final customDir = await filePicker.getDirectoryPath();
  // var dir = await getApplicationDocumentsDirectory();s
  String? dirPath = await FilePicker.platform.getDirectoryPath();
  // debugPrint("path::::::$customDir");
  debugPrint("path::::::$dirPath");
  Hive.registerAdapter(LoginModelAdapter());
  await Hive.initFlutter();
  await Hive.openBox<LoginModel>("Logins", path: dirPath);

  String me = 'MainPage';
  // final LogUtil logger = LogUtil(me);
  // UIUtils uiUtils = UIUtils(logger);
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
  cameras = await availableCameras();
  // FlutterError.onError = (FlutterErrorDetails details) {
  //   FlutterError.presentError(details);
  // };
  SystemChrome.setEnabledSystemUIMode(
    SystemUiMode.manual,
    overlays: [
      SystemUiOverlay.top, // Shows Status bar and hides Navigation bar
    ],
  );
  Get.lazyPut(
    () => MainTabLayoutController(),
  );
  runApp(const GbPrivateApp());
  // } else {
  //   debugPrint(status.name);
  //   exit(0);
  // }
}

class GbPrivateApp extends StatefulWidget {
  const GbPrivateApp({super.key});

  @override
  State<StatefulWidget> createState() => _GbPrivateAppState();
}

class _GbPrivateAppState extends State<GbPrivateApp>
    with WidgetsBindingObserver {
  bool _isDarkMode =
      SchedulerBinding.instance.platformDispatcher.platformBrightness ==
          Brightness.dark;
  InfoService infoService = InfoService();
  bool isHide = false;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    getThemeVal();
    // getPaymentStatus();
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      getThemeVal();
      // getPaymentStatus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return OverlaySupport(
      child: GestureDetector(
        onTap: () {
          FocusScopeNode currentFocus = FocusScope.of(context);
          if (!currentFocus.hasPrimaryFocus) {
            currentFocus.unfocus();
          }
        },
        child: ThemeChanger(
          dark: _isDarkMode,
          switchTheme: _switchTheme,
          child: GetMaterialApp(
            debugShowCheckedModeBanner: false,
            // localizationsDelegates: const [
            //   S.delegate,
            //   GlobalMaterialLocalizations.delegate,
            //   GlobalWidgetsLocalizations.delegate,
            //   GlobalCupertinoLocalizations.delegate,
            // ],
            // supportedLocales: LocaleManager.getSupportedLocales(),
            smartManagement: SmartManagement.full,
            // theme: _isDarkMode ? darkThemeData() : lightThemeData(),
            theme: lightThemeData(),
            initialRoute: Routers.introRoute,
            getPages: Pages.getPages,
            navigatorObservers: [defaultLifecycleObserver],
          ),
        ),
      ),
    );
  }

  void _switchTheme(bool dark) {
    _isDarkMode = dark;
    setState(() {});
  }

  getThemeVal() async {
    var selectedTheme = await infoService.read('theme');
    switch (selectedTheme) {
      case '0':
        _isDarkMode =
            SchedulerBinding.instance.platformDispatcher.platformBrightness ==
                Brightness.dark;
        break;
      case '1':
        _isDarkMode = false;
        break;
      case '2':
        _isDarkMode = true;
        break;
      default:
        _isDarkMode =
            SchedulerBinding.instance.platformDispatcher.platformBrightness ==
                Brightness.dark;
    }
    setState(() {});
  }
}

class ThemeChanger extends InheritedWidget {
  final bool dark;
  final Function(bool dark) switchTheme;

  const ThemeChanger({
    super.key,
    required this.dark,
    required this.switchTheme,
    required Widget child,
  }) : super(child: child);

  static ThemeChanger? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<ThemeChanger>();
  }

  @override
  bool updateShouldNotify(ThemeChanger oldWidget) => dark != oldWidget.dark;
}
