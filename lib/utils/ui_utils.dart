import 'dart:convert';
import 'dart:io';
import 'dart:math' as math;

import 'package:credit_card_validator/credit_card_validator.dart';
import 'package:credit_card_validator/validation_results.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gb_private/models/discus_form/discus_form_element.dart';
import 'package:gb_private/models/info/category.dart';
import 'package:gb_private/screens/widgets/summary_card.dart';
import 'package:gb_private/utils/global.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:gb_private/utils/themes.dart';
import 'package:get/get.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:velocity_x/velocity_x.dart';
import 'package:path/path.dart' as path;

import '../screens/components/discus_text_field.dart';

class UpperCaseTextFormatter extends TextInputFormatter {
  String capitalize(String value) {
    var result = value[0].toUpperCase();
    bool cap = true;
    for (int i = 1; i < value.length; i++) {
      if (value[i - 1] == " " && cap == true) {
        result = result + value[i].toUpperCase();
      } else {
        result = result + value[i];
        cap = false;
      }
    }
    return result;
  }

  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: capitalize(newValue.text),
      selection: newValue.selection,
    );
  }
}

class LowerCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toLowerCase(),
      selection: newValue.selection,
    );
  }
}

class CardNumberFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    var inputText = newValue.text;

    if (newValue.selection.baseOffset == 0) {
      return newValue;
    }

    var bufferString = StringBuffer();
    for (int i = 0; i < inputText.length; i++) {
      bufferString.write(inputText[i]);
      var nonZeroIndexValue = i + 1;
      if (nonZeroIndexValue % 4 == 0 && nonZeroIndexValue != inputText.length) {
        bufferString.write(' ');
      }
    }

    var string = bufferString.toString();
    return newValue.copyWith(
      text: string,
      selection: TextSelection.collapsed(
        offset: string.length,
      ),
    );
  }
}

class UIUtils {
  final LogUtil _logger;
  UIUtils(this._logger);

  /// For eMail validation
  static String eMailRegex =
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
  static final RegExp regExp = RegExp(eMailRegex);

  /// For Password validation
  static String passwordRegex =
      r'^(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
  static final RegExp regExpPw = RegExp(passwordRegex);

  logJsonData(Map<String, dynamic> data) {
    JsonEncoder encoder = const JsonEncoder.withIndent('  ');
    String prettyprint = encoder.convert(data);
    _logger.logTrace("\n-------------------$prettyprint\n-------------------");
  }

  showSnackBarMessage(context, String message) {
    return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
    ));
  }

  /// Using in GreenBox Creating Folder
  validateFolderName(String? value) {
    if (value!.isEmpty || value.length > 3) {
      return 'Please enter a Folder Name';
    }
  }

  final dividerDefault =
      const Divider(thickness: 0.8, color: Color.fromRGBO(238, 238, 238, 1));

  ///email id input

  /// Filter through ANY list that has toJson implemented
  Future<void> filterJsonList(
      String searchText, List? listToSearch, List? displayList) async {
    displayList!.clear();
    listToSearch!.cast().forEach((item) {
      var tsk = item;
      bool matchFound = false;
      for (var element in item.toJson().values) {
        if (element == null) continue;
        String elementStr = element.toString().toLowerCase();
        if (elementStr.contains(searchText.toLowerCase())) {
          _logger.logTrace(
              'TaskListWidget::searchData::Match for $searchText found in $elementStr');
          displayList.add(tsk);
          matchFound = true;
          break;
        }
      }
      // If we reached this far, the text wasn't found. Remove element.
      if (!matchFound) {
        displayList.remove(tsk);
        _logger.logTrace('Removing element ${item.toJson()}');
      }
    });
  }

  ///password input
  Widget passwordView(
      String txtLabelPassword,
      String txtEnterPassword,
      TextEditingController passwordCon,
      RxBool hidePassword,
      void Function(String value) changePassword,
      bool passwordIsValidCurrent) {
    return DiscusFormTextField(
      textInputType: TextInputType.visiblePassword,
      label: txtLabelPassword,
      hintText: txtEnterPassword,
      controller: passwordCon,
      obscureText: hidePassword.value,
      suffixIcon: IconButton(
        icon: hidePassword.value
            ? const Icon(Icons.visibility_off_outlined, color: widgetIcon)
            : const Icon(Icons.visibility_outlined, color: widgetIcon),
        onPressed: () {
          hidePassword.value = !hidePassword.value;
        },
      ),
      onChanged: changePassword,
      errorText: passwordIsValidCurrent ? null : "S.current.txtErrorPassword",
      onEditingComplete: () => TextInput.finishAutofillContext(),
      isPswMatch: true,
    );
  }

  gridOrListView(
      FromPage? fromPage, Function? onViewChange, ViewType? listGrid) {
    return Visibility(
      visible: fromPage == FromPage.favourite ||
          fromPage == FromPage.secureInfo ||
          fromPage == FromPage.settingCategories ||
          fromPage ==
              FromPage
                  .settingsAddCategories || // widget.fromPage == FromPage.settingsPage ||
          fromPage == FromPage.subCategory,
      child: Padding(
        padding: const EdgeInsets.only(right: 6),
        child: InkWell(
            onTap: () {
              var viewType =
                  listGrid == ViewType.list ? ViewType.grid : ViewType.list;

              if (onViewChange != null) {
                onViewChange(viewType);
              }
            },
            child: listGrid == ViewType.grid
                ? const Icon(Icons.grid_view_rounded, size: 28)
                : const Icon(Icons.list, size: 28)),
      ),
    );
  }

  checkPlatformIsWeb() {
    if (kIsWeb) {
      return true;
    } else {
      return false;
    }
  }

  checkPlatformIsAndroidOrIos() {
    _logger.logDebug(
        "Platform is ${Platform.operatingSystem} version ${Platform.operatingSystemVersion}");
    return Platform.isAndroid || Platform.isIOS;
  }

  /// Show Navigation Item with divider and non divider (optional).
  Widget drawerItemWidget(
      {bool visibleDivider = false,
      bool visibleTitle = true,
      required Widget child}) {
    return Column(children: [
      Visibility(
        visible: visibleDivider,
        child: const Divider(),
      ),
      Visibility(visible: visibleTitle, child: child),
    ]);
  }

  /// Drawer header Using for web
  Widget createDrawerBodyItem(
      {required IconData icon,
      required String title,
      required VoidCallback onTap,
      Color? tileColor}) {
    return ListTile(
      title: HStack(
        <Widget>[Icon(icon), title.text.make().pOnly(left: 8.0)],
      ),
      tileColor: tileColor,
      onTap: onTap,
      dense: true,
    );
  }

  // final googleSignIn = GoogleSignIn(scopes: [
  //   drive.DriveApi.driveAppdataScope,
  //   drive.DriveApi.driveFileScope,
  //   drive.DriveApi.driveReadonlyScope,
  // ], clientId: kIsWeb ? Global.clientIdWeb : null);
  //
  // void uploadFileToGoogleDrive(
  //     List<int> settingJson,
  //     GoogleSignInAccount? googleUser,
  //     String? accessToken,
  //     String fileName,
  //     bool fileOverride,
  //     Function onError,
  //     Function onSuccess,
  //     {String? folderId}) async {
  //   drive.DriveApi? driveApi;
  //
  //   if (googleUser != null) {
  //     driveApi = await GoogleDriveService().getDriveApi(googleUser);
  //     if (driveApi == null) {
  //       onError(1);
  //       return;
  //     }
  //   } else {
  //     driveApi = await GoogleDriveService().getDriveApiClient(accessToken!);
  //   }
  //
  //   // final String? folderId =
  //   //     await GoogleDriveService().getFolderId(driveApi, fileOverride);
  //   List<String>? folderIDNew = [];
  //   if (folderId != null) {
  //     folderIDNew.add(folderId);
  //     await InfoService().save('SelectedFolderId', folderId);
  //   }
  //   if (folderId == null) {
  //     String? folderId = await InfoService().read("SelectedFolderId");
  //     if (folderId != null) {
  //       folderIDNew.add(folderId);
  //       await InfoService().save('SelectedFolderId', folderId);
  //     }
  //     // else {
  //     //   String rootFolderId =
  //     //       await GoogleDriveService().getRootFolderId(driveApi);
  //     //   folderIDNew.add(rootFolderId);
  //     //   await InfoService().save('SelectedFolderId', rootFolderId);
  //     // }
  //     // onError(2);
  //     // return;
  //   }
  //
  //   // Create data here instead of loading a file
  //   final Stream<List<int>> mediaStream =
  //   Future.value(settingJson).asStream().asBroadcastStream();
  //   var media = drive.Media(mediaStream, settingJson.length);
  //   // Upload
  //   if (!fileOverride) {
  //     // Set up File info
  //     drive.File driveFile = drive.File();
  //     driveFile.name = fileName;
  //     driveFile.modifiedTime = DateTime.now().toUtc();
  //
  //     // driveFile.parents = folderIDNew;
  //
  //     if (folderIDNew.isNotEmpty) {
  //       driveFile.parents = folderIDNew;
  //     }
  //     final response = await driveApi.files.create(
  //       driveFile,
  //       uploadMedia: media,
  //     );
  //     // fileId = response.id;
  //
  //     _logger.logDebug("response: ${response.id}");
  //   } else {
  //     // String getFileID = await InfoService().read("FileId");
  //     drive.File driveFileOverride = drive.File();
  //     driveFileOverride.name = fileName;
  //     driveFileOverride.modifiedTime = DateTime.now().toUtc();
  //
  //     if (folderIDNew.isNotEmpty) {
  //       driveFileOverride.parents = folderIDNew;
  //     }
  //     String? fileId;
  //     await driveApi.files.list(q: "name='$fileName'").then((r) {
  //       fileId = r.files![0].id;
  //       debugPrint("File ID: $fileId");
  //     });
  //
  //     if (fileId != null) {
  //       await driveApi.files.delete(fileId!).then((value) async {
  //         final response = await driveApi?.files
  //             .create(driveFileOverride, uploadMedia: media);
  //         _logger.logDebug("response: ${response?.id}");
  //       });
  //     }
  //   }
  //
  //   onSuccess();
  // }
  //
  // void syncDataOnGoogleDrive(
  //     List<int> settingJson, BuildContext context, Function? onEvent) async {
  //   String? accessToken = await InfoService().read('GoogleAcessToken');
  //   String googleDriveFileName =
  //   await InfoService().read("GoogleDriveFileName");
  //   if (checkPlatformIsAndroidOrIos()) {
  //     var signedInUser = await googleSignIn.isSignedIn();
  //     GoogleSignInAccount? currentUserData = googleSignIn.currentUser;
  //     if (signedInUser && currentUserData != null) {
  //       uploadFileToGoogleDrive(settingJson, currentUserData, accessToken,
  //           googleDriveFileName, true, (int error) async {
  //             if (error == 1) {
  //               await showDialog(
  //                 context: context,
  //                 builder: (BuildContext context) => showMessage(
  //                     context, "Sign-in first", "Error", false,
  //                     okClick: () {}),
  //               );
  //             } else if (error == 2) {
  //               await showDialog(
  //                 context: context,
  //                 builder: (BuildContext context) => showMessage(
  //                     context, "Failure", "Error", false,
  //                     okClick: () {}),
  //               );
  //             }
  //           }, () {
  //             // showSnackBarMessage(context, "Uploaded");
  //             debugPrint("uploaded");
  //             InfoService().save('SyncOn', 'GoogleDrive');
  //           });
  //     } else {
  //       await googleSignIn.signIn();
  //       onEvent!();
  //       // syncDataOnGoogleDrive(settingJson, context);
  //     }
  //   } else {
  //     uploadFileToGoogleDrive(
  //         settingJson, null, accessToken, googleDriveFileName, true,
  //             (int error) async {
  //           if (error == 1) {
  //             await showDialog(
  //               context: context,
  //               builder: (BuildContext context) => showMessage(
  //                   context, "Sign-in first", "Error", false,
  //                   okClick: () {}),
  //             );
  //           } else if (error == 2) {
  //             await showDialog(
  //               context: context,
  //               builder: (BuildContext context) =>
  //                   showMessage(context, "Failure", "Error", false, okClick: () {}),
  //             );
  //           }
  //         }, () {
  //       // showSnackBarMessage(context, "Uploaded");
  //       InfoService().save('SyncOn', 'GoogleDrive');
  //
  //       debugPrint("uploaded");
  //     });
  //   }
  // }

  copyDataToClipBoard(text, context) async {
    await Clipboard.setData(ClipboardData(text: text));
    ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
      content: Text('Copied to clipboard'),
    ));
  }

// showGoogleDriveFileList(
//     BuildContext context, drive.FileList list, googleSignIn, keyValue) async {
//   await showDialog(
//       context: context,
//       builder: (context) {
//         return AlertDialog(
//           title: const Text(
//             "Choose file to download from drive",
//             style: TextStyle(fontSize: 16),
//           ),
//           content: ListView.builder(
//               shrinkWrap: true,
//               itemCount: list.files?.length,
//               itemBuilder: (context, index) {
//                 String? fileName = list.files![index].name;
//                 String? fileNameWithoutExt = fileName?.split('.').first;
//                 return InkWell(
//                     onTap: () async {
//                       File savedFile = await GoogleDriveService()
//                           .downloadGoogleDriveFile(
//                               list.files![index], googleSignIn);
//                       String? selectedGoogleFileName =
//                           path.basename(savedFile.path);
//                       await InfoService().save(
//                           'GoogleDriveFileName', selectedGoogleFileName);
//                       await InfoService().save('SyncOn', 'GoogleDrive');
//                       await InfoService().restoreJson(savedFile);
//
//                       var driveApi = await GoogleDriveService()
//                           .getDriveApi(googleSignIn.currentUser);
//                       drive.FileList fileList = await driveApi!.files
//                           .list(q: "name='$selectedGoogleFileName'");
//                       InfoService().save('FileId', fileList.files![0].id);
//
//                       debugPrint(savedFile.path);
//
//                       String navPath =
//                           await AuthService().getSaveMasterPassword(keyValue);
//                       Get.offAllNamed(navPath);
//                       showSnackBarMessage(
//                           Get.context, "Password saved successfully.");
//                     },
//                     child: Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Row(
//                         children: [
//                           Text(fileNameWithoutExt ?? ''),
//                           Text(
//                             list.files![index].modifiedTime.toString(),
//                           ),
//                         ],
//                       ),
//                     ));
//               }),
//         );
//       });
// }

// showDropBoxFileList(BuildContext context, list, keyValue) async {
//   await showDialog(
//       context: context,
//       builder: (context) {
//         return AlertDialog(
//           title: const Text(
//             "Choose file to download from dropbox",
//             style: TextStyle(fontSize: 16),
//           ),
//           content: ListView.builder(
//               shrinkWrap: true,
//               itemCount: list.length,
//               itemBuilder: (context, index) {
//                 String fileName = list[index]['name'];
//                 String fileNameWithoutExt = fileName.split('.').first;
//                 return InkWell(
//                     onTap: () async {
//                       await DropBoxService().downloadFromDropBox(
//                           list[index]['name'],
//                           folderName: '');
//                       String navPath = await AuthService().getNavPath();
//                       Get.offAllNamed(navPath);
//                       showSnackBarMessage(
//                           Get.context, "Password saved successfully.");
//                     },
//                     child: Padding(
//                       padding: const EdgeInsets.all(8.0),
//                       child: Text(fileNameWithoutExt),
//                     ));
//               }),
//         );
//       });
// }

  Map<String, dynamic>? validateCardNum(String? value,
      CreditCardValidator ccValidator, bool? required, String? catName) {
    if (required ?? false) {
      if (value == null || value.isEmpty) {
        return {"result": null, "message": 'Please enter card number'};
      } else {
        if (value.length < 16) {
          return {
            "result": null,
            "message": 'Card number must have at least 16 digits'
          };
        }

        var ccNumResults = ccValidator.validateCCNum(value);
        if (ccNumResults.isValid) {
          return {"result": ccNumResults, "message": null};
        } else {
          return {
            "result": ccNumResults,
            "message": catName == 'Credit Card'
                ? ccNumResults.message
                : "Invalid card number"
          };
        }
      }
    } else {
      if (value == null || value.isEmpty) {
        return null;
      } else {
        if (value.length < 16) {
          return {
            "result": null,
            "message": 'Card number must have at least 16 digits'
          };
        }

        var ccNumResults = ccValidator.validateCCNum(value);
        if (ccNumResults.isValid) {
          return {"result": ccNumResults, "message": null};
        } else {
          return {
            "result": ccNumResults,
            "message": catName == 'Credit Card'
                ? ccNumResults.message
                : "Invalid card number"
          };
        }
      }
    }
  }

  String? validateCvv(String? value, CreditCardValidator ccValidator,
      bool? required, CCNumValidationResults? ccNumValidationResults) {
    if (required ?? false) {
      if (value == null || value.isEmpty) {
        return 'Please enter CVV';
      } else {
        if (ccNumValidationResults != null) {
          var cvvResults =
              ccValidator.validateCVV(value, ccNumValidationResults.ccType);
          if (cvvResults.isValid) {
            return null;
          } else {
            return cvvResults.message;
          }
        }
      }
      if (value.length < 3) {
        return 'CVV must be at least 3 digits';
      }
    } else {
      if (ccNumValidationResults != null) {
        var cvvResults =
            ccValidator.validateCVV(value!, ccNumValidationResults.ccType);
        if (cvvResults.isValid) {
          return null;
        } else {
          return cvvResults.message;
        }
      }
    }
    return null;
  }

  // Future<void> getParamToUploadToDrive(List<int> settingJson, context,
  //     googleSignIn, driveFile, controller, fromPage) async {
  //   final driveApi = await GoogleDriveService().getDriveApi(googleSignIn);
  //   if (driveApi == null) {
  //     await showDialog(
  //       context: context,
  //       builder: (BuildContext context) => showMessage(
  //           context, "Sign-in first", "Something went wrong", false),
  //     );
  //     return;
  //   }
  //
  //   // Not allow a user to do something else
  //   showGeneralDialog(
  //     context: context,
  //     barrierDismissible: false,
  //     transitionDuration: const Duration(seconds: 2),
  //     barrierColor: Colors.black.withOpacity(0.5),
  //     pageBuilder: (context, animation, secondaryAnimation) => const Center(
  //       child: CircularProgressIndicator(),
  //     ),
  //   );
  //
  //   final String? folderId =
  //   await GoogleDriveService().getFolderId(driveApi, fromPage);
  //   if (folderId == null) {
  //     // await uiUtils.showMessage(context, "Failure", "Error");
  //     await showDialog(
  //       context: context,
  //       builder: (BuildContext context) =>
  //           showMessage(context, "Failure", "Upload fail try again", false),
  //     );
  //     return;
  //   }
  //
  //   // Create data here instead of loading a file
  //   final Stream<List<int>> mediaStream =
  //   Future.value(settingJson).asStream().asBroadcastStream();
  //   var media = drive.Media(mediaStream, settingJson.length);
  //
  //   // Set up File info
  //   driveFile.name = '${controller.text}.json';
  //   driveFile.modifiedTime = DateTime.now().toUtc();
  //   driveFile.parents = [folderId];
  //
  //   // Upload
  //   final response = await driveApi.files.create(driveFile, uploadMedia: media);
  //   _logger.logDebug("response: ${response.id}");
  //   GoogleDriveService().signOut(googleSignIn);
  // }

  List<String> instructionsList = [
    'For strong password your password have Minimum 8 characters',
    'Minimum 1 Upper case, ',
    'Minimum 1 lowercase',
    'Minimum 1 Numeric Number',
    'Minimum 1 Special Character'
  ];

  instructionListWidget(dynamic getTextValue) {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: instructionsList.length,
      itemBuilder: (context, index) {
        final isValid = validatePasswordInstruction(getTextValue, index);
        return Row(
          children: [
            Icon(
              isValid ? Icons.check : Icons.close,
              color: isValid ? Colors.green : Colors.red,
            ).paddingSymmetric(vertical: 2, horizontal: 6),
            Expanded(child: Text(instructionsList[index]))
          ],
        );
      },
    );
  }

  bool validatePasswordInstruction(String password, int index) {
    switch (index) {
      case 0:
        return password.length >= 8;
      case 1:
        return RegExp(r'[A-Z]').hasMatch(password);
      case 2:
        return RegExp(r'[a-z]').hasMatch(password);
      case 3:
        return RegExp(r'[0-9]').hasMatch(password);
      case 4:
        return RegExp(r'[!@#$%^&*(),.?":{}|<>]').hasMatch(password);
      default:
        return false;
    }
  }

  getReadVal(value) {
    // widget.settingFlag! ? true : false
    switch (value) {
      case null:
        return false;

      case false:
        return false;
      // Add more cases as needed
      default:
        return true;
    }
  }

  List<TextInputFormatter>? getTextFieldFormattersDate(value) {
    switch (value) {
      case null:
        return [];

      case false:
        return [];
      // Add more cases as needed
      default:
        return <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly
          // Allow only digits (numbers)
        ];
    }
  }

  List<TextInputFormatter>? getTextFieldFormattersCreditCard(value) {
    switch (value) {
      case null:
        return [];

      case false:
        return [];
      // Add more cases as needed
      default:
        return <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly,
          LengthLimitingTextInputFormatter(16),
          CardNumberFormatter(),
        ];
    }
  }

  List<TextInputFormatter>? getTextFieldFormattersAccount(value) {
    switch (value) {
      case null:
        return [];

      case false:
        return [];
      // Add more cases as needed
      default:
        return <TextInputFormatter>[
          FilteringTextInputFormatter.digitsOnly,
          LengthLimitingTextInputFormatter(12),
          // Allow only digits (numbers)
        ];
    }
  }

  List<TextInputFormatter>? getTextFieldFormattersCvv(value) {
    switch (value) {
      case null:
        return [];

      case false:
        return [];
      // Add more cases as needed
      default:
        return <TextInputFormatter>[
          LengthLimitingTextInputFormatter(4),
          FilteringTextInputFormatter.digitsOnly
        ];
    }
  }

  getMaxLength(value) {
    switch (value) {
      case null:
        return MaxLengthEnforcement.enforced;

      case false:
        return MaxLengthEnforcement.none;
      // Add more cases as needed
      default:
        return MaxLengthEnforcement.enforced;
    }
  }

  getFlagVal(value) {
    switch (value) {
      case null:
        return TextInputType.number;

      case false:
        return TextInputType.text;
      // Add more cases as needed
      default:
        return TextInputType.number;
    }
  }

  List<Category> categoryListSettingsPage = [
    Category(
      id: 1,
      name: 'Categories',
      icon: Icons.category_outlined.codePoint.toString(),
      colorValue: selectedIconColorCode,
    ),
    Category(
        id: 2,
        name: 'Security',
        icon: Icons.security_outlined.codePoint.toString(),
        colorValue: selectedIconColorCode),
    Category(
        id: 3,
        name: 'Data',
        icon: Icons.wb_cloudy_outlined.codePoint.toString(),
        colorValue: selectedIconColorCode),
    Category(
        id: 4,
        name: 'Purchase Plan',
        icon: Icons.ads_click.codePoint.toString(),
        colorValue: selectedIconColorCode),
    Category(
        id: 5,
        name: 'About',
        icon: Icons.add_to_home_screen_outlined.codePoint.toString(),
        colorValue: selectedIconColorCode),
  ];

  List<Category> categoryListSecurityPage = [
    Category(
        id: 1,
        name: "Change password",
        subTitle: "",
        icon: Icons.key_outlined.codePoint.toString(),
        colorValue: selectedIconColorCode),
    Category(
        id: 2,
        name: 'Screen Lock',
        subTitle: "Biometric & Screen locks",
        icon: Icons.fingerprint_outlined.codePoint.toString(),
        colorValue: selectedIconColorCode),
    Category(
        id: 3,
        name: 'Theme',
        subTitle: "",
        icon: Icons.contrast_outlined.codePoint.toString(),
        colorValue: selectedIconColorCode)
  ];

  List<Category> themeList = [
    Category(
        id: 1,
        name: 'System default',
        icon: Icons.light_mode_outlined.codePoint.toString(),
        colorValue: selectedIconColorCode),
    Category(
        id: 2,
        name: 'Light',
        icon: Icons.light_mode_outlined.codePoint.toString(),
        colorValue: selectedIconColorCode),
    Category(
        id: 3,
        name: 'Dark',
        icon: Icons.dark_mode_outlined.codePoint.toString(),
        colorValue: selectedIconColorCode),
  ];

  List<Category> dataList = [
    Category(
      id: 1,
      name: 'This device',
      // icon: Icons.computer.codePoint.toString(),
      // colorValue: selectedIconColorCode,
      imagePath: 'assets/icons/this-device.png',
    ),
    Category(
      id: 2,
      name: 'Google Drive',
      // icon: Icons.add_to_drive.codePoint.toString(),
      // colorValue: selectedIconColorCode,
      imagePath: 'assets/icons/google-drive.png',
    ),
    Category(
      id: 3,
      name: 'DropBox',
      // icon: Icons.add_box.codePoint.toString(),
      // colorValue: selectedIconColorCode,
      imagePath: 'assets/icons/dropbox.png',
    ),
    Category(
      id: 4,
      name: 'Greenbox',
      imagePath: 'assets/icons/greenbox.png',
    ),
    Category(
      id: 5,
      name: 'Onedrive',
      // icon: Icons.cloud.codePoint.toString(),
      // colorValue: selectedIconColorCode,
      imagePath: 'assets/icons/onedrive.png',
    ),
  ];

// void pickFileFromDropBox(keyValue) async {
//   FilePickerResult? selectedFile = await FilePicker.platform.pickFiles();
//
//   if (selectedFile != null) {
//     File? file = File(selectedFile.files.single.path!);
//     debugPrint(file.path);
//
//     var savedFile = await DropBoxService().downloadFromDropB ox(file);
//     String navPath = await AuthService().getSaveMasterPassword(keyValue);
//     Get.offAllNamed(navPath);
//
//     InfoService().restoreJson(savedFile);
//   } else {
//     String navPath = await AuthService().getSaveMasterPassword(keyValue);
//     Get.offAllNamed(navPath);
//     showSnackBar(
//         context: Get.context,
//         msg: "Password saved successfully.",
//         linkText: "",
//         duration: 5);
//   }
// }

// Widget adBanner(bannerAd) {
//   return SizedBox(
//     height: 60,
//     width: double.infinity,
//     child: AdWidget(
//       ad: bannerAd!,
//     ),
//   );
// }

  static const transformFileOutlinedIcon = RotatedBox(
    quarterTurns: 1,
    child: Icon(Icons.insert_drive_file_outlined, color: Global.colorSeparator),
  );

  Icon deleteIcon = const Icon(
    Icons.delete,
    size: 18,
    color: Colors.red,
  );

  validateEmail(String eMail) {
    return regExp.hasMatch(eMail);
  }

  validatePassword(String password) {
    return regExpPw.hasMatch(password);
  }

  static const List<Map<String, dynamic>> menuDetails = [
    {"name": "Preview", "icon": FontAwesomeIcons.solidEye},
    {"name": "Share", "icon": FontAwesomeIcons.share},
    {"name": "External Share", "icon": FontAwesomeIcons.solidShareFromSquare},
    {"name": "Create SoftLink", "icon": FontAwesomeIcons.link},
    {"name": "Rename", "icon": FontAwesomeIcons.pencil},
    {"name": "Subscribe", "icon": FontAwesomeIcons.solidBell},
    {"name": "Download", "icon": FontAwesomeIcons.cloudArrowDown},
    {"name": "Move To", "icon": FontAwesomeIcons.arrowsUpDownLeftRight},
    {"name": "Copy To", "icon": FontAwesomeIcons.solidCopy},
    {"name": "Add to Favorite", "icon": FontAwesomeIcons.solidStar},
    {"name": "Remove", "icon": FontAwesomeIcons.trash},
    {"name": "View Details", "icon": FontAwesomeIcons.circleInfo},
  ];

  menuListWidget(BuildContext context, Function menuTapHandler) {
    showModalBottomSheet<void>(
      context: context,
      isScrollControlled: true,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15),
          topRight: Radius.circular(15),
        ),
      ),
      builder: (BuildContext context) {
        return SizedBox(
          height: MediaQuery.of(context).size.height,
          // color: Colors.amber,
          child: ListView.builder(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () => menuTapHandler(index),
                child: Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: const BoxDecoration(
                    border: Border(bottom: BorderSide(width: 0.1)),
                    // borderRadius: BorderRadius.circular(5)
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Icon(
                        menuDetails[index]["icon"],
                        size: 20,
                      ),
                      const SizedBox(
                        width: 25,
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Text(
                            menuDetails[index]["name"],
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            },
            itemCount: menuDetails.length,
          ),
        );
      },
    );
  }

  /// truncate the string
  String truncateWithEllipsis(int cutoff, String myString) {
    return (myString.length <= cutoff)
        ? myString
        : '${myString.substring(0, cutoff)}...';
  }

  static Widget selectIconAvtar(dynamic selectedIcon) {
    return Column(
      children: [
        Container(
          padding: const EdgeInsets.all(20),
          width: 80,
          height: 80,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(50),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 2,
                blurRadius: 4,
                offset: const Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: selectedIcon.iconValue != null
              ? Icon(
                  // selectedIcon?.toIconData() ?? Icons.home,
                  selectedIcon?.toIconData(),
                  color: Color(selectedIcon?.colourValue ?? 0xFF1E88E5),
                  size: 30,
                )
              : Image.asset(
                  'assets/icons/transparent-icon.png',
                  width: 10,
                  height: 10,
                ),
        ),
        const SizedBox(
          height: 10,
        ),
        const Text("Change Icon"),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }

  ListTile purchasedCardTile(context, String text, bool isPurchased) {
    return ListTile(
        leading: Visibility(
          visible: isPurchased,
          child: Icon(
            Icons.check,
            size: 28,
            color: Theme.of(context).primaryColor,
          ),
        ),
        minLeadingWidth: 0,
        title: Text(
          text,
          style: const TextStyle(fontSize: 18),
        ));
  }

  purchaseCard(
      ProductDetails? productDetails, Function callBack, bool isPurchased) {
    return ListTile(
      title: Text(
        productDetails!.title,
      ),
      subtitle: Text(
        productDetails.description,
      ),
      trailing: TextButton(
        style: TextButton.styleFrom(
          backgroundColor: isPurchased ? Colors.grey[400] : Colors.green[800],
          // ignore: deprecated_member_use
          splashFactory: NoSplash.splashFactory,
          foregroundColor: Colors.white,
        ),
        onPressed: () {
          callBack();
        },
        child: Text(productDetails.price),
      ),
    );
  }

  Future<String> createFolderInAppDocDir(String folderName) async {
    if (kIsWeb) {
      // For web platform
      final folder = path.join('AppDocuments', folderName);
      // Path does not exist concept on web, so we simply return the folder path.
      return folder;
    } else {
      //Get this App Document Directory
      final Directory appDocDir = await getApplicationDocumentsDirectory();
      //App Document Directory + folder name
      final Directory appDocDirFolder =
          Directory('${appDocDir.path}/$folderName/');

      if (await appDocDirFolder.exists()) {
        //if folder already exists return path
        return appDocDirFolder.path;
      } else {
        //if folder not exists create folder and then return its path
        final Directory appDocDirNewFolder =
            await appDocDirFolder.create(recursive: true);
        return appDocDirNewFolder.path;
      }
    }
  }

// Future<String> createFolderInAppDocDir(String folderName) async {
//   if(kIsWeb){
//     final folder = io.Directory('AppDocuments/$folderName');
//     if (folder.existsSync()) {
//       // If the folder already exists, return its path
//       return folder.path;
//     } else {
//       // If the folder does not exist, create the folder and then return its path
//       folder.createSync(recursive: true);
//       return folder.path;
//     }
//
//   }else{
//     //Get this App Document Directory
//     final Directory appDocDir = await getApplicationDocumentsDirectory();
//     //App Document Directory + folder name
//     final Directory appDocDirFolder =
//     Directory('${appDocDir.path}/$folderName/');
//
//     if (await appDocDirFolder.exists()) {
//       //if folder already exists return path
//       return appDocDirFolder.path;
//     } else {
//       //if folder not exists create folder and then return its path
//       final Directory appDocDirNewFolder =
//       await appDocDirFolder.create(recursive: true);
//       return appDocDirNewFolder.path;
//     }
//   }
//
//
// }
  Future<String> createFolderInExtStorage() async {
    final Directory? extDir = await getExternalStorageDirectory();
    final Directory newDir = Directory('$extDir');
    return newDir.path;
  }

  Widget showMessage(
      BuildContext context, String msg, String title, bool isCancel,
      {Function? okClick,
      Function? cancelClick,
      TextEditingController? controller}) {
    return AlertDialog(
      title: Text(
        title,
        style: const TextStyle(fontSize: 16),
      ),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(msg),
          10.heightBox,
          Visibility(
              visible: isCancel,
              child: SizedBox(
                width: double.infinity,
                child: TextFormField(
                  controller: controller,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  )),
                ),
              )),
        ],
      ),
      actions: [
        Visibility(
          visible: isCancel,
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              minimumSize: const Size(40, 40),
              backgroundColor: Colors.white70,
            ),
            onPressed: () {
              Get.back();
            },
            child: const Text("Cancel", style: TextStyle(color: Colors.black)),
          ),
        ),
        ElevatedButton(
          style: ElevatedButton.styleFrom(
            minimumSize: const Size(40, 40),
          ),
          onPressed: () {
            if (isCancel) {
              okClick!(controller?.text.trim());
            }
            Get.back();
          },
          child: const Text(
            "OK",
            style: TextStyle(color: Colors.white),
          ),
        ),
      ],
    );
  }

  Widget showCvv(BuildContext context) {
    return AlertDialog(
      title: const Text("S.current.titleCvvDetails",
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Text("S.current.msgThe3Or4DigitSecurityCodeOnTheFront"),
          const SizedBox(height: 16),
          const Align(
            alignment: Alignment.centerLeft,
            child: Text("S.current.strBack",
                style:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          ),
          Image.asset(
              'assets/icons/indian_card_cvv.png'), // Replace with actual image paths
          const SizedBox(height: 16),
          const Align(
            alignment: Alignment.centerLeft,
            child: Text("S.current.strFront",
                style:
                    TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
          ),
          Image.asset('assets/icons/american_card_cvv.png'),
        ],
      ),
      actions: [
        TextButton(
          onPressed: () => Get.back(),
          child: const Text("S.current.btnClose"),
        ),
      ],
    );
  }

  Widget renderNoDataFound() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Container(
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
          height: 100,
          width: double.maxFinite,
          child: const Card(
            elevation: 5,
            child: Text("S.current.dataNotFound"),
          ),
        ),
      ],
    );
  }

  renderSearchBox(context, editingController, onChanged) {
    return Padding(
      padding: const EdgeInsets.all(7),
      child: TextField(
        controller: editingController,
        onChanged: onChanged,
        decoration: const InputDecoration(
            labelText: "S.current.search",
            hintText: "S.current.searchElipse",
            prefixIcon: Icon(Icons.search),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0)))),
      ),
    );
  }

  String getFormattedDateOrEmpty(timeInMillis, String dateFormat) {
    String strDate = "";
    int time;
    if (timeInMillis != null && dateFormat.isNotEmpty) {
      if (timeInMillis is String && timeInMillis.isNotEmpty) {
        time = int.parse(timeInMillis);
      } else {
        time = timeInMillis;
      }

      strDate = DateFormat(dateFormat)
          .format(DateTime.fromMillisecondsSinceEpoch(time));
    } else {
      _logger.logWarning(
          "getFormattedDateOrEmpty::Invalid call with NULL value: $timeInMillis");
    }
    return strDate;
  }

  /// When booleans can be null... Safety function
  bool getBoolVal(bool? boolVal) {
    return boolVal != null && boolVal;
  }

  List<Category> predefinedList = [
    Category(
        // date: DateUtil.getDateTimeUtc(DateTime.now()),
        id: 0,
        name: "Credit Card",
        icon: Icons.credit_card.codePoint.toString(),
        colorValue: selectedIconColorCode,
        fieldList: [
          DiscusFormElement(
              type: FormElementTypes.accountHolderName.name,
              fieldLabel: 'Type Here',
              dateFormat: 'MM/yyyy',
              fieldValue: "",
              hidden: false,
              required: true,
              placeholderText: 'Type Here'),
          DiscusFormElement(
            type: FormElementTypes.card.name,
            fieldLabel: 'Type Here',
            dateFormat: 'dd/MM/yyyy',
            fieldValue: "",
            hidden: false,
            required: true,
            placeholderText: "Type Here",
          ),
          DiscusFormElement(
              type: FormElementTypes.expiryDate.name,
              fieldLabel: 'Type Here',
              dateFormat: 'MM/yyyy',
              fieldValue: "",
              hidden: false,
              required: true,
              placeholderText: 'Type Here'),
          DiscusFormElement(
              type: FormElementTypes.cvv.name,
              fieldLabel: 'Type Here',
              dateFormat: 'MM/yyyy',
              fieldValue: "",
              hidden: false,
              required: true,
              placeholderText: 'Type Here'),
          DiscusFormElement(
              type: FormElementTypes.documentType.name,
              fieldLabel: 'Type Here',
              dateFormat: 'MM/yyyy',
              fieldValue: "",
              hidden: false,
              required: true,
              placeholderText: 'Type Here'),
        ]),
    Category(
        // date: DateUtil.getDateTimeUtc(DateTime.now()),
        id: 1,
        name: "Debit Card",
        icon: Icons.credit_card.codePoint.toString(),
        colorValue: selectedIconColorCode,
        fieldList: [
          DiscusFormElement(
              type: FormElementTypes.accountHolderName.name,
              fieldLabel: 'Type Here',
              dateFormat: 'MM/yyyy',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
          DiscusFormElement(
            type: FormElementTypes.card.name,
            fieldLabel: 'Type Here',
            dateFormat: 'dd/MM/yyyy',
            fieldValue: "",
            hidden: false,
            placeholderText: "Type Here",
          ),
          DiscusFormElement(
              type: FormElementTypes.expiryDate.name,
              fieldLabel: 'Type Here',
              dateFormat: 'MM/yyyy',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
          DiscusFormElement(
              type: FormElementTypes.cvv.name,
              fieldLabel: 'Type Here',
              dateFormat: 'MM/yyyy',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
        ]),
    Category(
        // date: DateUtil.getDateTimeUtc(DateTime.now()),
        id: 2,
        name: "Driving License",
        icon: FontAwesomeIcons.idCard.codePoint.toString(),
        colorValue: selectedIconColorCode,
        fieldList: [
          DiscusFormElement(
              type: FormElementTypes.expiryDate.name,
              fieldLabel: 'Type Here',
              dateFormat: 'MM/yyyy',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
          DiscusFormElement(
              type: FormElementTypes.dateOfIssuance.name,
              fieldLabel: 'Type Here',
              dateFormat: 'dd/MM/yyyy',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
        ]),
    Category(
        // date: DateUtil.getDateTimeUtc(DateTime.now()),
        id: 3,
        name: "Building Maintenance Payment",
        icon: Icons.apartment.codePoint.toString(),
        colorValue: selectedIconColorCode,
        fieldList: [
          DiscusFormElement(
              type: FormElementTypes.dueDate.name,
              fieldLabel: 'Type Here',
              dateFormat: 'dd/MM/yyyy',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
          DiscusFormElement(
              type: FormElementTypes.amount.name,
              fieldLabel: 'Type Here',
              dateFormat: '',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
        ]),
    Category(
        // date: DateUtil.getDateTimeUtc(DateTime.now()),
        id: 4,
        name: "Vehicle Insurance",
        icon: Icons.local_taxi_sharp.codePoint.toString(),
        colorValue: selectedIconColorCode,
        fieldList: [
          DiscusFormElement(
              type: FormElementTypes.dueDate.name,
              fieldLabel: 'Type Here',
              dateFormat: 'dd/MM/yyyy',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
          DiscusFormElement(
              type: FormElementTypes.amount.name,
              fieldLabel: 'Type Here',
              dateFormat: '',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
        ]),
    Category(
        // date: DateUtil.getDateTimeUtc(DateTime.now()),
        id: 5,
        name: "Any Membership Renewal",
        icon: Icons.subscriptions_sharp.codePoint.toString(),
        colorValue: selectedIconColorCode,
        fieldList: [
          DiscusFormElement(
              type: FormElementTypes.dueDate.name,
              fieldLabel: 'Type Here',
              dateFormat: 'dd/MM/yyyy',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
          DiscusFormElement(
              type: FormElementTypes.amount.name,
              fieldLabel: 'Type Here',
              dateFormat: '',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
        ]),
    Category(
        // date: DateUtil.getDateTimeUtc(DateTime.now()),
        id: 6,
        name: "Job Experience Letter",
        icon: FontAwesomeIcons.certificate.codePoint.toString(),
        colorValue: selectedIconColorCode,
        fieldList: [
          DiscusFormElement(
              type: FormElementTypes.documentType.name,
              fieldLabel: 'Type Here',
              dateFormat: '',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
        ]),
    Category(
        // date: DateUtil.getDateTimeUtc(DateTime.now()),
        id: 7,
        name: "Job Salary Slip",
        icon: Icons.receipt_outlined.codePoint.toString(),
        colorValue: selectedIconColorCode,
        fieldList: [
          DiscusFormElement(
              type: FormElementTypes.documentType.name,
              fieldLabel: 'Type Here',
              dateFormat: '',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
        ]),
    Category(
        // date: DateUtil.getDateTimeUtc(DateTime.now()),
        id: 8,
        name: "Degree Certificates",
        icon: FontAwesomeIcons.certificate.codePoint.toString(),
        colorValue: selectedIconColorCode,
        fieldList: [
          DiscusFormElement(
              type: FormElementTypes.documentType.name,
              fieldLabel: 'Type Here',
              dateFormat: '',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
        ]),
    Category(
        // date: DateUtil.getDateTimeUtc(DateTime.now()),
        id: 9,
        name: "Electric Bill",
        icon: Icons.electric_bolt_sharp.codePoint.toString(),
        colorValue: selectedIconColorCode,
        fieldList: [
          DiscusFormElement(
              type: FormElementTypes.dueDate.name,
              fieldLabel: 'Type Here',
              dateFormat: 'dd/MM/yyyy',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
          DiscusFormElement(
              type: FormElementTypes.amount.name,
              fieldLabel: 'Type Here',
              dateFormat: '',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
          DiscusFormElement(
              type: FormElementTypes.documentType.name,
              fieldLabel: 'Type Here',
              dateFormat: '',
              fieldValue: "",
              hidden: false,
              placeholderText: 'Type Here'),
        ]),
  ];

  /// Personal Icons Lists
  // List<IconType> icons = [
  //   IconType(icons: [
  //     IconDetails(
  //         iconValue: Icons.home.codePoint.toString(),
  //         title: 'Password',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.account_circle_outlined.codePoint.toString(),
  //         title: 'User',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.supervised_user_circle_sharp.codePoint.toString(),
  //         title: 'Users',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.favorite.codePoint.toString(),
  //         title: 'Like',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.messenger_outline.codePoint.toString(),
  //         title: 'Message',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.tapas_sharp.codePoint.toString(),
  //         title: 'Message',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.lock.codePoint.toString(),
  //         title: 'Lock',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.shopping_bag.codePoint.toString(),
  //         title: 'Bag',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.airplanemode_on_rounded.codePoint.toString(),
  //         title: 'Flight',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.add_shopping_cart_outlined.codePoint.toString(),
  //         title: 'Cart',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.key.codePoint.toString(),
  //         title: 'Key',
  //         colourValue: 0xFF067D0F,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.book.codePoint.toString(),
  //         title: 'Book',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.drive_eta_rounded.codePoint.toString(),
  //         title: 'Car',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.lock_open.codePoint.toString(),
  //         title: 'Lock',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //     IconDetails(
  //         iconValue: Icons.lock_open.codePoint.toString(),
  //         title: 'KeyLock',
  //         colourValue: selectedIconColorCode,
  //         type: 'personal'),
  //   ], iconType: 'Personal'),

  //   /// Finance Icons Lists
  //   IconType(icons: [
  //     IconDetails(
  //         iconValue: Icons.money.codePoint.toString(),
  //         title: 'Bank info',
  //         colourValue: selectedIconColorCode,
  //         type: 'finance'),
  //     IconDetails(
  //         iconValue: Icons.credit_card.codePoint.toString(),
  //         title: 'Credit Card',
  //         colourValue: selectedIconColorCode,
  //         type: 'finance'),
  //     IconDetails(
  //         iconValue: Icons.currency_rupee.codePoint.toString(),
  //         title: 'Rupees',
  //         colourValue: selectedIconColorCode,
  //         type: 'finance'),
  //     IconDetails(
  //         iconValue: Icons.currency_bitcoin.codePoint.toString(),
  //         title: 'Bitcoin',
  //         colourValue: selectedIconColorCode,
  //         type: 'finance'),
  //     IconDetails(
  //         iconValue: Icons.account_balance.codePoint.toString(),
  //         title: 'Account Balance',
  //         colourValue: selectedIconColorCode,
  //         type: 'finance'),
  //     IconDetails(
  //         iconValue: Icons.currency_pound.codePoint.toString(),
  //         title: 'CurrencyPound',
  //         colourValue: selectedIconColorCode,
  //         type: 'finance'),
  //     IconDetails(
  //         iconValue: Icons.attach_money_outlined.codePoint.toString(),
  //         title: 'Dollar',
  //         colourValue: selectedIconColorCode,
  //         type: 'finance'),
  //     IconDetails(
  //         iconValue: Icons.euro.codePoint.toString(),
  //         title: 'Euro',
  //         colourValue: selectedIconColorCode,
  //         type: 'finance'),
  //   ], iconType: 'finance'),

  //   /// Technology Icons Lists
  //   IconType(icons: [
  //     IconDetails(
  //         iconValue: Icons.language_outlined.codePoint.toString(),
  //         title: 'Website',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.wifi.codePoint.toString(),
  //         title: 'Wifi',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.security.codePoint.toString(),
  //         title: 'Security',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.email.codePoint.toString(),
  //         title: 'Email',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.computer.codePoint.toString(),
  //         title: 'Computer',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.phone.codePoint.toString(),
  //         title: 'Phone',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.phone_android.codePoint.toString(),
  //         title: 'Smart Phone',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.games.codePoint.toString(),
  //         title: 'Games',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.headphones.codePoint.toString(),
  //         title: 'HeadPhones',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.cloud.codePoint.toString(),
  //         title: 'Cloud Services',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.ondemand_video.codePoint.toString(),
  //         title: 'Youtube',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.radar_outlined.codePoint.toString(),
  //         title: 'EmailReader',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.router.codePoint.toString(),
  //         title: 'Router',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.facebook.codePoint.toString(),
  //         title: 'Facebook',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.apple.codePoint.toString(),
  //         title: 'Apple',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //     IconDetails(
  //         iconValue: Icons.print_rounded.codePoint.toString(),
  //         title: 'Printer',
  //         colourValue: selectedIconColorCode,
  //         type: 'Technology'),
  //   ], iconType: 'Technology'),
  // ];

  notificationWidget(
      {required BuildContext context,
      required List<Map<String, dynamic>> notification,
      required String title}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
          child: Text(
            title,
            style: const TextStyle(fontSize: 12),
          ),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * 0.3,
          child: ListView.builder(
              itemCount: notification.length,
              itemBuilder: (context, index) {
                return Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                  decoration: BoxDecoration(
                      border: Border.all(width: 0.5),
                      borderRadius: BorderRadius.circular(5)),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      const Icon(
                        FontAwesomeIcons.bell,
                        size: 12,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width - 100,
                        child: Text(
                          notification[index]["title"],
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 10),
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 4,
                        ),
                      ),
                      const Spacer(),
                      GestureDetector(
                        onTap: () {},
                        child: Icon(
                          notification[index]["icon2"],
                          size: 24,
                          color: notification[index]["isActive"]
                              ? Colors.blue
                              : const Color(0xffc6c9ce),
                        ),
                      ),
                    ],
                  ),
                );
              }),
        ),
      ],
    );
  }

  navigateTo(String route, {arguments}) {
    _logger.logObserve("Navigating to... $route");
    Get.offNamed(route, arguments: arguments);
    return true;
  }
}

class HorizontalLineWidget extends StatelessWidget {
  const HorizontalLineWidget({Key? key}) : super(key: key);

  /// Draw a horizontal screen-wise separator
  @override
  Widget build(context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.8,
        height: 1.0,
        color: (MediaQuery.of(context).platformBrightness == Brightness.dark
            ? Colors.black.withOpacity(.5)
            : Colors.white.withOpacity(.5)),
      ),
    );
  }
}

String generateRandomString(int len) {
  var r = math.Random();
  const chars =
      "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890";
  return List.generate(len, (index) => chars[r.nextInt(chars.length)]).join();
}

class ResponsiveWidget extends StatelessWidget {
  final bool mobile;

  const ResponsiveWidget({Key? key, required this.mobile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrains) {
      return Container();
    });
  }
}

Future<bool> onWillPop(
    BuildContext context, GlobalKey<ScaffoldState>? key) async {
  final LogUtil logger = LogUtil("onWillPop");
  logger.logVerbose("key value $key ");
  if (key != null) {
    logger.logTrace(
        "key value $Key and condition ${key.currentState!.isDrawerOpen}");
    if (key.currentState!.isDrawerOpen) {
      Navigator.of(context).pop();
      return false;
    } else {
      return (await showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text("S.current.areYouSureQ"),
              content: const Text("S.current.reallyExitQ"),
              actions: <Widget>[
                TextButton(
                  onPressed: () {
                    Navigator.of(context).pop(false);
                  },
                  child: const Text("S.current.strNo"),
                ),
                TextButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: const Text("S.current.strYes"),
                ),
              ],
            ),
          )) ??
          false;
    }
  } else {
    return (await showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text("S.current.areYouSureQ"),
            content: const Text("S.current.reallyExitQ"),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  if (key != null) {
                    if (key.currentState!.isDrawerOpen) {
                      Navigator.of(context).pop(false);
                    }
                  } else {
                    Navigator.of(context).pop(false);
                  }
                },
                child: const Text("S.current.strNo"),
              ),
              TextButton(
                onPressed: () => Navigator.of(context).pop(true),
                child: const Text("S.current.strYes"),
              ),
            ],
          ),
        )) ??
        false;
  }
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

/// String to bool
extension BoolParsing on String {
  bool parseBool() {
    return toLowerCase() == 'true';
  }
}

///First Letter capitalize
String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
