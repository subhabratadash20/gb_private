import 'package:gb_private/models/discus_form/discus_form_element.dart';
import 'package:gb_private/screens/widgets/view_attatchement.dart';

extension FormElementTypesExtension on FormElementTypes {
  String get title {
    switch (this) {
      case FormElementTypes.string:
        return 'Text';
      case FormElementTypes.email:
        return 'Email';
      case FormElementTypes.date:
        return 'Date';
      case FormElementTypes.dateOfIssuance:
        return 'Date Of Issuance';
      case FormElementTypes.dueDate:
        return 'Due Date';
      case FormElementTypes.amount:
        return 'Amount';
      case FormElementTypes.number:
        return 'Number';
      case FormElementTypes.password:
        return 'Password';
      case FormElementTypes.card:
        return 'Card';
      case FormElementTypes.website:
        return 'Website';
      case FormElementTypes.note:
        return 'Note';
      case FormElementTypes.name:
        return 'Name';
      case FormElementTypes.cvv:
        return 'CVV';
      case FormElementTypes.expiryDate:
        return 'Expiry Date';
      case FormElementTypes.address:
        return 'Address';
      case FormElementTypes.bankName:
        return 'Bank Name';
      case FormElementTypes.documentType:
        return 'Attachment';
      case FormElementTypes.account:
        return 'Account';
      case FormElementTypes.location:
        return 'Location';
      case FormElementTypes.trendingPsw:
        return 'Trending Psw';
      case FormElementTypes.accountHolderName:
        return 'Account holder name';

      default:
        return "";
    }
  }
}

extension ToEnum on String {
  toEnum() {
    switch (this) {
      case 'jpg':
        return ViewFileType.jpg;
      case 'png':
        return ViewFileType.png;
      case 'pdf':
        return ViewFileType.pdf;
      case 'jpeg':
        return ViewFileType.jpeg;
    }
  }
}
