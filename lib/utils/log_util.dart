import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:logging/logging.dart';

class LogUtil {
  final String name;
  static bool isInit = false;
  static const JsonEncoder _encoder = JsonEncoder.withIndent('  ');
  static const String _trace = "[TRACE]";
  static const String _verbose = "[VERBOSE]";
  static const String _debug = "[DEBUG]";
  static const String _observe = "[OBSERVE]";
  static const String _info = "[INFO]";
  static const String _warning = "[WARN]";
  static const String _error = "[ERROR]";
  static const String _pretty = "[PRETTY]";

  LogUtil(this.name) {
    if (!isInit) {
      isInit = true;
      debugPrint("[INFO] Logger initialized");
      Logger.root.level = Level.ALL;
      Logger.root.onRecord.listen(_recordHandler);
    }
  }

  static final _dateFormatter = DateFormat('H:m:s.S');
  static final Logger _logger = Logger("Flutter Enterprise App");

  void _recordHandler(LogRecord rec) {
    debugPrint('${_dateFormatter.format(rec.time)}: ${rec.message}');
  }

  static String ANSI_RESET = "\u001B[0m";
  static String ANSI_YELLOW = "\u001B[33m";

  void _prettyPrint(msg, severity, error, st, logFunction, prettyPrint) {
    if (prettyPrint) {
      String prettyPrintStr = _encoder.convert(msg);
      logFunction("\n-------------------$severity-------------------");
      logFunction("$name:\n$prettyPrintStr\n");

      if (error != null) {
        logFunction("\n-------------------ERROR-------------------");
        logFunction("error:", error, st);
        logFunction("\n-------------------END-ERROR-------------------");
      }
    } else {
      logFunction("$severity $name: $msg", error, st);
    }
  }

  /// Use this for iterations, data retrieval etc - items that are deemed running fine
  void logTrace(msg,
          [Object? error, StackTrace? st, bool prettyPrint = false]) =>
      _prettyPrint(msg, _trace, error, st, _logger.finest, prettyPrint);

  /// Use this for conditional outputs, especially those that are deemed to work
  void logVerbose(msg,
          [Object? error, StackTrace? st, bool prettyPrint = false]) =>
      _prettyPrint(msg, _verbose, error, st, _logger.finer, prettyPrint);

  /// Use this for helper messages - especially ones
  void logObserve(msg,
          [Object? error, StackTrace? st, bool prettyPrint = false]) =>
      _prettyPrint(msg, _observe, error, st, _logger.shout, prettyPrint);

  /// Use this for debug values & important info
  void logDebug(msg,
          [Object? error, StackTrace? st, bool prettyPrint = false]) =>
      _prettyPrint(msg, _debug, error, st, _logger.shout, prettyPrint);

  void logInfo(msg,
          [Object? error, StackTrace? st, bool prettyPrint = false]) =>
      _prettyPrint(msg, _info, error, st, _logger.info, prettyPrint);

  void logWarning(msg,
          [Object? error, StackTrace? st, bool prettyPrint = false]) =>
      _prettyPrint(msg, _warning, error, st, _logger.warning, prettyPrint);

  void logError(msg,
          [Object? error, StackTrace? st, bool prettyPrint = false]) =>
      _prettyPrint(msg, _error, error, st, _logger.severe, prettyPrint);

  void logPrettyJson(msg) =>
      _prettyPrint(msg, _pretty, null, null, _logger.fine, true);
}
