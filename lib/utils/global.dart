import 'dart:io';

import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:gb_private/models/user_session.dart';
import 'package:gb_private/models/user_session_proxy.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:intl/intl.dart';

class Global {
  static const String _me = "Globals";
  static final LogUtil _logger = LogUtil(_me);
  final Map valueMap = <String, Object>{};
  static final Global _instance = Global._internal();

  static String applicationClientName = "Discus Enterprise App";
  static String deviceInfoStr = "1";
  static int applicationId = 1;
  static int defaultPageSize = 30;
  static int offset = 10;

  /// Logged in user details
  static UserSession? currentUserSession;

  /// App Section
  static String sectionType = AppSectionType.collab.toString();

  static String baseLocalPath = 'BaseLocalPath';
  static const String authLabel = 'auth';
  static const String keySessionSiteId = "session_site_id";
  static const String keySessionUserName = "session_user_name";
  static const String keySessionUserFirstName = "session_user_first_name";
  static const String keySessionUserLastName = "session_user_last_name";
  static const String keySessionClientId = "session_client_id";
  static const String keySessionRememberMe = "session_remember_me";
  /// For a future feature to demonstrate when the user logged in the last time.
  static const String keySessionLoginTime = "session_login_time";
  static const String keySessionUserType = "session_user_type";
  static const String cookie = "session_user_cookie";


  /// Admin specific functionality will exist across the App
  static const String keySessionUserIsAdmin = "session_is_admin";
  static const String keySessionToken = "session_token";

  /// Session status
  static const String keySessionAuthState = "session_auth_state";
  static const int sessionAuthStateLoggedIn = 1;
  static const int sessionAuthStateLoggedOut = 0;
  static const int sessionAuthStateLockedOut = 2;

  static final String platform = kIsWeb
      ? "Web"
      : Platform.isAndroid
      ? "Android"
      : Platform.isIOS
      ? "iOS"
      : Platform.isWindows
      ? "Windows"
      : Platform.isMacOS
      ? "MaOS"
      : Platform.isLinux
      ? "Linux"
      : "Unknown";

  /// Login related
  static const String keyUserTypeDiscus = "user_type_discus";
  static const String keyUserTypeGoogle = "user_type_google";
  static const String keyUserTypeLinkedIn = "user_type_linkedin";
  static const String keyUserTypeApple = "user_type_apple";
  static UserSessionProxy? currentUser;
  /// To check the method of login & inform the server accordingly
  static final userType = {
    keyUserTypeDiscus: 1,
    keyUserTypeGoogle: 2,
    keyUserTypeLinkedIn: 3,
    keyUserTypeApple: 4,
  };
  // Splash Screen Image
  static const String splashScreenImage = 'assets/icons/screen-icon.png';

  // App Logo using MasterPage And
  static const String appLogo = 'assets/icons/transparent-icon.png';

  /// web
  static const String clientIdWeb ='1044202021334-7agcljh3eb2etl1cjgpaqlvis86g41as.apps.googleusercontent.com';
  static const String clientSecretWeb = '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';
  static const String redirectUriWeb = 'urn:ietf:wg:oauth:2.0:oob';

// dropbox
  static const String dropboxClientId = 'gb-pvt';
  static const String dropboxKey = '1lmkzbf8wfhcqef';
  static const String dropboxSecret = '529p4at479wjeqg';

  static const String defaultFontFamily = "Arimo";



// Auto-consume must be true on iOS.
// To try without auto-consume on another platform, change `true` to `false` here.
  static bool kAutoConsume = Platform.isIOS || true;

  /// Product Id
  static const String kConsumableId =
      'solutions.discus.apps.gbpvt.android.inapp.unlockpremium499';

  static const List<String> kProductIds = <String>[
    kConsumableId,
  ];




  /// one drive client id and redirected uri
  static const String clientId = 'e889fc15-2839-4611-9e09-5b100a58aa41';
  static const String redirectUri =
      'https://oauth.pstmn.io/v1/browser-callback';

  // static const String scope = 'Files.ReadWrite offline_access';
  static const String scope = 'Files.ReadWrite.All';

  ///  Default min rows of text area
  static const int minRowsForTextArea = 4;

  /// Default max length for login name
  static const int maxLengthLogin = 50;

  static const Color colorSeparator = Color(0xff3c5caf);

  static const String folderName = 'gb-pvt';
  static const String folderNameToStoreFileLocally = 'gb-pvt-store';

  factory Global() => _instance;

  Global._internal() {
    _instance.init().then((value) {
      _logger.logInfo("####Gloabls init done $value");
    });
  }

  Future<bool> init() async {
    return true;
  }

  static const String version = "1.1.0 (29)";
  static String keySessionUserId = "session_user_id";
  static String keySessionUserIdGreenBox = "keySessionUserIdGreenBox";

  static final dateFormatDateTimeShort = DateFormat("hh:mm a dd MMM yy");

// checkPlatformIsWeb() {
//   if (kIsWeb) {
//     return true;
//   }else{
//     return false;
//   }
// }
//
// checkPlatformIsAndroidOrIos() {
//   _logger.logDebug(
//       "Platform is ${Platform.operatingSystem} version ${Platform.operatingSystemVersion}");
//   return Platform.isAndroid || Platform.isIOS;
// }
}

enum AppSectionType { greenbox, collab, kriya, procurement, account, profile, subscription, todo}