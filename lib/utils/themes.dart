import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFF17A600);
const kSecondaryColor = Color(0xFF067D0F);
const kContentColorLightTheme = Color(0xFF212121);
const kContentColorDarkTheme = Color(0xFFFFFFFF);
const kSuccessColor = Color(0xFF067D0F);
const kWarningColor = Color(0xFFF3BB1C);
const kErrorColor = Color(0xFFF03738);
const kDividerColor = Colors.white54;
const kScaffoldBackgroundColorLightTheme = Colors.white;
const widgetIcon = Color(0xffc6c9ce);

/// Main DashBoard Card Color
const kIconColor = Color(0xFF067D0F);
const kIconColor1 = Color(0xFFF3BB1C);
const kIconColor2 = Color(0xFFFFFFFF);
const kDefaultPadding = 10.0;

// this color Code we are using a selectedIcon Page and UiUtils
const selectedIconColorCode = 0xFF067D0F;

ThemeData lightThemeData() {
  return ThemeData.light().copyWith(
    primaryColor: kPrimaryColor,
    primaryColorDark: Colors.black26,
    primaryColorLight: Colors.white,
    iconTheme: const IconThemeData(color: kPrimaryColor),
    scaffoldBackgroundColor: kScaffoldBackgroundColorLightTheme,
    appBarTheme: appBarTheme,
    //iconTheme: IconThemeData(color: kContentColorLightTheme),
    colorScheme: const ColorScheme.light(
      primary: kPrimaryColor,
      secondary: kSecondaryColor,
      error: kErrorColor,
    ),
    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    /// TODO TextFont and fontFamily will reflect in all screens
    textTheme: const TextTheme(
      displayLarge: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Color(0x003c3c3d),
          fontSize: 72.0,
          fontWeight: FontWeight.bold,
          decoration: TextDecoration.none),
      displayMedium: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.black54,
          decoration: TextDecoration.none),
      displaySmall: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.black54,
          decoration: TextDecoration.none),
      headlineMedium: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.black54,
          decoration: TextDecoration.none),
      headlineSmall: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.black87,
          decoration: TextDecoration.none),
      titleLarge: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          fontSize: 36.0,
          color: Colors.black87,
          decoration: TextDecoration.none),
      bodyLarge: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.black87,
          decoration: TextDecoration.none),
      bodyMedium: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          fontSize: 14.0,
          color: Colors.black87,
          decoration: TextDecoration.none),
      titleMedium: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.black87,
          decoration: TextDecoration.none),
      titleSmall: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.black,
          decoration: TextDecoration.none),
      bodySmall: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.black54,
          decoration: TextDecoration.none),
      labelLarge: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.black87,
          decoration: TextDecoration.none),
      labelSmall: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.black,
          decoration: TextDecoration.none),
    ),

    /// TODO Card  ThemeData will reflect in all screens
    cardTheme: CardTheme(
      margin: const EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 8),
      elevation: 6.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      //  backgroundColor: Colors.white,
      selectedItemColor: kContentColorLightTheme.withOpacity(0.7),
      unselectedItemColor: kContentColorLightTheme.withOpacity(0.32),
      selectedIconTheme: const IconThemeData(color: kPrimaryColor),
      showUnselectedLabels: true,
    ),
  );
}

ThemeData darkThemeData() {
  // ByDefault flutter provided us light and dark theme
  // we just modify it as our need
  return ThemeData.dark().copyWith(
    primaryColor: kPrimaryColor,
    scaffoldBackgroundColor: kContentColorLightTheme,
    appBarTheme: appBarTheme,
    primaryColorDark: const Color(0xffFFFFFF),
    primaryColorLight: Colors.black26,
    iconTheme: const IconThemeData(color: kContentColorDarkTheme),
    colorScheme: const ColorScheme.dark().copyWith(
      primary: kPrimaryColor,
      secondary: kSecondaryColor,
      error: kErrorColor,
    ),

    /// TODO TextFont and fontFamily will reflect in all screens
    textTheme: const TextTheme(
      displayLarge: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          fontSize: 72.0,
          fontWeight: FontWeight.bold,
          decoration: TextDecoration.none),
      displayMedium: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          decoration: TextDecoration.none),
      displaySmall: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          decoration: TextDecoration.none),
      headlineMedium: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          decoration: TextDecoration.none),
      headlineSmall: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          decoration: TextDecoration.none),
      titleLarge: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          fontSize: 36.0,
          color: Colors.white70,
          decoration: TextDecoration.none),
      bodyLarge: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          decoration: TextDecoration.none),
      bodyMedium: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          fontSize: 14.0,
          color: Colors.white70,
          decoration: TextDecoration.none),
      titleMedium: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          decoration: TextDecoration.none),
      titleSmall: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          decoration: TextDecoration.none),
      bodySmall: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          decoration: TextDecoration.none),
      labelLarge: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          decoration: TextDecoration.none),
      labelSmall: TextStyle(
          fontFamily: 'Century',
          inherit: true,
          color: Colors.white70,
          decoration: TextDecoration.none),
    ),

    /// TODO Card  ThemeData will reflect in all screens
    cardTheme: CardTheme(
      margin: const EdgeInsets.only(top: 8, left: 8, right: 8, bottom: 8),
      elevation: 6.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      // backgroundColor: kContentColorLightTheme,
      selectedItemColor: Colors.white70,
      unselectedItemColor: kContentColorDarkTheme.withOpacity(0.32),
      selectedIconTheme: const IconThemeData(color: kPrimaryColor),
      showUnselectedLabels: true,
    ),
  );
}

const appBarTheme = AppBarTheme(centerTitle: false, elevation: 0);
const tabLinkStyle =
    TextStyle(fontWeight: FontWeight.w500, fontFamily: 'Century');

const taglineText = TextStyle(color: Colors.grey, fontFamily: 'Century');
const categoryText = TextStyle(
    color: Color(0xff444444),
    fontWeight: FontWeight.w700,
    fontFamily: 'Century');

const inputFieldTextStyle =
    TextStyle(fontFamily: 'Century', fontWeight: FontWeight.w400, fontSize: 14);

const inputFieldHintTextStyle =
    TextStyle(fontFamily: 'Century', color: Colors.black38, fontSize: 14);

const inputFieldDialogBoxAppBar =
    TextStyle(fontFamily: 'Century', color: Colors.white, fontSize: 14);

const inputFieldHintDarkTextStyle =
    TextStyle(fontFamily: 'Century', color: Colors.white70, fontSize: 14);

const inputTitleTextStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.bold,
    color: Colors.black54,
    fontFamily: 'Century');

const inputTitleDarkTextStyle = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.bold,
    color: Colors.white70,
    fontFamily: 'Century');
const inputFieldPasswordTextStyle = TextStyle(
    fontFamily: 'Century', fontWeight: FontWeight.w500, letterSpacing: 3);

const inputFieldHintPasswordTextStyle = TextStyle(
    fontFamily: 'Century', color: Color(0xff444444), letterSpacing: 1);

///////////////////////////////////
/// BOX DECORATION STYLES
//////////////////////////////////

const authPlateDecoration = BoxDecoration(
    color: kContentColorDarkTheme,
    boxShadow: [
      BoxShadow(
          color: Color.fromRGBO(0, 0, 0, .1),
          blurRadius: 10,
          spreadRadius: 5,
          offset: Offset(0, 1))
    ],
    borderRadius: BorderRadiusDirectional.only(
        bottomEnd: Radius.circular(20), bottomStart: Radius.circular(20)));

/////////////////////////////////////
/// INPUT FIELD DECORATION STYLES
////////////////////////////////////
inputFieldFocusedBorderStyle(Color color) {
  return OutlineInputBorder(
      borderRadius: const BorderRadius.all(Radius.circular(6)),
      borderSide: BorderSide(
        color: color,
      ));
}
// const inputFieldFocusedBorderStyle = OutlineInputBorder(
//     borderRadius: BorderRadius.all(Radius.circular(6)),
//     borderSide: BorderSide(
//       color: kPrimaryColor,
//     ));

const inputFieldDefaultBorderStyle = OutlineInputBorder(
  gapPadding: 0,
  borderRadius: BorderRadius.all(
    Radius.circular(6),
  ),
);
