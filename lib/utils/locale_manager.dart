import 'package:flag/flag.dart';
import 'package:flutter/material.dart';
import 'package:gb_private/generated/l10n.dart';
import 'package:gb_private/utils/log_util.dart';

class LocaleManager {
  static final LocaleManager _instance = LocaleManager._internal();
  LocaleManager._internal() {
    _logger.logInfo("LocaleManager initialized.");
  }
  factory LocaleManager() => _instance;

  static final LogUtil _logger = LogUtil("LocaleManager");

  static void setLocale(Locale locale) {
    S.load(locale);
    _logger.logInfo('Locale changed to $locale');
  }

  static Flag getFlag(Locale countryLocale,
      [double height = 25, BoxFit fit = BoxFit.contain]) {
    return //Flexible(
      //child: Flag(
      Flag.fromString(
        countryLocale.countryCode ?? "US",
        height: height, // ?? 25,
        fit: fit,
        //),
      );
  }

  /// Show dropdown items
  static getDropDownItems() {
    return LocaleManager.getSupportedLocales()
        .map<DropdownMenuItem<Locale>>((Locale value) {
      return DropdownMenuItem<Locale>(
        value: value,
        child: Text(value.languageCode),
      );
    }).toList();
  }

  /// Render a language selection drop-down.
  /// Changes to the drop-down will change the App language.
  static renderLanguageDropdown(setState) {
    return Flexible(
      child: DropdownButton<Locale>(
        autofocus: true,
        icon: Flexible(
          fit: FlexFit.loose,
          child: LocaleManager.getFlag(LocaleManager.selectedLocale, 25),
        ),
        style: const TextStyle(color: Colors.deepPurple),
        underline: Container(
          height: 2,
          color: Colors.deepPurpleAccent,
        ),
        onChanged: (newValue) {
          setState(() {
            Locale newLocale = newValue ?? localeUsa;
            LocaleManager.selectedLocale = newLocale;
            LocaleManager.setLocale(newLocale);
          });
        },
        value: LocaleManager.selectedLocale,
        items: LocaleManager.getDropDownItems(),
      ),
    );
  }

  static Locale get localeUsa => const Locale('en', 'US');
  static Locale get localeIndia => const Locale('hi', 'IN');
  static Locale get localeArabic => const Locale('ar', 'AE');
  static Locale get localeSpanish => const Locale('es', 'ES');
  static Locale get localeFrench => const Locale('fr', 'FR');

  static Locale selectedLocale = localeUsa;

  static List<Locale> getSupportedLocales() {
    return [localeUsa, localeIndia, localeArabic, localeSpanish, localeFrench];
  }
}