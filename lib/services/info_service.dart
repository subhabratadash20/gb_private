import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:crypto/crypto.dart';
import 'package:encrypt/encrypt.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:gb_private/models/login/login.dart';
import 'package:gb_private/screens/widgets/summary_card.dart';
import 'package:gb_private/utils/global.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:gb_private/utils/ui_utils.dart';

class InfoService {
  static final InfoService _singleton = InfoService._internal();

  factory InfoService() {
    return _singleton;
  }

  InfoService._internal();

  static const String _me = 'InfoService';
  static final LogUtil _logger = LogUtil(_me);
  final UIUtils uiUtils = UIUtils(_logger);
  final _storage = const FlutterSecureStorage();
  String? checkSavedViewType = '';

  // final googleSignIn = GoogleSignIn.standard(scopes: [
  //   drive.DriveApi.driveAppdataScope,
  //   drive.DriveApi.driveFileScope,
  // ]);

  // final googleSignIn = GoogleSignIn(scopes: [
  //   drive.DriveApi.driveAppdataScope,
  //   drive.DriveApi.driveFileScope,
  //   drive.DriveApi.driveReadonlyScope,
  // ],
  //     clientId: kIsWeb ? Global.clientIdWeb : null
  //   // clientId: (Platform.isAndroid || Platform.isIOS) ? Global.clientIDAndroid :Global.clientIDIos
  // );

  // drive.File driveFile = drive.File();
  File? getSavedFile;
  String? combinedJsonData;

  // StreamController for category updates
  final StreamController<List<LoginModel>> _loginsController =
      StreamController<List<LoginModel>>.broadcast();

  Stream<List<LoginModel>> get loginsStream => _loginsController.stream;

  // Update categories and notify listeners
  void updateLogins(List<LoginModel> newLogins) {
    _loginsController.add(newLogins);
  }

  /// get all saved category data (If we pass key it will return subcategory)
  // Future<List<LoginModel>> readAllLogins() async {
  //   List list = [];
  //   var loginJson = await read("Logins");
  //   if (loginJson != null && loginJson != 'null') {
  //     list = jsonDecode(loginJson);
  //   }
  //   // _logger.logDebug('encoded category json $categoryJson');
  //   return list.map((e) => LoginModel.fromJson(e)).toList();
  // }

  // save login list
  Future<List<int>> saveLogins(List<LoginModel> loginList) async {
    // var loginsJson = jsonEncode(loginList.map((e) => e.toJson()).toList());
    // await save('Logins', loginsJson);
    _logger.logInfo("${await read("Logins")}");
    List<int> settingJson = await combineJsonAndSaveInFile();
    return settingJson;
  }

  /// save sub category list
  Future<List<int>> saveSubCategoryList(key, subCategoryList) async {
    var categoriesJson =
        jsonEncode(subCategoryList.map((e) => e.toJson()).toList());
    await save(key, categoriesJson);
    // saveCategoryLocallyInFile(categoriesJson);
    _logger.logInfo("${await read(key)}");
    List<int> settingJson = await combineJsonAndSaveInFile();
    return settingJson;
  }

  // Combine category json and sub category json
  Future<dynamic> combineJsonAndSaveInFile() async {
    var settingsCategoryJson = await read("Logins");
    if (settingsCategoryJson != null) {
      var settingsCategoryList = jsonDecode(settingsCategoryJson);
      for (var element in settingsCategoryList) {
        var categoryItemListJson = await read("category_${element["id"]}");
        if (categoryItemListJson != null) {
          element["subCategoryList"] = jsonDecode(categoryItemListJson);
        }
      }
      combinedJsonData = jsonEncode(settingsCategoryList);

      // _logger.logDebug(combinedJsonData);
      var result = await saveJsonInFile(combinedJsonData);
      return result;
    }
    return null;
  }

  saveJsonInFile(settingJson) async {
    // Encrypt Data
    var masterPassword = await read(Global.keySessionUserId);
    var masterPasswordMD5 = md5.convert(utf8.encode(masterPassword)).toString();
    List<int> bytesToEncrypt = utf8.encode(settingJson);
    final key = Key.fromUtf8(masterPasswordMD5);
    final encrypt = Encrypter(AES(key));
    final iv = IV.fromLength(16);
    final encrypted = encrypt.encryptBytes(bytesToEncrypt, iv: iv);

    String folderPath = await uiUtils
        .createFolderInAppDocDir(Global.folderNameToStoreFileLocally);
    String originalFileName = "combinedJsonFile.json";

    File fileCreated =
        await createFile('$folderPath$originalFileName', encrypted.bytes);
    getSavedFile = fileCreated;

    // _logger.logDebug(encrypted);

    return encrypted.bytes;
  }

  File? savedFile() {
    return getSavedFile;
  }

  /// Method to get decrypted json file
  Future<void> restoreJson(File savedFile) async {
    Uint8List fileBytes = savedFile.readAsBytesSync();
    var masterPassword = await read(Global.keySessionUserId);
    String masterPasswordMD5 =
        md5.convert(utf8.encode(masterPassword)).toString();

    Key key = Key.fromUtf8(masterPasswordMD5);
    Encrypted encrypted = Encrypted(fileBytes);
    IV iv = IV.fromLength(16);
    List<int> decrypted = Encrypter(AES(key)).decryptBytes(encrypted, iv: iv);
    // _logger.logDebug(decrypted);
    var decodedJson = utf8.decode(decrypted);
    var combinedList = jsonDecode(decodedJson);

    // _logger.logDebug('saveCategoryLocallyInFile $decodedJson');

    // Restore Subcategories
    for (Map element in combinedList) {
      await save(
          "category_${element["id"]}", jsonEncode(element["subCategoryList"]));
      element.remove("subCategoryList");
    }
    // Restore Settings Categories
    await save("SettingsCategory", jsonEncode(combinedList));
  }

  /// get all categories from secure storage
  Future<dynamic> readAll() async {
    final all = await _storage.readAll(
      aOptions: _getAndroidOptions(),
    );
    return all;
  }

  // checkPlatformIsWeb() {
  //   if (kIsWeb) {
  //     return true;
  //   }else{
  //     return false;
  //   }
  // }
  //
  // checkPlatformIsAndroidOrIos() {
  //   _logger.logDebug(
  //       "Platform is ${Platform.operatingSystem} version ${Platform.operatingSystemVersion}");
  //   return Platform.isAndroid || Platform.isIOS;
  // }

  /// get single categories from secure storage
  Future<dynamic> read(String key) async {
    final single = await _storage.read(
        key: key, aOptions: _getAndroidOptions(), webOptions: _getWebOptions());
    return single;
  }

  /// save view type like list or grid in secure storage
  Future<ViewType>? getSaveViewType(viewType) async {
    checkSavedViewType = await read(viewType);

    if (checkSavedViewType == null) {
      save(viewType, ViewType.list.name);
      return viewType = ViewType.list;
    }
    if (checkSavedViewType == ViewType.list.name) {
      save(viewType, ViewType.list.name);
      return viewType = ViewType.list;
    }

    if (checkSavedViewType == ViewType.grid.name) {
      save(viewType, ViewType.grid.name);
      return viewType = ViewType.grid;
    }
    return ViewType.list;
  }

  // delete category
  Future<void> delete(key) async {
    await _storage.delete(key: key);
  }

  // save data
  Future<void> save(key, value) async {
    await _storage.write(
      key: key,
      value: value,
      aOptions: _getAndroidOptions(),
    );
    readAll();
  }

  AndroidOptions _getAndroidOptions() => const AndroidOptions(
        encryptedSharedPreferences: true,
      );

  WebOptions _getWebOptions() => const WebOptions();

  Future<File> createFile(filePath, data) async {
    var oldFile = File(filePath);
    if (await oldFile.exists()) {
      await oldFile.delete();
    }
    // var file = File(filePath + ".swp");
    var file = File(filePath);
    await file.create();
    await file.writeAsBytes(data);
    // var renameFile = file.renameSync(filePath);
    _logger.logDebug("File Path: $filePath");
    _logger.logDebug("File Size: ${file.lengthSync()}");
    return file;
  }
}
