import 'package:gb_private/routes.dart';
import 'package:gb_private/services/info_service.dart';
import 'package:gb_private/utils/global.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:gb_private/utils/ui_utils.dart';
import 'package:local_auth/local_auth.dart';

class AuthService {
  static final AuthService _instance = AuthService._internal();

  factory AuthService() => _instance;

  AuthService._internal();

  final LocalAuthentication auth = LocalAuthentication();
  static const String _me = 'AuthService';
  static final LogUtil _logger = LogUtil(_me);
  final UIUtils uiUtils = UIUtils(_logger);
  InfoService infoService = InfoService();

  Future<String> getSaveMasterPassword(String key) async {
    await InfoService().save(Global.keySessionUserId, key);
    return Routers.mainScreen;
  }
}
