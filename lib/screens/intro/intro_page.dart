import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/routes.dart';
import 'package:gb_private/utils/themes.dart';
import 'package:get/get.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({super.key});

  @override
  State<IntroPage> createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(const Duration(seconds: 2), () {
      Get.offAllNamed(Routers.splashRoute);
      // Get.offAllNamed(Routers.authenticationRoute);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: Center(
        child: Container(
            height: 250,
            width: 250,
            decoration: const BoxDecoration(color: Colors.white30,
                borderRadius: BorderRadius.all(Radius.circular(250))),
            child: Padding(
              padding: const EdgeInsets.all(48),
              child: SvgPicture.asset(
                'assets/images/Logo-white.svg',
              ),
            )),
      ),
    );
  }
}
