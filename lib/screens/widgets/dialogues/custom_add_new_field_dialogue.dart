import 'package:flutter/material.dart';

import '../../../models/addfield_model.dart';
import '../../../models/drag_drop_data_model.dart';

class CustomAddNewFieldDialogue extends StatefulWidget {
  final List<ImageData> itemList;
  // List<DragDropData> listForAddingNewField;
  final Function(DragDropData) onSelect;
  const CustomAddNewFieldDialogue(
      {super.key,
      required this.itemList,
      // required this.listForAddingNewField,
      required this.onSelect});

  @override
  State<CustomAddNewFieldDialogue> createState() =>
      _CustomAddNewFieldDialogueState();
}

class _CustomAddNewFieldDialogueState extends State<CustomAddNewFieldDialogue> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Container(
        // height: 250,
        width: double.infinity,
        padding: const EdgeInsets.all(
          20,
        ),
        decoration: BoxDecoration(
            color: const Color(0xFFFFFFFF),
            borderRadius: BorderRadius.circular(24)),
        child: Wrap(
          // alignment: WrapAlignment.spaceAround,
          spacing: 12,
          children: List.generate(
            widget.itemList.length,
            (index) => FilterChip(
              // padding: EdgeInsets.only(right: 5),
              labelStyle: const TextStyle(color: Color(0xFF6E8A9E)),
              label: Row(
                mainAxisSize: MainAxisSize.min,
                //crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    widget.itemList[index].imagePath,
                    width: 15,
                    height: 15,
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Text(widget.itemList[index].name,
                      style: const TextStyle(
                        fontSize: 11,
                        color: Color(0xFF6E8A9E),
                        fontFamily: 'Poppins',
                      )),
                ],
              ),
              onSelected: (bool selected) {
                Navigator.pop(context);
                widget.onSelect(DragDropData(
                    name: widget.itemList[index].name,
                    controller: TextEditingController(),
                    type: widget.itemList[index].type,
                    isObscure: widget.itemList[index].type == "pin",
                    imagePath: widget.itemList[index].imagePath));
              },
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(24.0),
                side: const BorderSide(
                  color: Color(0xFFA7C3D7),
                  width: 0.5,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
