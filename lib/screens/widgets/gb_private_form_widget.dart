import 'package:flutter/material.dart';
import 'package:gb_private/models/discus_form/discus_form_element.dart';
import 'package:gb_private/screens/widgets/fields/custom_text_field.dart';
import 'package:gb_private/screens/widgets/fields/email_text_field.dart';
import 'package:gb_private/utils/log_util.dart';

class DiscusFormWidget extends StatefulWidget {
  final List<DiscusFormElement> elementData;
  final bool viewCategory;
  final bool? settingFlag;
  final String? catName;

  const DiscusFormWidget(
      {Key? key,
      required this.elementData,
      required this.viewCategory,
      this.settingFlag,
      this.catName})
      : super(key: key);

  @override
  State<DiscusFormWidget> createState() => _DiscusFormWidgetState();
}

class _DiscusFormWidgetState extends State<DiscusFormWidget> {
  static const String _me = "DiscusFormWidget";
  static final LogUtil _logger = LogUtil(_me);

  @override
  Widget build(BuildContext context) {
    _logger.logTrace("Received form: ${widget.elementData}");
    return ListView.builder(
      itemCount: widget.elementData.length,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) =>
          findAndRenderElement(widget.elementData[index]),
    );
  }

  Widget findAndRenderElement(DiscusFormElement elementData) {
    try {
      Widget? renderWidget;
      var elementType = elementData.type;
      elementType = elementType.replaceFirst("-", "_");
      FormElementTypes enumVal = FormElementTypes.values
          .firstWhere((element) => element.name == elementType);

      switch (enumVal) {
        case FormElementTypes.string:
          renderWidget = Row(
            children: [
              Expanded(
                  child: CustomTextField(
                      element: elementData,
                      settingFlag: widget.settingFlag,
                      onSelectFieldType: (FormElementTypes dropDownValue) {
                        dateFormat(elementData, dropDownValue);
                        setState(() {
                          elementData.type = dropDownValue.name;
                        });
                      }))
            ],
          );
          break;
        case FormElementTypes.email:
          renderWidget = Row(
            children: [
              Expanded(
                child: EmailTextField(
                  element: elementData,
                  settingFlag: widget.settingFlag,
                  onSelectFieldType: (FormElementTypes dropDownValue) {
                    dateFormat(elementData, dropDownValue);
                    setState(() {
                      elementData.type = dropDownValue.name;
                    });
                  },
                ),
              ),
            ],
          );
          break;
        default:
      }
      return renderWidget ??
          Text(
            'No widget Applied',
            style: TextStyle(color: Theme.of(context).primaryColorDark),
          );
    } catch (error) {
      return Text(
        "Error data found.",
        style: TextStyle(color: Theme.of(context).primaryColorDark),
      );
    }
  }

  void dateFormat(element, FormElementTypes dropDownValue) {
    switch (dropDownValue) {
      case FormElementTypes.date:
        element.dateFormat = 'dd/MM/yyyy';
        break;
      case FormElementTypes.expiryDate:
        element.dateFormat = 'MM/yyyy';
        break;
      case FormElementTypes.string:
        break;
      case FormElementTypes.email:
        break;
      case FormElementTypes.number:
        break;
      case FormElementTypes.password:
        break;
      case FormElementTypes.card:
        break;
      case FormElementTypes.website:
        break;
      case FormElementTypes.note:
        break;
      case FormElementTypes.name:
        break;
      case FormElementTypes.cvv:
        break;
      case FormElementTypes.address:
        break;
      case FormElementTypes.bankName:
        break;
      case FormElementTypes.documentType:
        break;
      case FormElementTypes.account:
        break;
      case FormElementTypes.location:
        break;
      case FormElementTypes.trendingPsw:
        break;
      case FormElementTypes.accountHolderName:
        // TODO: Handle this case.
        break;
      case FormElementTypes.dateOfIssuance:
        element.dateFormat = 'dd/MM/yyyy';
        break;
      case FormElementTypes.dueDate:
        element.dateFormat = 'dd/MM/yyyy';
        break;
      case FormElementTypes.amount:
        // TODO: Handle this case.
        break;
    }
  }
}
