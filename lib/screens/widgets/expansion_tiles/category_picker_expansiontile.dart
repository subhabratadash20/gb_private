import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CategoryPickerExpansionTile extends StatefulWidget {
  final List<String> categoryDataList;
  final String Function(String selectedItem)? onSelect;
  const CategoryPickerExpansionTile(
      {super.key, required this.categoryDataList, this.onSelect});

  @override
  State<CategoryPickerExpansionTile> createState() =>
      _CategoryPickerExpansionTileState();
}

class _CategoryPickerExpansionTileState
    extends State<CategoryPickerExpansionTile> {
  ExpansionTileController expansionTileController = ExpansionTileController();
  bool isExpanded = false;
  String selectedItem = "";
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      selectedItem = widget.categoryDataList[0];
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        dividerColor: Colors.transparent,
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
                color: Color(
                  0xB3ECF4FF,
                ),
                blurRadius: 20,
                offset: Offset(0, 4))
          ],
        ),
        child: ExpansionTile(
          trailing: RotatedBox(
            quarterTurns: isExpanded ? 3 : 2,
            child: SvgPicture.asset(
              'assets/icons/arrow-left.svg',
            ),
          ),
          onExpansionChanged: (x) {
            setState(() {
              isExpanded = x;
            });
          },

          maintainState: true,
          controller: expansionTileController,

          collapsedBackgroundColor: Colors.white,
          collapsedShape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          // tilePadding: EdgeInsets.zero,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                'Categories',
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 14,
                    fontFamily: 'Poppins',
                    color: Color(0xFF585858)),
              ),
              // if (expansionTileController.isExpanded)
              Text(
                selectedItem,
                style: const TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 14,
                  fontFamily: 'Poppins',
                  color: Color(0xFF2EBA0B),
                ),
              ),
            ],
          ),

          children: List.generate(
            widget.categoryDataList.length,
            (index) => Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const Divider(
                  thickness: 2,
                  color: Color(
                    0xB3ECF4FF,
                  ),
                  // height: 0,
                ),
                ListTile(
                  onTap: () {
                    selectedItem = widget.categoryDataList[index];
                    // setState(() {
                    if (expansionTileController.isExpanded) {
                      expansionTileController.collapse();
                    }
                    widget.onSelect != null
                        ? widget.onSelect!(selectedItem)
                        : null;
                    // });
                  },
                  leading: SvgPicture.asset(
                    "assets/icons/${widget.categoryDataList[index]}.svg",
                  ),
                  title: Text(
                    widget.categoryDataList[index],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
