import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/models/login/login.dart';
import 'package:gb_private/screens/login/login_detail.dart';

class LoginCard extends StatefulWidget {
  final List<LoginModel> logins;

  const LoginCard({Key? key, required this.logins}) : super(key: key);

  @override
  State<LoginCard> createState() => _LoginCardState();
}

class _LoginCardState extends State<LoginCard> {
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: widget.logins.length,
      separatorBuilder: (context, index) => const SizedBox(
        height: 14,
      ),
      itemBuilder: (context, index) {
        log(widget.logins[index].profilePic.toString());

        return InkWell(
          onTap: () {
            debugPrint("${widget.logins[index].icons}");
            // print(widget.logins[index].toJson());
            // Get.toNamed(
            //   Routers.loginDetail,
            //   arguments: {widget.logins[index], index},
            // );
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (_) => LoginDetail(
                    loginModel: widget.logins[index], boxItemIndex: index),
              ),
            );
          },
          child: Container(
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
                // Set color based on index
                color: Colors.white,
                // color: Colors.pinkAccent,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.06), // Shadow color
                    spreadRadius: 4, // Spread radius
                    blurRadius: 4, // Blur radius
                    offset: const Offset(0, 3), // Offset in the x, y direction
                  ),
                ],
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              children: [
                SizedBox(
                  height: 50,
                  width: 50,
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(4),
                      // backgroundColor: Colors.transparent,
                      child: widget.logins[index].profilePic == ''
                          ? Image.asset(
                              "assets/images/global-fill.png",
                              fit: BoxFit.cover,
                            )
                          : widget.logins[index].profilePic![0] == "P"
                              ? SvgPicture.memory(
                                  const Base64Decoder().convert(
                                      widget.logins[index].profilePic!),
                                  fit: BoxFit.cover,
                                )
                              : Image.memory(
                                  const Base64Decoder().convert(
                                      widget.logins[index].profilePic!),
                                  fit: BoxFit.cover,
                                )),
                ),
                const SizedBox(width: 8),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      widget.logins[index].title,
                      style: const TextStyle(
                        fontFamily: 'Poppins',
                        color: Color(0xff585858),
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                      ),
                    ),
                    Text(
                      widget.logins[index].website,
                      style: const TextStyle(
                        fontFamily: 'Poppins',
                        color: Color(0xff828282),
                        fontWeight: FontWeight.w400,
                        fontSize: 14,
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                Row(
                  // mainAxisSize: MainAxisSize.min,
                  children: [
                    // const SizedBox(width: 16),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          widget.logins[index].isFavorite =
                              !widget.logins[index].isFavorite;
                          widget.logins[index].save();
                        });
                      },
                      child: Transform.scale(
                        scale: 0.8,
                        child: widget.logins[index].isFavorite
                            ? SvgPicture.asset(
                                'assets/icons/Star-Yellow.svg',
                                fit: BoxFit.none,
                              )
                            : SvgPicture.asset(
                                'assets/icons/Star-Yellow.svg',
                                fit: BoxFit.none,
                                colorFilter: const ColorFilter.mode(
                                    Colors.grey, BlendMode.srcIn),
                              ),
                      ),
                    ),
                    // IconButton(
                    //   padding: EdgeInsets.zero,
                    //   onPressed: () {
                    //     setState(() {
                    //       widget.logins[index].isFavorite =
                    //           !widget.logins[index].isFavorite;
                    //       widget.logins[index].save();
                    //     });
                    //   },
                    //   icon: Transform.scale(
                    //     scale: 0.8,
                    //     child: widget.logins[index].isFavorite
                    //         ? SvgPicture.asset(
                    //             'assets/icons/Star-Yellow.svg',
                    //             fit: BoxFit.none,
                    //           )
                    //         : SvgPicture.asset(
                    //             'assets/icons/Star-Yellow.svg',
                    //             fit: BoxFit.none,
                    //             colorFilter: const ColorFilter.mode(
                    //                 Colors.grey, BlendMode.srcIn),
                    //           ),
                    //   ),
                    // ),
                    // Triple dots
                    // const SizedBox(width: 16),
                    PopupMenuButton<String>(
                      padding: EdgeInsets.zero,
                      itemBuilder: (context) => [
                        PopupMenuItem(
                          value: 'Delete',
                          child: const Text('Delete'),
                          onTap: () {
                            widget.logins[index].delete();
                          },
                        ),
                        // const PopupMenuItem(
                        //   value: 'option2',
                        //   child: Text('Option 2'),
                        // ),
                        // const PopupMenuItem(
                        //   value: 'option3',
                        //   child: Text('Option 3'),
                        // ),
                      ],
                      icon: SvgPicture.asset(
                        'assets/icons/three_dot.svg',
                      ),
                      onSelected: (value) {
                        // Handle menu item selection
                      },
                    ),
                  ],
                ),
              ],
            ),
            // child: ListTile(
            //   onTap: () {
            //     // print(widget.logins[index].toJson());
            //     Get.toNamed(Routers.loginDetail, arguments: widget.logins[index]);
            //   },
            //   leading: Transform.scale(
            //     scale: 1,
            //     child: CircleAvatar(
            //         backgroundColor: Colors.transparent,
            //         backgroundImage: widget.logins[index].icons == null
            //             ? const AssetImage("assets/images/global-fill.png")
            //             : MemoryImage(const Base64Decoder()
            //                     .convert(widget.logins[index].icons!.first!))
            //                 as ImageProvider),
            //   ),
            //   title: Text(widget.logins[index].title),
            //   subtitle: Text(widget.logins[index].website),
            //   contentPadding: EdgeInsets.zero,
            //   // i
            //   trailing: Padding(
            //     padding: const EdgeInsets.all(8.0),
            //     child: Row(
            //       mainAxisSize: MainAxisSize.min,
            //       children: [
            //         // const SizedBox(width: 16),

            //         IconButton(
            //           onPressed: () {
            //             setState(() {
            //               widget.logins[index].isFavorite =
            //                   !widget.logins[index].isFavorite;
            //               widget.logins[index].save();
            //             });
            //           },
            //           icon: Transform.scale(
            //             scale: 0.8,
            //             child: widget.logins[index].isFavorite
            //                 ? SvgPicture.asset(
            //                     'assets/icons/Star-Yellow.svg',
            //                     fit: BoxFit.none,
            //                   )
            //                 : SvgPicture.asset(
            //                     'assets/icons/Star-Yellow.svg',
            //                     fit: BoxFit.none,
            //                     colorFilter: const ColorFilter.mode(
            //                         Colors.grey, BlendMode.srcIn),
            //                   ),
            //           ),
            //         ),
            //         // Triple dots
            //         // const SizedBox(width: 16),
            //         PopupMenuButton<String>(
            //           itemBuilder: (context) => [
            //             PopupMenuItem(
            //               value: 'Delete',
            //               child: const Text('Delete'),
            //               onTap: () {
            //                 widget.logins[index].delete();
            //               },
            //             ),
            //             const PopupMenuItem(
            //               value: 'option2',
            //               child: Text('Option 2'),
            //             ),
            //             const PopupMenuItem(
            //               value: 'option3',
            //               child: Text('Option 3'),
            //             ),
            //           ],
            //           icon: const Icon(Icons.more_vert),
            //           onSelected: (value) {
            //             // Handle menu item selection
            //           },
            //         ),
            //       ],
            //     ),
            //   ),
            // ),
          ),
        );
      },
    );

    // return ListView.builder(
    //   itemCount: widget.logins.length,
    //   itemBuilder: (context, index) {
    //     return Container(decoration: const BoxDecoration(color: Colors.white),
    //       child: Card(
    //         color: Colors.white,
    //         child: ListTile(
    //           title: Text(widget.logins[index].title),
    //           subtitle: Text(widget.logins[index].website),
    //           // leading: const CircleAvatar(backgroundImage: NetworkImage('https://www.freepnglogos.com/uploads/amazon-png-logo-vector/amazon-icon-symbol-png-logo-21.png'),backgroundColor: Colors.transparent,),
    //           leading: Transform.scale(
    //             scale: 1,
    //             child: CircleAvatar(
    //               backgroundColor: Colors.transparent,
    //               backgroundImage: AssetImage(widget.logins[index].icons.first)
    //             ),
    //           ),
    //           trailing: Row(
    //             mainAxisAlignment: MainAxisAlignment.end,
    //             mainAxisSize: MainAxisSize.min,
    //             children: [
    //               // Your custom icon
    //               GestureDetector(
    //                 onTap: () {
    //                   setState(() {
    //                     widget.logins[index].isFavorite = !widget.logins[index].isFavorite;
    //                   });
    //                 },
    //                 child: Transform.scale(
    //                   scale: 0.8,
    //                   child: widget.logins[index].isFavorite
    //                       ? SvgPicture.asset(
    //                           'assets/icons/Star-Yellow.svg',
    //                           fit: BoxFit.none,
    //                         )
    //                       : SvgPicture.asset(
    //                           'assets/icons/Star-Yellow.svg',
    //                           fit: BoxFit.none,
    //                           colorFilter: const ColorFilter.mode(Colors.grey, BlendMode.srcIn),
    //                         ),
    //                 ),
    //               ), // Triple dots
    //               PopupMenuButton<String>(
    //                 itemBuilder: (context) => [
    //                   const PopupMenuItem(
    //                     value: 'option1',
    //                     child: Text('Option 1'),
    //                   ),
    //                   const PopupMenuItem(
    //                     value: 'option2',
    //                     child: Text('Option 2'),
    //                   ),
    //                   const PopupMenuItem(
    //                     value: 'option3',
    //                     child: Text('Option 3'),
    //                   ),
    //                 ],
    //                 icon: const Icon(Icons.more_vert),
    //                 onSelected: (value) {
    //                   // Handle menu item selection
    //                 },
    //               ),
    //             ],
    //           ),
    //         ),
    //       ),
    //     );
    //   },
    // );
  }
}
