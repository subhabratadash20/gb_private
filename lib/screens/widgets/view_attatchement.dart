import 'dart:convert';
import 'dart:io';
import 'dart:io' as io;

import 'package:csv/csv.dart';
import 'package:excel/excel.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gb_private/services/info_service.dart';
import 'package:gb_private/utils/extension.dart';
import 'package:gb_private/utils/global.dart';
import 'package:path_provider/path_provider.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class ViewAttachment extends StatefulWidget {
  final String fileName;
  final String fileData;

  const ViewAttachment(
      {Key? key, required this.fileName, required this.fileData})
      : super(key: key);

  @override
  State<ViewAttachment> createState() => _ViewAttachmentState();
}

class _ViewAttachmentState extends State<ViewAttachment> {
  String? attachmentData;

  Directory? createdFilePath;

  bool? fileExists;

  List<int>? readFileHasDat;

  @override
  void initState() {
    getDirectoryPath();
    // readAttachmentData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: FutureBuilder<Directory?>(
          future: getDirectoryPath(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return SizedBox(
                  width: double.infinity,
                  height: double.infinity,
                  child: fileViewer(widget.fileName, snapshot.data));
            }
            return const SizedBox.shrink();
          }),
    );
  }

  // readAttachmentData() async {
  //   attachmentData = await InfoService().read('AttachmentFilePath');
  //   return attachmentData;
  // }

  checkFileAvailable(fileType, fileData) async {
    final String filePath = "${fileData.path}/${widget.fileName}";
    final File file = File(filePath);

    fileExists = await file.exists();

    if (fileExists!) {
      // If the file exists, read its binary data using readAsBytes.
      InfoService().save('AttachmentFilePath', filePath);
      readFileHasDat = await file.readAsBytes();
      // Check if the widget is still mounted before calling setState
      if (mounted) {
        setState(() {
          // Update your state variables here
        });
      }
    } else {
      File? file = File("${fileData.path}${widget.fileName}");
      Uint8List bytes = base64Decode(widget.fileData);
      file.writeAsBytes(bytes);
      // Check if the widget is still mounted before calling setState
      if (mounted) {
        setState(() {
          // Update your state variables here
        });
      }
    }
  }

  fileViewer(fileType, fileData) {
    writeDataInFile(fileData.path);
    if (fileExists != null && fileExists! && readFileHasDat != null) {
      Widget? buildWidget = const SizedBox.shrink();
      String fileExtension = fileType.split('.').last;
      switch (fileExtension.toEnum()) {
        case ViewFileType.png:
        case ViewFileType.jpg:
        case ViewFileType.jpeg:
          buildWidget = Image.file(File('${fileData.path}${widget.fileName}'));
          break;
        case ViewFileType.pdf:
          buildWidget =
              SfPdfViewer.file(File('${fileData.path}${widget.fileName}'));
          /*buildWidget = PDFView(
            filePath: '${fileData.path}${widget.fileName}',
            enableSwipe: true,
            swipeHorizontal: true,
            autoSpacing: false,
            pageFling: false,
            onError: (error) {
              print('Error loading PDF: $error');
            },
          );*/

          break;
        case ViewFileType.xls:
        case ViewFileType.xlsx:
          return FutureBuilder(
            future: _loadExcelFile('${fileData.path}${widget.fileName}'),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return const Text('Error loading Excel file');
              } else if (snapshot.hasData) {
                return Text(snapshot.data.toString());
              }
              return const Text('No data available');
            },
          );
        case ViewFileType.csv:
          return FutureBuilder(
            future: _loadCsvFile('${fileData.path}${widget.fileName}'),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const CircularProgressIndicator();
              } else if (snapshot.hasError) {
                return const Text('Error loading CSV file');
              } else if (snapshot.hasData) {
                return Text(snapshot.data.toString());
              }
              return const Text('No data available');
            },
          );
        default:
          buildWidget =
              Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                Image.asset(
                  'assets/icons/transparent-icon.png',
                  width: 100,
                  height: 100,
                ),
                const Text("You have not attached any file")
              ]);
      }
      return buildWidget;
    }
  }

  Future<String> _loadExcelFile(String filePath) async {
    final excel = Excel.decodeBytes(File(filePath).readAsBytesSync());
    final firstSheet = excel.tables.keys.first;
    final sheet = excel.tables[firstSheet];
    return sheet.toString();
  }

  Future<List<List<dynamic>>> _loadCsvFile(String filePath) async {
    final csvFile = File(filePath);
    final csvContent = await csvFile.readAsString();
    final csvData = const CsvToListConverter().convert(csvContent);
    return csvData;
  }

  Future<Directory?> getDirectoryPath() async {
    if (kIsWeb) {
      // For web platform
      final folder = io.Directory('AppDocuments/${Global.folderName}');
      if (!folder.existsSync()) {
        folder.createSync(recursive: true);
      }
      return folder;
    } else {
      // final Directory appDocDir = await getApplicationDocumentsDirectory();
      // return Directory('${appDocDir.path}/${Global.folderName}/');

      final Directory appDocDir = await getApplicationDocumentsDirectory();
      final createdDirectory =
      Directory('${appDocDir.path}/${Global.folderName}/');

      await createdDirectory.create(recursive: true);
      await checkFileAvailable(widget.fileName, createdDirectory);

      return createdDirectory;
    }
  }

  writeDataInFile(String filePath) async {
    final Directory appDocDir = await getApplicationDocumentsDirectory();
    final createdDirectory =
    Directory('${appDocDir.path}/${Global.folderName}/');

    await createdDirectory.create(recursive: true);
    await checkFileAvailable(widget.fileName, createdDirectory);

    return createdDirectory;
  }
}

enum ViewFileType { png, jpg, jpeg, pdf, xls, xlsx, csv }
