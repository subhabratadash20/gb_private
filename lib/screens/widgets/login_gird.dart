import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/screens/controllers/main_tab_layout_controller.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../models/login/login.dart';

class LoginGridView extends StatefulWidget {
  const LoginGridView({super.key, required this.logins});
  final List<LoginModel> logins;

  @override
  State<LoginGridView> createState() => _LoginGridViewState();
}

class _LoginGridViewState extends State<LoginGridView> {
  Map groupItemsByCategory() {
    return groupBy(widget.logins, (item) => item.category);
  }

  @override
  Widget build(BuildContext context) {
    final groupedList = widget.logins.groupBy((element) => element.category);
    return GetBuilder<MainTabLayoutController>(
        builder: (mainTabLayoutController) {
      return GridView(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, mainAxisExtent: 110),
          children: groupedList.keys
              .map(
                (e) => InkWell(
                  onTap: () {
                    mainTabLayoutController.selectedCategory = e;
                    mainTabLayoutController.pageController.nextPage(
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.linear);
                    setState(() {});
                  },
                  child: Container(
                    margin: const EdgeInsets.all(5),

                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: Colors.white,
                      boxShadow: const [
                        BoxShadow(
                            color: Color(
                              0xB3ECF4FF,
                            ),
                            blurRadius: 20,
                            offset: Offset(0, 4))
                      ],
                    ),
                    padding: const EdgeInsets.all(5),

                    // borderRadius: BorderRadius.circular(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            SvgPicture.asset(
                              "assets/icons/${e!}.svg",
                              fit: BoxFit.cover,
                              height: 35,
                              width: 35,
                              // scale: 1,
                            ),
                            Text(
                              e,
                              style: const TextStyle(
                                fontFamily: 'Poppins',
                                color: Color(0xff585858),
                                fontWeight: FontWeight.w500,
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            PopupMenuButton<String>(
                              padding: EdgeInsets.zero,
                              itemBuilder: (context) => [
                                PopupMenuItem(
                                  value: 'info',
                                  child: const Text('info'),
                                  onTap: () {
                                    // widget.logins[index].delete();
                                  },
                                ),
                                // const PopupMenuItem(
                                //   value: 'option2',
                                //   child: Text('Option 2'),
                                // ),
                                // const PopupMenuItem(
                                //   value: 'option3',
                                //   child: Text('Option 3'),
                                // ),
                              ],
                              icon: SvgPicture.asset(
                                'assets/icons/three_dot.svg',
                                height: 20,
                              ),
                              onSelected: (value) {
                                // Handle menu item selection
                              },
                            ),
                            Text(
                              groupedList[e]!.length.toString(),
                              style: const TextStyle(
                                fontFamily: 'Poppins',
                                color: Color(0xff6E8A9E),
                                fontWeight: FontWeight.w900,
                                fontSize: 24,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              )
              .toList());
    });
  }
}
