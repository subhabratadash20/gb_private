import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class IconsGalary extends StatefulWidget {
  final List<String> fileList;
  const IconsGalary({super.key, required this.fileList});

  @override
  State<IconsGalary> createState() => _IconsGalaryState();
}

class _IconsGalaryState extends State<IconsGalary> {
  List<String> localFileList = [];
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        localFileList = widget.fileList;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Select Icon',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Color(0xFF585858),
            fontFamily: 'Poppins',
          ),
        ),
        centerTitle: true,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: SvgPicture.asset(
              "assets/icons/Close-white.svg",
              // ignore: deprecated_member_use
              color: Colors.grey,
            )),
      ),
      body: ListView(
        children: [
          Container(
            decoration: BoxDecoration(
              border: Border.all(color: const Color(0XFFD1DDED), width: 1),
              borderRadius: BorderRadius.circular(8),
            ),
            margin: const EdgeInsets.all(15.0),
            child: TextField(
              onChanged: (value) {
                setState(() {
                  value.isNotEmpty
                      ? localFileList = widget.fileList
                          .where((e) =>
                              e.toLowerCase().contains(value.toLowerCase()))
                          .toList()
                      : localFileList = widget.fileList;
                });
              },
              decoration: InputDecoration(
                hintText: 'Search Icons',
                hintStyle: const TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color(0xFF9498AF),
                  fontFamily: 'Poppins-Regular',
                ),
                suffixIcon: Transform.scale(
                  scale: 0.5,
                  child: SvgPicture.asset(
                    'assets/icons/Search.svg',
                    width: 10,
                  ),
                ),
                contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                border: const OutlineInputBorder(
                  borderSide: BorderSide.none,
                ),
                // focusedBorder: OutlineInputBorder(
                //   borderSide:
                //       const BorderSide(color: Color(0XFFD1DDED), width: 2),
                //   borderRadius: BorderRadius.circular(8),
                // ),
              ),
            ),
          ),
          GridView.builder(
            padding: const EdgeInsets.all(10),
            itemCount: localFileList.length,
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                mainAxisSpacing: 10,
                crossAxisSpacing: 10,
                maxCrossAxisExtent: 100),
            itemBuilder: (context, index) => InkWell(
              onTap: () {
                Get.back(result: localFileList[index]);
              },
              child: Container(
                width: 83,
                height: 83,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(
                        color: Color(
                          0xB3ECF4FF,
                        ),
                        blurRadius: 20,
                        offset: Offset(0, 4))
                  ],
                ),
                padding: const EdgeInsets.all(17),

                // borderRadius: BorderRadius.circular(10),
                child: localFileList[index].endsWith(".svg")
                    ? SvgPicture.asset(
                        localFileList[index],
                        fit: BoxFit.cover,
                        // scale: 1,
                      )
                    : Image.asset(
                        localFileList[index],
                        fit: BoxFit.cover,
                      ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
