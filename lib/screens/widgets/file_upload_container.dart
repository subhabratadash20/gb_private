import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../controllers/image_picker_controller.dart';
import 'custom_dialogue_option_for_filepicker.dart';

class FileUploadContainer extends StatefulWidget {
  List<String> fileList;
  FileUploadContainer({super.key, required this.fileList});

  @override
  State<FileUploadContainer> createState() => _FileUploadContainerState();
}

class _FileUploadContainerState extends State<FileUploadContainer> {
  PageController pageController = PageController();
  Future<String>? _showUploadDialog() async {
    final file = await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        backgroundColor: Colors.white,
        contentPadding: EdgeInsets.zero,
        content: Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0), color: Colors.white),
          padding: const EdgeInsets.all(20),
          height: 180,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  DialogOption(
                    icon: const Icon(
                      Icons.camera_alt,
                      color: Color(0xFFA9C4D8),
                      size: 38,
                    ),
                    text: 'Camera',
                    onTap: () async {
                      final file =
                          await ImagePickerCotroller().captureImageFromCamera();
                      if (file != null && mounted) {
                        Navigator.of(context).pop(file.path);
                      }
                    },
                  ),
                  DialogOption(
                    icon: const Icon(
                      Icons.cloud_upload_rounded,
                      size: 35,
                      color: Color(0xFFA9C4D8),
                    ),
                    text: 'Upload',
                    onTap: () async {
                      FilePicker filePicker = FilePicker.platform;
                      final x = await filePicker.pickFiles(
                        allowMultiple: true,
                        // allowedExtensions: []
                      );
                      if (mounted) Navigator.pop(context);
                      widget.fileList
                          .addAll(x!.files.map((e) => e.path!).toList());
                      setState(() {});
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
    return file;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            InkWell(
              onTap: () async {
                final file = await _showUploadDialog();
                if (file != null) {
                  widget.fileList.add(file);
                  setState(() {});
                }
                // Navigator.of(context).push(MaterialPageRoute(
                //     builder: (context) => MyImagePicker()));
              },
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Text(
                    'Add files',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      fontFamily: 'Poppins',
                      color: Color(0xff2E2E2E),
                    ),
                  ),
                  const SizedBox(width: 8),
                  // Icon(Icons.add_circle, color: Colors.green, size: 20),
                  SvgPicture.asset(
                    'assets/icons/add_files.svg',
                  )
                ],
              ),
            ),
            Row(
              children: [
                InkWell(
                  onTap: () {
                    pageController.previousPage(
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.linear);
                  },
                  child: RotatedBox(
                    quarterTurns: 4,
                    child: SvgPicture.asset(
                      'assets/icons/arrow-left.svg',
                    ),
                  ),
                ),
                const SizedBox(width: 12),
                InkWell(
                  onTap: () {
                    pageController.nextPage(
                        duration: const Duration(milliseconds: 200),
                        curve: Curves.linear);
                  },
                  child: RotatedBox(
                    quarterTurns: 2,
                    child: SvgPicture.asset(
                      'assets/icons/arrow-left.svg',
                    ),
                  ),
                )
                // const IconButton(
                //   icon: Icon(
                //     Icons.arrow_back_ios,
                //     size: 12,
                //     color:
                //         //  index == 0 ?
                //         //  Colors.grey
                //         //  :
                //         Color(0xFF585858),
                //   ),
                //   // onPressed: () {
                //   //   // index != 0 ? index-- : null;
                //   //   setState(() {});
                //   // },
                //   onPressed: null,
                // ),
                // IconButton(
                //   icon: const Icon(
                //     Icons.arrow_forward_ios,
                //     size: 12,
                //     color: Color(0xFF585858),
                //     // color: index == widget.fileList.length - 1
                //     //     ? Colors.grey
                //     //     : const Color(0xFF585858),
                //   ),
                //   onPressed: () {
                //     // index < widget.fileList.length - 1 ? index++ : null;
                //     setState(() {});
                //   },
                // ),
              ],
            )
          ],
        ),
        const SizedBox(height: 5),
        widget.fileList.isEmpty
            ? Container(
                height: 80,
                width: double.infinity,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(8)),
                    color: Colors.white70),
                padding: const EdgeInsets.symmetric(horizontal: 36),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset('assets/icons/Document.svg'),
                    const SizedBox(width: 8),
                    const Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Not Uploaded yet,',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Color(0xFF344C5D),
                              fontSize: 12),
                        ),
                        Text(
                          'Your files will appear here',
                          style: TextStyle(
                              fontWeight: FontWeight.w300,
                              color: Color(0xFF6E8A9E),
                              fontSize: 12),
                        )
                      ],
                    )
                  ],
                ),
              )
            : SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                controller: pageController,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: List.generate(
                    widget.fileList.length,
                    (index) => Padding(
                      padding: const EdgeInsets.only(right: 10),
                      child: widget.fileList[index].endsWith('.pdf')
                          ? Container(
                              width: 84,
                              height: 84,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.grey.withOpacity(0.2),
                              ),
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  const Text(
                                    'PDF',
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.black,
                                      fontFamily: 'Poppins',
                                    ),
                                  ),
                                  Positioned(
                                    bottom: 5,
                                    right: 5,
                                    child: InkWell(
                                      onTap: () {
                                        widget.fileList.removeAt(index);
                                        setState(() {});
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 4, bottom: 4, right: 4),
                                        child: Transform.scale(
                                          scale: 1.2,
                                          child: SvgPicture.asset(
                                            'assets/icons/remove.svg',
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          : Container(
                              width: 84,
                              height: 84,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: FileImage(
                                    File(widget.fileList[index]),
                                  ),
                                ),
                              ),
                              alignment: Alignment.bottomRight,
                              child: InkWell(
                                onTap: () {
                                  widget.fileList.removeAt(index);
                                  setState(() {});
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      left: 4, bottom: 4, right: 4),
                                  child: Transform.scale(
                                    scale: 1.2,
                                    child: SvgPicture.asset(
                                      'assets/icons/remove.svg',
                                    ),
                                  ),
                                ),
                              ),
                            ),
                    ),
                  ),
                ),
              )
        // ClipRRect(
        //     borderRadius: BorderRadius.circular(15),
        //     child: Image.file(
        //       File(
        //         widget.fileList[index],
        //       ),
        //     ),
        //   )
      ],
    );
  }
}
