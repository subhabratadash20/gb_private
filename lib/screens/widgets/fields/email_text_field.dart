import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gb_private/models/discus_form/discus_form_element.dart';
import 'package:gb_private/screens/widgets/fields/label_field.dart';
import 'package:gb_private/screens/widgets/fields/text_field_bottom_widget.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:gb_private/utils/ui_utils.dart';

class EmailTextField extends StatefulWidget {
  final DiscusFormElement element;
  final TextEditingController? controller;
  final Function onSelectFieldType;
  final bool? settingFlag;

  const EmailTextField({
    Key? key,
    required this.element,
    this.controller,
    required this.onSelectFieldType,
    this.settingFlag,
  }) : super(key: key);

  @override
  State<EmailTextField> createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {
  static String me = "Email TextField";
  static final LogUtil _logger = LogUtil(me);
  final UIUtils uiUtils = UIUtils(_logger);
  var obscureText = true;

  @override
  void initState() {
    obscureText = widget.element.hidden ?? false;
    super.initState();
  }

  @override
  void dispose() {
    widget.controller?.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        DiscusLabelField(element: widget.element, name: ''),
        Row(
          children: [
            Expanded(
              child: GestureDetector(
                onTap: () {
                  uiUtils.copyDataToClipBoard(
                      widget.element.fieldValue, context);
                },
                child: TextFormField(
                  readOnly: widget.settingFlag == null,
                  enabled: widget.settingFlag == null ? false : true,
                  obscureText: obscureText,
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  validator: (value) => defaultValidator(value),
                  initialValue: widget.element.fieldValue,
                  onChanged: (text) {
                    widget.element.fieldValue = text;
                  },
                  controller: widget.controller,
                  keyboardType: TextInputType.emailAddress,
                  // maxLines: widget.element.maxLength,
                  decoration: InputDecoration(
                    suffixIcon: Visibility(
                      visible: widget.element.hidden!,
                      child: IconButton(
                          onPressed: () {
                            setState(() {
                              obscureText = !obscureText;
                            });
                          },
                          icon: obscureText
                              ? const Icon(FontAwesomeIcons.eyeSlash)
                              : const Icon(Icons.remove_red_eye_outlined)),
                    ),
                    border: InputBorder.none,
                    filled: true,
                    hintText: widget.settingFlag == false
                        ? ""
                        : 'Enter ${widget.element.toEnumTitle()}',
                  ),
                ),
              ),
            ),
          ],
        ),
        Visibility(
          visible: widget.settingFlag == false,
          child: TextFieldBottomWidget(
            dropDownValue: widget.element.toEnumType(),
            onSelectFieldType: (FormElementTypes dropDownValue) {
              widget.onSelectFieldType(dropDownValue);
            },
            requiredValue: widget.element.required,
            onRequiredTap: (requiredValue) {
              widget.element.required = requiredValue;
            },
            hideValue: widget.element.hidden,
            onHideTap: (hideValue) {
              widget.element.hidden = hideValue;
            },
          ),
        )
      ],
    );
  }

  String? defaultValidator(String? value) {
    bool emailValid = uiUtils.validateEmail(value!);
    if (widget.element.required ?? false) {
      if (value.isEmpty) {
        return 'eMail is required';
      } else if (!emailValid) {
        return 'eMail address is invalid';
      }
    }
    return null;
  }
}