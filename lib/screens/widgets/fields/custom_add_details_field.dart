import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gb_private/models/drag_drop_data_model.dart';
import 'package:intl/intl.dart';

class CustomAddDetailsField extends StatefulWidget {
  DragDropData item;
  final void Function(String)? onChanged;
  CustomAddDetailsField({super.key, required this.item, this.onChanged});

  @override
  State<CustomAddDetailsField> createState() => _CustomAddDetailsFieldState();
}

class _CustomAddDetailsFieldState extends State<CustomAddDetailsField> {
  TextEditingController localTextController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      key: ValueKey(widget.item),
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
          boxShadow: const [
            BoxShadow(
                color: Color(
                  0xB3ECF4FF,
                ),
                blurRadius: 20,
                offset: Offset(0, 4))
          ],
          border: Border.all(
            color: const Color(
              0xB3ECF4FF,
            ),
          )),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          // Container(
          //   alignment: Alignment.center,
          //   height: 38,
          //   padding: const EdgeInsets.symmetric(
          //     horizontal: 15.0,
          //   ),
          //   child: Row(
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //     children: [
          //       const Icon(
          //         FontAwesomeIcons.gripVertical,
          //         size: 16,
          //       ),
          //       const SizedBox(
          //         width: 15,
          //       ),
          //       Text(
          //         widget.item.name,
          //         style: const TextStyle(
          //             fontWeight: FontWeight.w500,
          //             fontSize: 14,
          //             fontFamily: 'Poppins-Regular',
          //             color: Color(0xFF585858)),
          //       ),
          //       const Spacer(),
          //       if (widget.item.editable)
          //         InkWell(
          //           onTap: () {
          //             isEditing = true;
          //             setState(() {});
          //             // renameField(widget.item);
          //           },
          //           child: const Icon(
          //             Icons.edit,
          //             size: 20,
          //           ),
          //         )
          //     ],
          //   ),
          // ),
          //
          Container(
            padding: const EdgeInsets.only(left: 15.0, top: 5, bottom: 5),
            height: 38,
            child: Row(
              children: [
                const Icon(
                  FontAwesomeIcons.gripVertical,
                  size: 16,
                ),
                const SizedBox(
                  width: 5,
                ),
                Expanded(
                  child: TextField(
                    readOnly: !widget.item.isEditing,
                    style: const TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 14,
                        fontFamily: 'Poppins-Regular',
                        color: Color(0xFF585858)),
                    controller: widget.item.isEditing
                        ? localTextController
                        : TextEditingController(text: widget.item.name),
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 10,
                      ),
                      focusedBorder: widget.item.isEditing
                          ? const OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Color(0XFFADE3A0), width: .7))
                          : const OutlineInputBorder(
                              borderSide: BorderSide.none),
                      border: const OutlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                      suffixIcon: widget.item.isEditing
                          ? IconButton(
                              onPressed: () {
                                widget.item.isEditing = false;
                                localTextController.clear();
                                setState(() {});
                              },
                              iconSize: 15,
                              icon: SvgPicture.asset(
                                "assets/icons/close.svg",
                                fit: BoxFit.cover,
                                height: 15,
                              ),
                            )
                          : null,
                    ),
                  ),
                ),
                if (widget.item.editable)
                  widget.item.isEditing
                      ? IconButton(
                          onPressed: () {
                            setState(() {
                              widget.item.name = localTextController.text;
                              widget.item.isEditing = false;
                            });
                          },
                          iconSize: 15,
                          icon: SvgPicture.asset(
                            "assets/icons/Done.svg",
                            fit: BoxFit.cover,
                            height: 15,
                          ),
                        )
                      : IconButton(
                          onPressed: () {
                            widget.item.isEditing = true;
                            setState(() {
                              localTextController.text = widget.item.name;
                            });
                            // renameField(widget.item);
                          },
                          style: IconButton.styleFrom(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 0)),
                          icon: const Icon(
                            Icons.edit,
                            size: 20,
                          ),
                        ),
              ],
            ),
          ),
          const Divider(
            thickness: 2,
            color: Color(
              0xB3ECF4FF,
            ),
            height: 0,
          ),
          SizedBox(
            height: 38,
            child: TextField(
              maxLines:
                  widget.item.type != "pin" && widget.item.type != "password"
                      ? 10
                      : 1,
              keyboardType:
                  widget.item.type == "pin" || widget.item.type == "number"
                      ? TextInputType.number
                      : null,
              obscureText: widget.item.isObscure,
              readOnly:
                  widget.item.type == "date" || widget.item.type == "time",
              onTap: () async {
                if (widget.item.type == "date") {
                  widget.item.controller.text = DateFormat(
                          widget.item.name == "Date[MM/YY]"
                              ? "MM/yy"
                              : "yyyy/MM/dd")
                      .format(
                    await showDatePicker(
                          initialDatePickerMode: DatePickerMode.day,
                          initialEntryMode: DatePickerEntryMode.calendarOnly,
                          context: context,
                          firstDate: DateTime(2000),
                          lastDate: DateTime(2100),
                          initialDate: DateTime.now(),
                          builder: (context, child) {
                            return Theme(
                              data: Theme.of(context).copyWith(
                                colorScheme: const ColorScheme.light(
                                  background: Colors.white,
                                  surfaceTint: Colors.white,
                                  primary: Color(0xff22B70A),
                                ),
                              ),
                              child: child!,
                            );
                          },
                        ) ??
                        DateTime.now(),
                  );
                } else {
                  if (widget.item.type == "time") {
                    TimeOfDay? time = await showTimePicker(
                      context: context,
                      initialTime: TimeOfDay.now(),
                      builder: (context, child) {
                        return Theme(
                          data: Theme.of(context).copyWith(
                            colorScheme: const ColorScheme.light(
                              background: Colors.white,
                              surfaceTint: Colors.white,
                              primary: Color(0xff22B70A),
                              secondary: Color(0xff22B70A),
                            ),
                          ),
                          child: child!,
                        );
                      },
                    );
                    setState(() {});
                    if (time != null) {
                      widget.item.controller.text =
                          "${time.hour}:${time.minute} ${time.period.name}";
                    }
                  } else {
                    null;
                  }
                }
                setState(() {});
              },
              onChanged: widget.onChanged,
              controller: widget.item.controller,
              textInputAction: widget.item.type == "multiline-text"
                  ? TextInputAction.newline
                  : null,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.zero,
                // isDense: true,
                hintText: widget.item.name,
                hintStyle: const TextStyle(
                    fontFamily: 'Poppins-Regular',
                    color: Color(0XFF9498AF),
                    fontSize: 14),
                border: InputBorder.none,
                prefixIcon: Image.asset(
                  widget.item.imagePath,
                  width: 15,
                  height: 15,
                ),
                suffixIcon:
                    widget.item.type == "password" || widget.item.type == "pin"
                        ? InkWell(
                            onTap: () {
                              setState(() {
                                widget.item.isObscure = !widget.item.isObscure;
                              });
                            },
                            child: Icon(
                                widget.item.isObscure
                                    ? Icons.visibility_outlined
                                    : Icons.visibility_off_outlined,
                                size: 20,
                                color: const Color(0xFFA9C4D8)),
                          )
                        : null,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
