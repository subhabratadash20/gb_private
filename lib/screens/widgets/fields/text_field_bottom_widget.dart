import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:gb_private/models/discus_form/discus_form_element.dart';
import 'package:gb_private/utils/extension.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:gb_private/utils/ui_utils.dart';
import 'package:velocity_x/velocity_x.dart';

class TextFieldBottomWidget extends StatefulWidget {
  final Function? onSelectFieldType;
  final FormElementTypes? dropDownValue;
  final Function? onRequiredTap;
  final Function? onHideTap;
  final bool? hideValue;
  final bool? requiredValue;

  const TextFieldBottomWidget(
      {Key? key,
        this.onSelectFieldType,
        this.onRequiredTap,
        this.onHideTap,
        this.dropDownValue,
        this.hideValue,
        this.requiredValue})
      : super(key: key);

  @override
  State<TextFieldBottomWidget> createState() => _TextFieldBottomWidgetState();
}

class _TextFieldBottomWidgetState extends State<TextFieldBottomWidget> with WidgetsBindingObserver {
  static String me = "TextField Bottom Widget";
  static final LogUtil _logger = LogUtil(me);
  final UIUtils uiUtils = UIUtils(_logger);
  var isDarkMode =
      SchedulerBinding.instance.platformDispatcher.platformBrightness == Brightness.dark;
  FormElementTypes? dropdownValue;
  bool isChecked = false;
  bool isHide = false;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    isChecked = widget.requiredValue ?? false;
    isHide = widget.hideValue ?? false;
    dropdownValue = widget.dropDownValue ?? FormElementTypes.values.first;
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      getThemeVal();
    }
  }
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Flexible(
          flex: 1,
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 2),
            decoration: const BoxDecoration(
                border:
                Border(bottom: BorderSide(width: 1, color: Colors.black))),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<FormElementTypes>(
                isExpanded: true,
                value: dropdownValue,
                elevation: 16,
                onChanged: (FormElementTypes? value) {
                  // This is called when the user selects an item.
                  // dropdownValue = value;
                  widget.onSelectFieldType!(value!);
                },
                items: FormElementTypes.values
                    .map<DropdownMenuItem<FormElementTypes>>(
                        (FormElementTypes formElementType) {
                      return DropdownMenuItem<FormElementTypes>(
                        value: formElementType,
                        child: Text(
                          formElementType.title,
                          style: const TextStyle(fontSize: 12),
                        ),
                      );
                    }).toList(),
              ),
            ),
          ),
        ),
        Flexible(
          flex: 1,
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 2),
            decoration: const BoxDecoration(
                border:
                Border(bottom: BorderSide(width: 1, color: Colors.black))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Checkbox(
                  checkColor: Colors.white,
                  value: isChecked,
                  onChanged: (bool? value) {
                    setState(() {
                      isChecked = value!;
                      widget.onRequiredTap!(isChecked);
                    });
                  },
                ),
                const Flexible(
                    child: Text("Required?", style: TextStyle(fontSize: 12))),
              ],
            ),
          ),
        ),
        Flexible(
          flex: 1,
          child: GestureDetector(
            onTap: () {
              setState(() {
                isHide = !isHide;
                widget.onHideTap!(isHide);
              });
            },
            child: Container(
              margin: const EdgeInsets.fromLTRB(2, 0, 16, 0),
              decoration: const BoxDecoration(
                  border: Border(
                      bottom: BorderSide(width: 1, color: Colors.black))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.remove_red_eye_outlined,
                    size: 22,
                    color: isHide
                        ? Colors.blue
                        : isDarkMode
                        ? Colors.white
                        : Colors.black,
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Text(
                    "Hide?",
                    style: TextStyle(
                        color: isHide
                            ? Colors.blue
                            : isDarkMode
                            ? Colors.white
                            : Colors.black,
                        fontSize: 12),
                  )
                ],
              ).pLTRB(0, 10, 0, 16),
            ),
          ),
        ),
      ],
    );
  }

  void getThemeVal() {
    isDarkMode =
        SchedulerBinding.instance.platformDispatcher.platformBrightness == Brightness.dark;
    setState(() {
    });
  }
}
