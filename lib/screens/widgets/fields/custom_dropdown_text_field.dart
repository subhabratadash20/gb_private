import 'package:flutter/material.dart';

class CustomDropdownTextField extends StatefulWidget {
  final TextEditingController controller;
  final List<String> options;
  final ValueChanged<String> onChanged;
  const CustomDropdownTextField(
      {super.key,
      required this.options,
      required this.onChanged,
      required this.controller});

  @override
  State<CustomDropdownTextField> createState() =>
      _CustomDropdownTextFieldState();
}

class _CustomDropdownTextFieldState extends State<CustomDropdownTextField> {
  @override
  void dispose() {
    widget.controller.dispose();
    super.dispose();
  }

  bool _isDropdownOpen = false;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        TextField(
          controller: widget.controller,
          onTap: _toggleDropdown,
          readOnly: true,
          decoration: const InputDecoration(
            labelText: 'Select an option',
            suffixIcon: Icon(Icons.keyboard_arrow_down_outlined),
          ),
        ),
        if (_isDropdownOpen)
          Container(
            width: double.infinity,
            margin: const EdgeInsets.only(top: 8),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
              borderRadius: BorderRadius.circular(4),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                for (String option in widget.options)
                  ListTile(
                    title: Text(option),
                    onTap: () {
                      widget.controller.text = option;
                      widget.onChanged(option);
                      _toggleDropdown();
                    },
                  ),
              ],
            ),
          ),
      ],
    );
  }

  void _toggleDropdown() {
    setState(() {
      _isDropdownOpen = !_isDropdownOpen;
    });
  }
}
