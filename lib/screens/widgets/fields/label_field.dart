import 'package:flutter/material.dart';
import 'package:gb_private/models/discus_form/discus_form_element.dart';
import 'package:gb_private/utils/ui_utils.dart';
import 'package:velocity_x/velocity_x.dart';

class DiscusLabelField extends StatefulWidget {
  final DiscusFormElement element;
  final String? name;
  const DiscusLabelField({super.key, required this.element, this.name});

  @override
  State<DiscusLabelField> createState() => _DiscusLabelFieldState();
}

class _DiscusLabelFieldState extends State<DiscusLabelField> {
  late dynamic key;
  bool isIconButtonPressed =
      false; // Add this variable to track the pressed state

  @override
  initState() {
    key = widget.element.key ?? generateRandomString(10);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = styleOfBackend(widget.element, context);
    return Container(
      key: Key(key),
      margin: const EdgeInsets.only(top: 10, bottom: 5),
      child: SelectableText.rich(TextSpan(children: [
        (widget.element.userFieldLabel == null ||
                widget.element.userFieldLabel == '')
            ? TextSpan(text: widget.element.toEnumTitle(), style: textStyle)
            : TextSpan(text: widget.element.userFieldLabel, style: textStyle),
        WidgetSpan(child: 5.widthBox),
        TextSpan(text: widget.name, style: textStyle),
        TextSpan(
            text: widget.element.required == null ? '' : '\u{002A}',
            style: const TextStyle(color: Colors.red))
      ])),
    );
  }
}
