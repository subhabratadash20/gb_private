import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gb_private/models/discus_form/discus_form_element.dart';
import 'package:gb_private/screens/widgets/fields/label_field.dart';
import 'package:gb_private/screens/widgets/fields/text_field_bottom_widget.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:gb_private/utils/ui_utils.dart';

class CustomTextField extends StatefulWidget {
  final DiscusFormElement element;
  final TextEditingController? controller;
  final Function onSelectFieldType;
  final bool? settingFlag;
  const CustomTextField(
      {super.key,
      required this.element,
      this.controller,
      required this.onSelectFieldType,
      this.settingFlag});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  var obscureText = true;
  static String me = "CustomTextField";
  static final LogUtil _logger = LogUtil(me);
  final UIUtils uiUtils = UIUtils(_logger);

  @override
  void initState() {
    obscureText = widget.element.hidden ?? false;
    super.initState();
  }

  @override
  void dispose() {
    widget.controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        DiscusLabelField(
          element: widget.element,
          name: '',
        ),
        Row(
          children: [
            Expanded(
                child: GestureDetector(
              child: TextFormField(
                readOnly: widget.settingFlag == null,
                enabled: widget.settingFlag == null ? false : true,
                obscureText: obscureText,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (value) => _validateInput(value),
                initialValue: widget.element.fieldValue,
                onChanged: (value) {
                  widget.element.fieldValue = value;
                },
                controller: widget.controller,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    suffixIcon: Visibility(
                      visible: widget.element.hidden!,
                      child: IconButton(
                        onPressed: () {
                          setState(() {
                            obscureText = !obscureText;
                          });
                        },
                        icon: obscureText
                            ? const Icon(FontAwesomeIcons.eyeSlash)
                            : const Icon(Icons.remove_red_eye_outlined),
                      ),
                    ), filled: true, hintText: widget.settingFlag == false ? "" : 'Enter ${widget.element.toEnumTitle()}'),
              ),
            ))
          ],
        ),
        Visibility(
          visible: widget.settingFlag == false,
          child: TextFieldBottomWidget(
            dropDownValue: widget.element.toEnumType(),
            onSelectFieldType: (FormElementTypes dropDownValue) {
              widget.onSelectFieldType(dropDownValue);
            },
            requiredValue: widget.element.required,
            onRequiredTap: (requiredValue) {
              widget.element.required = requiredValue;
            },
            hideValue: widget.element.hidden,
            onHideTap: (hideValue) {
              widget.element.hidden = hideValue;
            },
          ),
        )
      ],
    );
  }

  String? _validateInput(String? value) {
    if (widget.element.required ?? false) {
      if (value == null || value.isEmpty) {
        return 'Please enter here';
      }
    }
    return null;
  }
}
