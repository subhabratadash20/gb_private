import 'package:flutter/material.dart';
import 'package:gb_private/models/info/category.dart';
import 'package:gb_private/services/info_service.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:gb_private/utils/ui_utils.dart';

class SummaryCardView extends StatefulWidget {
  final Category? category;
  final FromPage? fromPage;
  final ViewType? viewType;
  final Function? onFavoriteClick;
  final Function? onDeleteClick;
  final Function? onThemeClick;
  final GestureTapCallback? onTap;
  final String? title, subTitle;
  final IconData? icon;
  final Color? iconColor;
  final int? index;
  final int selectedIndex;

  const SummaryCardView({
    Key? key,
    this.category,
    this.fromPage,
    this.onFavoriteClick,
    this.onDeleteClick,
    this.viewType,
    this.onTap,
    this.title,
    this.subTitle,
    this.icon,
    this.iconColor,
    this.onThemeClick,
    this.index,
    this.selectedIndex = -1,
  }) : super(key: key);

  @override
  State<SummaryCardView> createState() => _SummaryCardViewState();
}

class _SummaryCardViewState extends State<SummaryCardView> {
  static String me = "Summary Card";
  static final LogUtil _logger = LogUtil(me);
  final UIUtils uiUtils = UIUtils(_logger);
  InfoService infoService = InfoService();

  String? readScreenLock;
  late bool biometric;

  List<Category>? categoryItemList = [];

  readLock() async {
    readScreenLock = await infoService.read("ScreenLock") ?? 'false';
    biometric = readScreenLock!.parseBool();
    _logger.logDebug(readScreenLock);
  }
  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}

enum FromPage {
  secureInfo,
  subCategory,
  settingsPage,
  settingCategories,
  settingsAddCategories,
  favourite,
  dashboard,
  theme,
  security,
  about,
  search,
  googleDrivePage,
  googleDriveFilePage,
  backUpPage,
  backUpPageDownload,
  dataPage,
  dropBoxFolderPage,
  dropBoxFilePage,
  oneDriveFolderPage,
  oneDriveFilePage,
  authPage,
  summaryCard
}
enum ViewType { list, grid, row }
