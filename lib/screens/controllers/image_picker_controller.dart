import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerCotroller {
  Future<File?> getImageFromGallery() async {
    final image = await ImagePicker().pickImage(source: ImageSource.gallery);
    return image != null ? File(image.path) : null;
  }

  Future<File?> captureImageFromCamera() async {
    final image = await ImagePicker().pickImage(source: ImageSource.camera);
    return image != null ? File(image.path) : null;
  }

  Future<List<String>> initImages() async {
    // >> To get paths you need these 2 lines
    final manifestContent = await rootBundle.loadString('AssetManifest.json');

    final Map<String, dynamic> manifestMap = json.decode(manifestContent);
    // >> To get paths you need these 2 lines

    final imagePaths = manifestMap.keys
        .where((String key) => key.contains('icon_gallery/'))
        .where((String key) => key.contains('.svg') || key.contains(".png"))
        // .where((element) => element.contains(".png"))
        .toList();
    debugPrint("$imagePaths");
    return imagePaths;
  }
}
