import 'dart:async';
import 'dart:collection';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:gb_private/screens/controllers/main_tab_layout_controller.dart';
import 'package:gb_private/screens/login/categorywiselogindetails.dart';
import 'package:gb_private/styles/size_config.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:gb_private/utils/ui_utils.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../models/login/login.dart';
import '../login/login_details_list.dart';

class MainTabScreen extends StatefulWidget {
  const MainTabScreen({super.key});

  @override
  State<MainTabScreen> createState() => _MainTabScreenState();
}

class _MainTabScreenState extends State<MainTabScreen> {
  static const String _me = 'MainTabScreen';
  static final LogUtil _logger = LogUtil(_me);
  final UIUtils uiUtils = UIUtils(_logger);
  DateTime? currentBackPressTime;
  final ListQueue<int> _navigationQueue = ListQueue();
  bool isVisible = true;
  bool callback = false;
  MainTabLayoutController mainTabLayoutController =
      Get.find<MainTabLayoutController>();

  /// WillPopScope BackScreen Controller function.
  Future<bool> onWillPop() async {
    if (_navigationQueue.isEmpty) {
      DateTime now = DateTime.now();
      if (callback) {
        _handleAppExit();
      } else {
        _resetAppToInitialState();
      }
      _startExitCallbackTimer();
      if (currentBackPressTime == null ||
          now.difference(currentBackPressTime!) > const Duration(seconds: 2)) {
        currentBackPressTime = now;
        _showExitToast();
        return false;
      }
      return true;
    } else {
      _navigateBack();
      return false;
    }
  }

  void _handleAppExit() {
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');
  }

  void _resetAppToInitialState() {
    setState(() {
      mainTabLayoutController.currentIndex = 0;
      isVisible = true;
    });
  }

  void _startExitCallbackTimer() {
    Timer(const Duration(seconds: 2), () {
      setState(() {
        callback = false;
      });
    });
    callback = true;
  }

  void _showExitToast() {
    VxToast.show(
      context,
      msg: "S.of(context).msgPressAgainToExitTheApp",
      position: VxToastPosition.center,
      showTime: 3000,
    );
  }

  void _navigateBack() {
    setState(() {
      _navigationQueue.removeLast();
      int position = _navigationQueue.isEmpty ? 0 : _navigationQueue.last;
      mainTabLayoutController.currentIndex = position;
    });
  }

  List<BoardingItem> items = [
    BoardingItem(
      label: 'Login',
      icon: SvgPicture.asset('assets/icons/Login.svg'),
      screen: const LoginDetails(),
      activeIcon: SvgPicture.asset('assets/icons/Login-green.svg'),
    ),
    BoardingItem(
      label: 'Cards',
      icon: SvgPicture.asset('assets/icons/Cards.svg'),
      screen: const LoginDetails(),
      activeIcon: SvgPicture.asset('assets/icons/Card-green.svg'),
    ),
    BoardingItem(
      label: '',
      icon: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(50),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 2,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: SvgPicture.asset('assets/icons/search-eye-fill.svg'),
      ),
      screen: const LoginDetails(),
      activeIcon: Container(
        padding: const EdgeInsets.all(10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(50),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 2,
              offset: const Offset(0, 2),
            ),
          ],
        ),
        child: SvgPicture.asset('assets/icons/Search-green.svg'),
      ),
    ),
    BoardingItem(
      label: 'Notes',
      icon: SvgPicture.asset('assets/icons/notes_grey.svg'),
      screen: const LoginDetails(),
      activeIcon: SvgPicture.asset('assets/icons/Notes.svg'),
    ),
    BoardingItem(
      label: 'Profile',
      icon: const CircleAvatar(
        backgroundImage: NetworkImage(
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgn__ik2ek8B-Ti9IKcANJUP7gQA9Qc0A09Td9AtElZw&s',
        ),
      ),
      screen: const Placeholder(),
      activeIcon: const CircleAvatar(
        backgroundImage: NetworkImage(
          scale: 0.5,
          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgn__ik2ek8B-Ti9IKcANJUP7gQA9Qc0A09Td9AtElZw&s',
        ),
      ),
    ),
    // BoardingItem(label: label, icon: icon, screen: screen, activeIcon: activeIcon)
  ];
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   WidgetsBinding.instance.addPersistentFrameCallback((_) async {
  //     await Permission.manageExternalStorage.request();
  //     String? dirPath = await FilePicker.platform.getDirectoryPath();

  //     debugPrint("path::::::$dirPath");
  //     Hive.registerAdapter(LoginModelAdapter());
  //     await Hive.initFlutter();
  //     await Hive.openBox<LoginModel>("Logins", path: dirPath);
  //   });
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    SizeConfig.init(context);
    return PopScope(
      child: GetBuilder<MainTabLayoutController>(builder: (_) {
        return Scaffold(
          body: PageView(
              physics: const NeverScrollableScrollPhysics(),
              controller: mainTabLayoutController.pageController,
              children: [
                items[mainTabLayoutController.currentIndex].screen,
                const CategoryWiseLogins()
              ]),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: const Color(0xFFFFFFFF),
            fixedColor: const Color(0xFF22B70A),
            currentIndex: mainTabLayoutController.currentIndex,
            onTap: mainTabLayoutController.changeIndex,
            items: items
                .map(
                  (e) => BottomNavigationBarItem(
                      icon: e.label.isEmpty
                          ? SizedBox(
                              height: 45,
                              width: 45,
                              child: e.icon,
                            )
                          : e.icon,
                      label: e.label,
                      activeIcon: e.activeIcon),
                )
                .toList(),
          ),
          // bottomNavigationBar: uiUtils.checkPlatformIsAndroidOrIos()
          //     ? buildBottomNavigationBar()
          //     : const SizedBox.shrink(),
        );
      }),
    );
  }

  // Widget buildBottomNavigationBar() {
  //   return SizedBox(
  //     height: 80,
  //     child:
  //   );
  // }
}
