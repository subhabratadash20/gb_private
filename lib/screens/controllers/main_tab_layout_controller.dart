import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class BoardingItem {
  final String label;
  final Widget icon;
  final Widget screen;
  final Widget activeIcon;

  BoardingItem({
    required this.label,
    required this.icon,
    required this.screen,
    required this.activeIcon,
  });
}

class MainTabLayoutController extends GetxController {
  PageController pageController = PageController();
  String selectedCategory = "";
  bool isLoginSelected = true;
  int currentIndex = 0;

  void changeIndex(int newIndex) {
    currentIndex = newIndex;
    update();
  }
}
