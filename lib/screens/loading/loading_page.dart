import 'dart:async';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/routes.dart';
import 'package:gb_private/services/info_service.dart';
import 'package:gb_private/utils/global.dart';
import 'package:get/get.dart';

class LoadingPage extends StatefulWidget {
  const LoadingPage({super.key});

  @override
  State<LoadingPage> createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  InfoService infoService = InfoService();
  bool _showButton = false;

  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 3), () {
      setState(() {
        _showButton = true;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFF17A600),
      body: Stack(
        alignment: Alignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  'never\nforget your',
                  style: TextStyle(
                    height: 1.1,
                    color: Colors.white54,
                    fontSize: 60,
                    fontFamily: 'CenturyGothicPro',
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    height: 68,
                    child: DefaultTextStyle(
                      style: const TextStyle(
                        height: 1.1,
                        color: Colors.white,
                        fontSize: 60,
                        fontFamily: 'CenturyGothicPro',
                        fontWeight: FontWeight.bold,
                      ),
                      child: AnimatedTextKit(
                        animatedTexts: [
                          FadeAnimatedText('Password'),
                          FadeAnimatedText('Document'),
                          FadeAnimatedText('Login'),
                          FadeAnimatedText('Note'),
                        ],
                        isRepeatingAnimation: true,
                        totalRepeatCount: 20,
                      ),
                    ),
                  ),
                ),
                const Text(
                  'again',
                  style: TextStyle(
                    height: 1.1,
                    color: Colors.white54,
                    fontSize: 60,
                    fontFamily: 'CenturyGothicPro',
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 40),
              ],
            ),
          ),
          if (_showButton)
            Positioned(
              left: 20,
              right: 20,
              bottom: 40,
              child: Container(
                height: 45,
                decoration: BoxDecoration(
                  border: Border.all(color: Colors.white70),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.transparent,
                      shadowColor: Colors.transparent),
                  onPressed: () async {
                    var token = await readTokenFromDB();
                    if (token == null) {
                      Get.offAllNamed(Routers.masterPassRoute);
                    } else {
                      Get.offAllNamed(Routers.authenticationRoute,
                          arguments: [token]);
                    }
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        'Set Master Password',
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: 'Rubick',
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                      SvgPicture.asset('assets/icons/arrow-right-button.svg'),
                    ],
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }

  readTokenFromDB() async {
    return await infoService.read(Global.keySessionUserId);
  }
}
