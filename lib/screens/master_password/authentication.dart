import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/routes.dart';
import 'package:get/get.dart';

class AuthenticateUser extends StatefulWidget {
  final String? tokenValue;

  const AuthenticateUser({super.key, this.tokenValue});

  @override
  State<AuthenticateUser> createState() => _AuthenticateUserState();
}

class _AuthenticateUserState extends State<AuthenticateUser> {
  bool _isAuthenticated = false;
  bool _isPasswordValid = false;
  String password = '';

  checkAuth(String value) async {
    if (value == widget.tokenValue) {
      Timer(
        const Duration(milliseconds: 500),
        () {
          Get.offAllNamed(Routers.mainScreen);
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _isAuthenticated
                  ? SvgPicture.asset(
                      'assets/images/Master-password-auth-green.svg')
                  : SvgPicture.asset(
                      'assets/images/Master-password-auth-grey.svg'),
              const SizedBox(height: 36),
              const Text(
                'WELCOME BACK',
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.bold,
                    color: Color(0XFF6E8A9E)),
              ),
              const SizedBox(height: 4),
              const Text(
                'Please enter master password',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 24),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 5,
                      offset: const Offset(0, 2),
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    TextField(
                      style: TextStyle(
                          color: _isPasswordValid ? Colors.green : Colors.grey),
                      obscureText: true,
                      obscuringCharacter: '●',
                      onChanged: (value) async {
                        await checkAuth(value);
                        setState(() {
                          password = value;
                          if (widget.tokenValue == value) {
                            _isAuthenticated = true;
                            _isPasswordValid = true;
                          } else {
                            _isAuthenticated = false;
                            _isPasswordValid = false;
                          }
                        });
                      },
                      decoration: InputDecoration(
                        suffixIcon: Transform.scale(
                            scale: 0.5,
                            child: _isAuthenticated
                                ? SvgPicture.asset(
                                    'assets/icons/checked-green.svg')
                                : !_isPasswordValid && password.isNotEmpty
                                    ? SvgPicture.asset(
                                        'assets/icons/checked-red.svg')
                                    : SvgPicture.asset(
                                        'assets/icons/checked-grey.svg')),
                        hintText: 'Enter Password',
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide: BorderSide(
                              color:
                                  _isPasswordValid ? Colors.white : Colors.red,
                              width: 2),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10),
                          borderSide:
                              const BorderSide(color: Colors.white, width: 1),
                        ),
                        contentPadding: const EdgeInsets.symmetric(
                            horizontal: 16,
                            vertical: 16), // Adjust the horizontal padding
                      ),
                    ),
                  ],
                ),
              ),
              if (!_isPasswordValid && password.isNotEmpty)
                const Padding(
                  padding: EdgeInsets.only(top: 8),
                  child: Text(
                    'Incorrect Password',
                    style: TextStyle(color: Colors.red),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
