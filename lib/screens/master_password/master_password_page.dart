import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/routes.dart';
import 'package:gb_private/services/auth_service.dart';
import 'package:get/get.dart';

class MasterPasswordPage extends StatefulWidget {
  const MasterPasswordPage({super.key});

  @override
  State<MasterPasswordPage> createState() => _MasterPasswordPageState();
}

class _MasterPasswordPageState extends State<MasterPasswordPage> {
  bool _obscureText = true;
  bool _obscureRepsd = true;

  void _togglePasswordStatus() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  void _toggleRePasswordStatus() {
    setState(() {
      _obscureRepsd = !_obscureRepsd;
    });
  }

  String password = '';
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _pass = TextEditingController();
  final TextEditingController _retypePass = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          leading: const Icon(Icons.arrow_back_ios),
        ),
        body: SingleChildScrollView(
            child: Container(
                padding: const EdgeInsets.all(24.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Center(
                        child: password == ''
                            ? SvgPicture.asset(
                                'assets/images/Master-password-vector-grey.svg',
                                height: 126,
                              )
                            : SvgPicture.asset(
                                'assets/images/Master-password-vector.svg',
                                height: 126,
                              )),
                    const SizedBox(height: 16),
                    // Image.asset('assets/illustration.png'), // Replace with your asset image path

                    const Text('MASTER PASSWORD',
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Color(0XFF6E8A9E))),
                    const SizedBox(height: 4),
                    const Text(
                      'Set a master password',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF2E2E2E)),
                    ),
                    const SizedBox(height: 12),
                    const Text(
                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore.',
                      style: TextStyle(fontSize: 12, color: Color(0xFF585858)),
                    ),
                    const SizedBox(height: 16),
                    Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            TextFormField(
                              controller: _pass,
                              onChanged: (value) {
                                setState(() {
                                  password = value;
                                });
                              },
                              decoration: InputDecoration(
                                labelText: 'Password',
                                labelStyle:
                                    const TextStyle(color: Color(0xFFABABAB)),
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                // border: OutlineInputBorder(),
                                filled: true,
                                fillColor: const Color(0xFFF2F2F2),
                                focusedBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(width: 1, color: Colors.red),
                                ),
                                disabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide(
                                      width: 1, color: Colors.orange),
                                ),
                                enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(width: 1, color: Colors.green),
                                ),
                                errorBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.black)),
                                focusedErrorBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.yellowAccent)),
                                border: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide: BorderSide(
                                    width: 1,
                                  ),
                                ),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    _obscureText
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                  ),
                                  onPressed: _togglePasswordStatus,
                                  color: const Color(0xFFA9C4D8),
                                ),
                              ),
                              obscureText: _obscureText,
                              obscuringCharacter: '*',
                              validator: (val) {
                                if (val!.isEmpty) return 'Empty';
                                return null;
                              },
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            TextFormField(
                              controller: _retypePass,
                              validator: (val) {
                                if (val!.isEmpty) return 'Empty';
                                if (val != _pass.text) return 'Not Match';
                                return null;
                              },
                              decoration: InputDecoration(
                                labelText: 'Re-type Password',
                                labelStyle:
                                    const TextStyle(color: Color(0xFFABABAB)),
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 10),
                                //border: OutlineInputBorder(),
                                filled: true,
                                fillColor: const Color(0xFFF2F2F2),
                                focusedBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(width: 1, color: Colors.red),
                                ),

                                enabledBorder: const OutlineInputBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(4)),
                                  borderSide:
                                      BorderSide(width: 1, color: Colors.green),
                                ),
                                errorBorder: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide(
                                        width: 1, color: Colors.black)),

                                border: const OutlineInputBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(4)),
                                    borderSide: BorderSide(
                                      width: 1,
                                    )),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    _obscureRepsd
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                  ),
                                  onPressed: _toggleRePasswordStatus,
                                  color: const Color(0xFFA9C4D8),
                                ),
                              ),
                              obscureText: _obscureRepsd,
                              obscuringCharacter: '*',
                            ),
                          ],
                        )),

                    const SizedBox(
                      height: 10,
                    ),

                    Table(
                      children: [
                        TableRow(
                          children: [
                            SizedBox(
                              height: 25,
                              child: passwordRequirement(
                                  'One lowercase character',
                                  password.contains(RegExp(r'[a-z]'))),
                            ),
                            SizedBox(
                              height: 25,
                              child: passwordRequirement(
                                  'One uppercase character',
                                  password.contains(RegExp(r'[A-Z]'))),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            SizedBox(
                              height: 25,
                              child: passwordRequirement('One number',
                                  password.contains(RegExp(r'[0-9]'))),
                            ),
                            SizedBox(
                              height: 25,
                              child: passwordRequirement(
                                  'One special character',
                                  password.contains(RegExp(r'[!@#\$&*~]'))),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            SizedBox(
                              height: 25,
                              child: passwordRequirement(
                                  '8 characters minimum', password.length >= 8),
                            ),
                            Container(),
                            // Empty container for symmetry
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),

                    Container(
                      alignment: Alignment.center,
                      padding: const EdgeInsets.all(10),
                      child: Center(
                          child: Text.rich(TextSpan(
                              text:
                                  'By selecting Agree and continue, I agree to  Dynamic Layers ',
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 12,
                                  color: Color(0xFF000000)),
                              children: <TextSpan>[
                            TextSpan(
                                text: 'Terms of Service',
                                style: const TextStyle(
                                  fontSize: 12,
                                  color: Color(0xFF2EBA0B),
                                  decoration: TextDecoration.underline,
                                ),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    // code to open / launch terms of service link here
                                  }),
                          ]))),
                    ),
                    const SizedBox(height: 16),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: password == ''
                              ? const Color(0XFFD7DEF0)
                              : const Color(0XFF2EBA0B),
                          //,
                          padding: const EdgeInsets.symmetric(vertical: 16),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                        onPressed: () async {
                          if (_pass.text == _retypePass.text) {
                            await AuthService()
                                .getSaveMasterPassword(_pass.text);
                            // Handle Agree & Continue

                            Get.offAllNamed(Routers.mainScreen);
                          }
                        },
                        child: const Text(
                          'Agree & Continue',
                          style: TextStyle(fontSize: 16, color: Colors.white),
                        ),
                      ),
                    ),
                  ],
                ))));
  }

  Widget passwordRequirement(String requirement, bool isMet) {
    return Row(
      children: [
        Text('•',
            style: TextStyle(
                color: isMet ? Colors.green : Colors.red, fontSize: 20)),
        const SizedBox(width: 5),
        Expanded(
            child: Text(
          requirement,
          style: const TextStyle(fontSize: 10),
        )),
      ],
    );
  }
}
