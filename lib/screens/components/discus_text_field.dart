import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class DiscusFormTextField extends StatelessWidget {
  final String? label;
  final String? labelText;
  final bool? strike;
  final bool? showCursor;
  final bool readOnly;
  final GestureTapCallback? onTap;
  final ValueChanged<String>? onChanged;
  final VoidCallback? onEditingComplete;
  final ValueChanged<String>? onFieldSubmitted;
  final TextCapitalization textCapitalization;

  final bool obscureText;
  final TextInputType? textInputType;
  final FormFieldSetter<String>? onSaved;
  final bool autocorrect;
  final FormFieldValidator<String>? validator;
  final String? errorText;
  final String? initialValue;
  final TextEditingController? controller;
  final int maxLines;
  final List<TextInputFormatter>? inputFormatters;
  final bool autofocus;
  final bool isDense;
  final String? hintText;
  final TextInputAction? textInputAction;
  final Widget? suffix;
  final Widget? prefix;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final InputBorder? border;
  final FocusNode? focusNode;
  final int? errorMaxLines;
  final TextInputType? keyboardType;
  final int? minLines;
  final double? horizontal;
  final bool? isPswMatch;
  final int? inputLength;

  const DiscusFormTextField({
    Key? key,
    this.label = " ",
    this.labelText,
    this.strike = false,
    this.showCursor = true,
    this.readOnly = false,
    this.onTap,
    this.onChanged,
    this.onEditingComplete,
    this.onFieldSubmitted,
    this.textCapitalization = TextCapitalization.none,
    this.textInputType,
    this.onSaved,
    this.autocorrect = false,
    this.obscureText = false,
    this.validator,
    this.errorText,
    this.initialValue,
    this.controller,
    this.maxLines = 1,
    this.inputFormatters,
    this.autofocus = false,
    this.isDense = false,
    this.hintText,
    this.textInputAction,
    this.suffix,
    this.suffixIcon,
    this.prefix,
    this.prefixIcon,
    this.border,
    this.focusNode,
    this.errorMaxLines = 1,
    this.keyboardType,
    this.minLines,
    this.horizontal = 10,
    this.isPswMatch,
    this.inputLength,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
