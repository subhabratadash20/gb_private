import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:velocity_x/velocity_x.dart';

class DiscusAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;

  final String title;
  final List<Widget> iconWidget;
  final bool showIcon;
  final bool isShowLeadingIcon;
  final String? backRouter;
  final String? toolTipTextAppBar;
  final bool showToolTip;

  const DiscusAppBar(this.title, this.iconWidget,
      {Key? key,
      this.showIcon = false,
      this.isShowLeadingIcon = false,
      this.backRouter,
      required this.showToolTip,
      this.toolTipTextAppBar})
      : preferredSize = const Size.fromHeight(50.0),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0.1,
      centerTitle: true,
      leading: GestureDetector(
        onTap: () {
          Get.back();
        },
        child: Container(
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            // Customize the button color
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.03),
                spreadRadius: 1,
                blurRadius: 1,
                offset: const Offset(0, 1),
              ),
            ],
          ),
          child: const Icon(
            Icons.arrow_back,
          ),
        ),
      ),
      title: showToolTip
          ? Tooltip(
              message: toolTipTextAppBar ?? "",
              child: title.text.xl.ellipsis.make(),
            )
          : title.text.xl.ellipsis.make(),
      actions: <Widget>[
        Visibility(
          visible: showIcon,
          child: HStack(iconWidget, alignment: MainAxisAlignment.spaceBetween)
              .px12(),
        ),
      ],
    );
  }
}
