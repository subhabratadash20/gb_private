import 'dart:convert';
import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/screens/login/add_login_page.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:velocity_x/velocity_x.dart';

import '../../models/login/login.dart';

class LoginDetail extends StatefulWidget {
  final LoginModel loginModel;
  final int boxItemIndex;

  const LoginDetail(
      {super.key, required this.loginModel, required this.boxItemIndex});

  @override
  State<LoginDetail> createState() => _LoginDetailState();
}

class ImageModel {
  final String id;
  final String imagePath;

  ImageModel({
    required this.id,
    required this.imagePath,
  });
}

class _LoginDetailState extends State<LoginDetail> {
  final List<ImageModel> imageUrls = [];
  int _currentIndex = 0;

  List<LoginField> fields = [];
  final bool _isFavorite = false;
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      fields.addAll(
        [
          LoginField(
            title: 'Title',
            value: widget.loginModel.title,
          ),
          LoginField(
            title: 'Website',
            value: widget.loginModel.website,
          ),
          LoginField(
            title: 'Username',
            value: widget.loginModel.userName,
            isCopy: true,
          ),
          LoginField(
            title: 'Password',
            value: widget.loginModel.password ?? "",
            isHide: true,
            isCopy: true,
          ),
        ],
      );
      widget.loginModel.additionalFields != null &&
              widget.loginModel.additionalFields!.isNotEmpty
          ? fields.addAll(widget.loginModel.additionalFields!
              .map(
                (e) => LoginField(
                  title: e['name'],
                  value: e['controller'],
                  isCopy: e['type'] == "username",
                  // isHide: true,
                  // isCopy: true,
                ),
              )
              .toList())
          : null;
      setState(() {});
    });
    // fields.add(LoginField(title: title, value: value));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: const Icon(
            Icons.arrow_back_ios,
            size: 18,
            color: Color(0XFF6E8A9E),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            widget.loginModel.icons == null || widget.loginModel.icons!.isEmpty
                ? Container(
                    height: 80,
                    width: double.infinity,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(8)),
                        color: Colors.white70),
                    padding: const EdgeInsets.symmetric(horizontal: 36),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset('assets/icons/Document.svg'),
                        const SizedBox(width: 8),
                        const Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Not Uploaded yet,',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xFF344C5D),
                                  fontSize: 14),
                            ),
                            Text(
                              'Your files will appear here',
                              style: TextStyle(
                                fontWeight: FontWeight.w300,
                                color: Color(0xFF6E8A9E),
                                fontSize: 14,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                : Container(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    margin: const EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                        // Set color based on index
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: const Color(0XFF929CAA)
                                .withOpacity(.3), // Shadow color
                            // spreadRadius: 4, // Spread radius
                            blurRadius: 30, // Blur radius
                            offset: const Offset(
                                0, 4), // Offset in the x, y direction
                          ),
                        ],
                        borderRadius: BorderRadius.circular(10)),
                    child: Column(
                      children: [
                        Stack(
                          alignment: Alignment.bottomCenter,
                          children: [
                            CarouselSlider(
                              options: CarouselOptions(
                                aspectRatio: 16 / 1,
                                enlargeCenterPage: true,
                                autoPlay: true,
                                onPageChanged: (index, _) {
                                  setState(() {
                                    _currentIndex = index;
                                  });
                                },
                                // Show indicators
                                height: 200,
                                // Adjust the height as needed
                                viewportFraction: 1.0,
                                enableInfiniteScroll:
                                    false, // Set to true if you want infinite scroll
                              ),
                              items:
                                  widget.loginModel.icons!.mapIndexed((img, i) {
                                return Hero(
                                  tag: i
                                      .toString(), // Unique tag for hero animation
                                  child: Container(
                                    width: MediaQuery.sizeOf(context).width,
                                    margin: const EdgeInsets.symmetric(
                                        horizontal: 5.0),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(8.0),
                                      child: Stack(
                                        fit: StackFit.expand,
                                        children: [
                                          if (img![0] == "P")
                                            SvgPicture.memory(
                                              (const Base64Decoder()
                                                  .convert(img)),
                                              fit: BoxFit.cover,
                                            )
                                          else if (img[0] == 'J')
                                            Container(
                                              decoration: BoxDecoration(
                                                color: Colors.grey
                                                    .withOpacity(0.2),
                                              ),
                                              alignment: Alignment.center,
                                              child: const Text(
                                                'PDF',
                                                style: TextStyle(
                                                  color: Color(0xff2e2e2e),
                                                  fontFamily: 'Poppins',
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              ),
                                            )
                                          else
                                            Image.memory(
                                              (const Base64Decoder()
                                                  .convert(img)),
                                              fit: BoxFit.cover,
                                            ),
                                          Positioned(
                                            top: 10,
                                            right: 10,
                                            child: GestureDetector(
                                              onTap: () {
                                                Get.to(
                                                  () => FullScreenImage(
                                                    image: widget
                                                            .loginModel.icons ??
                                                        [],
                                                    tag: i.toString(),
                                                  ),
                                                );
                                              },
                                              child: SvgPicture.asset(
                                                'assets/icons/fullscreen-fill.svg',
                                                fit: BoxFit.none,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              }).toList(),
                            ),
                            widget.loginModel.icons == null
                                ? const SizedBox()
                                : Positioned(
                                    bottom: 19,
                                    child: AnimatedSmoothIndicator(
                                      activeIndex: _currentIndex,
                                      count: widget.loginModel.icons!.length,
                                      effect: const WormEffect(
                                        radius: 10,
                                        dotHeight: 10,
                                        dotWidth: 10,
                                        activeDotColor: Colors.white,
                                        dotColor: Color(0xffD9D9D9),
                                      ),
                                    ),
                                  ),
                          ],
                        ),
                        const SizedBox(
                          height: 15,
                        ),

                        // const SizedBox(
                        //   height: 15,
                        // ),
                      ],
                    ),
                  ),
            const SizedBox(
              height: 15,
            ),
            Container(
              padding: const EdgeInsets.all(10),
              margin: const EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  // Set color based on index
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: const Color(0XFF929CAA)
                          .withOpacity(.3), // Shadow color
                      // spreadRadius: 4, // Spread radius
                      blurRadius: 30, // Blur radius
                      offset:
                          const Offset(0, 4), // Offset in the x, y direction
                    ),
                  ],
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  LoginIcon(
                      icon: SvgPicture.asset(
                          'assets/icons/${widget.loginModel.category}.svg'),
                      name: widget.loginModel.category ?? ""),
                  LoginIcon(
                    onPressed: () {
                      widget.loginModel.isFavorite =
                          !widget.loginModel.isFavorite;
                      widget.loginModel.save();
                      setState(() {});
                    },
                    icon: SvgPicture.asset(
                      'assets/icons/Star-Yellow.svg',
                      color: widget.loginModel.isFavorite
                          ? null
                          : const Color(0xFFD1DBE7),
                    ),
                    name: widget.loginModel.isFavorite
                        ? 'Favorite'
                        : 'Not Favorite',
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (_) => AddLoginPage(
                            isEdit: true,
                            login: widget.loginModel,
                            boxItemIndex: widget.boxItemIndex,
                          ),
                        ),
                      );
                    },
                    child: LoginIcon(
                      icon: SvgPicture.asset('assets/icons/edit.svg'),
                      name: 'Edit',
                    ),
                  ),
                  LoginIcon(
                      icon: SvgPicture.asset('assets/icons/Delete.svg'),
                      onPressed: () {
                        widget.loginModel.delete();
                        Get.back();
                      },
                      name: 'Delete'),
                ],
              ),
            ),
            const SizedBox(
              height: 24,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              margin: const EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                  // Set color based on index
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: const Color(0XFF929CAA)
                          .withOpacity(.3), // Shadow color
                      // spreadRadius: 4, // Spread radius
                      blurRadius: 30, // Blur radius
                      offset:
                          const Offset(0, 4), // Offset in the x, y direction
                    ),
                  ],
                  borderRadius: BorderRadius.circular(10)),
              child: ListView.separated(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) => Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 4),
                      child: fields[index]),
                  separatorBuilder: (context, index) =>
                      const Divider(color: Color(0XFFD1DDED)),
                  itemCount: fields.length),
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}

class LoginIcon extends StatelessWidget {
  final Widget icon;
  final String name;
  final Function()? onPressed;
  const LoginIcon(
      {super.key, required this.icon, required this.name, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      splashColor: Colors.transparent,
      onPressed: onPressed,
      icon: Column(
        children: [
          SizedBox(
            height: 35,
            child: icon,
          ),
          Text(
            name,
            style: const TextStyle(
              color: Color(0xff2e2e2e),
              fontFamily: 'Poppins',
              fontSize: 12,
              fontWeight: FontWeight.w500,
            ),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}

class LoginField extends StatefulWidget {
  final String title;
  final String value;
  bool isCopy;
  bool isHide;

  LoginField(
      {super.key,
      required this.title,
      required this.value,
      this.isHide = false,
      this.isCopy = false});

  @override
  State<LoginField> createState() => _LoginFieldState();
}

class _LoginFieldState extends State<LoginField> {
  bool _showValue = false;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.title,
              style: const TextStyle(
                fontFamily: 'Poppins',
                fontSize: 14,
                fontWeight: FontWeight.w400,
                color: Color(0xff585858),
              ),
            ),
            widget.isHide
                ? SizedBox(
                    width: 200,
                    child: TextFormField(
                      readOnly: true,
                      obscureText: !_showValue,
                      initialValue: widget.value,
                      obscuringCharacter: '●',
                      style: !_showValue
                          ? const TextStyle(color: Color(0XFFD1DDED))
                          : null,
                      decoration: const InputDecoration.collapsed(
                        hintText: '',
                        border: InputBorder.none,
                      ),
                    ),
                  )
                : Text(
                    widget.value,
                    style: const TextStyle(
                      fontWeight: FontWeight.w500,
                      fontFamily: "Poppins-Regular",
                      fontSize: 18,
                    ),
                  ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Visibility(
              visible: widget.isHide,
              child: InkWell(
                onTap: () {
                  setState(() {
                    _showValue = !_showValue;
                  });
                },
                child: Icon(
                    _showValue ? Icons.visibility : Icons.visibility_off,
                    color: const Color(0xFFA9C4D8)),
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            Visibility(
              visible: widget.isCopy,
              child: GestureDetector(
                onTap: () async {
                  if (widget.isCopy) {
                    await Clipboard.setData(ClipboardData(text: widget.value));
                  }
                },
                child: SvgPicture.asset('assets/icons/Copy.svg'),
              ),
            ),
          ],
        )
      ],
    );
  }
}

class FullScreenImage extends StatefulWidget {
  final List<String?> image;
  final String tag;

  const FullScreenImage({
    super.key,
    required this.image,
    required this.tag,
  });

  @override
  State<FullScreenImage> createState() => _FullScreenImageState();
}

class _FullScreenImageState extends State<FullScreenImage> {
  createPdf({required String base64}) async {
    var bytes = base64Decode(base64);
    final output = await getTemporaryDirectory();
    final file = File(
        "${output.path}/exam${DateTime.now().microsecondsSinceEpoch}ple.pdf");
    return file.writeAsBytesSync(bytes.buffer.asUint8List());
  }

  int page = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: () {
          Get.back(); // Navigate back using GetX
        },
        child: SizedBox(
          height: MediaQuery.sizeOf(context).height,
          width: MediaQuery.sizeOf(context).width,
          child: Hero(
            tag: widget.tag, // Use the same tag as in the carousel item

            child: Stack(
              children: [
                PageView.builder(
                  itemCount: widget.image.length,
                  onPageChanged: (value) {
                    setState(() {
                      page = value;
                    });
                  },
                  itemBuilder: (context, index) {
                    final file = widget.image[index];

                    return file![0] == 'J'
                        ?
                        //  PDFView(
                        //     filePath: createPdf(base64: file),
                        //   )
                        const SizedBox()
                        : file[0] == 'P'
                            ? SvgPicture.memory(
                                (const Base64Decoder().convert(file)),
                                fit: BoxFit.fitHeight,
                              )
                            : Image.memory(
                                (const Base64Decoder().convert(file)),
                                fit: BoxFit.fitHeight,
                              );
                  },
                ),
                Positioned(
                  right: 10,
                  top: 30,
                  child: Row(
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: const Color(0xff9498AF).withOpacity(0.35),
                          borderRadius: BorderRadius.circular(4),
                        ),
                        padding: const EdgeInsets.all(4),
                        child: Text(
                          '${page + 1} / ${widget.image.length}',
                          style: const TextStyle(
                            fontFamily: 'Poppins',
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      const SizedBox(width: 5),
                      InkWell(
                        onTap: () {
                          Get.back();
                        },
                        child: SvgPicture.asset(
                          'assets/icons/close.svg',
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
