import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/screens/controllers/main_tab_layout_controller.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../../models/hive_box_models/hive_box_models.dart';
import '../../models/login/login.dart';
import '../../routes.dart';
import '../widgets/login_card.dart';

class CategoryWiseLogins extends StatefulWidget {
  const CategoryWiseLogins({super.key});

  @override
  State<CategoryWiseLogins> createState() => _CategoryWiseLoginsState();
}

class _CategoryWiseLoginsState extends State<CategoryWiseLogins>
    with SingleTickerProviderStateMixin {
  String searchVal = "";
  TabController? tabController;
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<MainTabLayoutController>(
        builder: (mainTabLayoutController) {
      return Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Get.toNamed(
              Routers.addLoginRoute,
            );
          },
          backgroundColor: const Color(0xFF22B70A),
          shape: const CircleBorder(),
          child: const Icon(Icons.add,
              color: Colors.white, size: 40), // Set the icon color to green
        ),
        appBar: AppBar(
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios,
              size: 18,
              color: Color(0XFF6E8A9E),
            ),
            onPressed: () {
              mainTabLayoutController.pageController.previousPage(
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.linear);
              mainTabLayoutController.selectedCategory = "";
              setState(() {});
            },
          ), // actions: const [
          //   Padding(
          //     padding: EdgeInsets.only(right: 8),
          //     child: SizedBox(
          //       height: 35,
          //       width: 35,
          //       child: CircleAvatar(
          //         backgroundImage: AssetImage(
          //           'assets/images/user-fill.png',
          //         ),
          //       ),
          //     ),
          //   )
          // ],
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(kToolbarHeight),
            child: TabBar(
              controller: tabController,
              tabAlignment: TabAlignment.start,
              isScrollable: true, // Make tabs scrollable
              tabs: const [
                Tab(
                  child: Text(
                    'All',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                Tab(
                  child: Text(
                    'Favorites',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: ValueListenableBuilder<Box<LoginModel>>(
            valueListenable: Boxes.getLogInsData().listenable(),
            builder: (context, box, _) {
              final logins = box.values
                  .where((element) =>
                      element.category ==
                      mainTabLayoutController.selectedCategory)
                  .toList();
              return Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                            color: const Color(0XFFD1DDED), width: 1),
                        borderRadius: BorderRadius.circular(8),
                      ),
                      child: TextField(
                        onChanged: (value) {
                          setState(() {
                            searchVal = value;
                          });
                        },
                        decoration: InputDecoration(
                          hintText: 'Search here',
                          hintStyle: const TextStyle(
                            fontFamily: 'Poppins',
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                            color: Color(0xff9498AF),
                          ),
                          suffixIcon: Transform.scale(
                            scale: 0.5,
                            child: SvgPicture.asset(
                              'assets/icons/Search.svg',
                              width: 10,
                            ),
                          ),
                          contentPadding:
                              const EdgeInsets.symmetric(horizontal: 10),
                          border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(8),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 16),

                    // Adjust spacing as needed
                    Expanded(
                      child: TabBarView(
                        controller: tabController,
                        children: [
                          LoginCard(
                            logins: searchVal.isEmpty
                                ? logins
                                : logins
                                    .where((element) => element.title
                                        .toLowerCase()
                                        .contains(searchVal.toLowerCase()))
                                    .toList(),
                          ),
                          LoginCard(
                            logins: searchVal.isEmpty
                                ? logins
                                    .where((element) => element.isFavorite)
                                    .toList()
                                : logins
                                    .where((element) =>
                                        element.isFavorite &&
                                        element.title
                                            .toLowerCase()
                                            .contains(searchVal.toLowerCase()))
                                    .toList(),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            }),
      );
    });
  }
}
