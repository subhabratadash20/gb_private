import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/models/login/login.dart';
import 'package:gb_private/routes.dart';
import 'package:gb_private/screens/login/login_list_page.dart';
import 'package:gb_private/services/info_service.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:get/get.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../../models/hive_box_models/hive_box_models.dart';

class LoginDetails extends StatefulWidget {
  const LoginDetails({super.key});

  @override
  State<LoginDetails> createState() => _LoginDetailsState();
}

class _LoginDetailsState extends State<LoginDetails> {
  static const String _me = 'LoginPageState';
  static final LogUtil _logger = LogUtil(_me);
  // final UIUtils uiUtils = UIUtils(_logger);
  InfoService infoService = InfoService();

  get backIcon => const Icon(Icons.arrow_back_ios);

  late Future<void> loginsFuture;

  List<LoginModel> logins = [];

  List<LoginModel> favoriteLogins = [];

  @override
  void initState() {
    super.initState();
    initializeState();
  }

  void initializeState() {
    loginsFuture = _readAllLogins();
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<Box<LoginModel>>(
        valueListenable: Boxes.getLogInsData().listenable(),
        builder: (context, box, _) {
          logins = box.values.toList();
          return Scaffold(
              body: logins.isNotEmpty
                  ? LoginListPage(logins: logins)
                  : Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                              'assets/images/No login added-vector.svg'),
                          const SizedBox(
                            height: 24,
                          ),
                          const Text(
                            'No login added',
                            style: TextStyle(
                              color: Color(0xFF2E2E2E),
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Outfit-Bold',
                            ),
                          ),
                          const Text(
                            'All logins will appear here',
                            style: TextStyle(
                              color: Color(0xFF6E8A9E),
                              fontSize: 16,
                              fontFamily: 'Poppins-Regular',
                            ),
                          ),
                        ],
                      ),
                    ),
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  Get.toNamed(Routers.addLoginRoute, arguments: [logins]);
                },
                backgroundColor: const Color(0xFF22B70A),
                shape: const CircleBorder(),
                child: const Icon(Icons.add,
                    color: Colors.white,
                    size: 40), // Set the icon color to green
              ));
        });
  }

  Future<void> _readAllLogins() async {
    final allLogins = Boxes.getLogInsData().values.toList();
    final favLogins = allLogins.where((element) => element.isFavorite).toList();
    if (mounted) {
      setState(() {
        logins = allLogins;
        favoriteLogins = favLogins;
      });
    }
  }
}
