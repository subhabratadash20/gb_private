import 'dart:convert';
import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/models/addfield_model.dart';
import 'package:gb_private/models/drag_drop_data_model.dart';
import 'package:gb_private/models/login/login.dart';
import 'package:gb_private/routes.dart';
import 'package:gb_private/screens/widgets/file_upload_container.dart';
import 'package:gb_private/screens/controllers/image_picker_controller.dart';
import 'package:gb_private/screens/widgets/expansion_tiles/category_picker_expansiontile.dart';
import 'package:gb_private/screens/widgets/fields/custom_add_details_field.dart';
import 'package:gb_private/utils/log_util.dart';
import 'package:gb_private/utils/themes.dart';
import 'package:gb_private/utils/ui_utils.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';

import '../../models/hive_box_models/hive_box_models.dart';
import '../widgets/custom_dialogue_option_for_filepicker.dart';
import '../widgets/dialogues/custom_add_new_field_dialogue.dart';

class AddLoginPage extends StatefulWidget {
  final LoginModel? login;
  final bool isEdit;
  final int? boxItemIndex;
  const AddLoginPage({
    super.key,
    this.login,
    this.isEdit = false,
    this.boxItemIndex,
  });

  @override
  State<AddLoginPage> createState() => _AddLoginPageState();
}

class _AddLoginPageState extends State<AddLoginPage>
    with SingleTickerProviderStateMixin {
  static final LogUtil _logger = LogUtil("me");
  final UIUtils uiUtils = UIUtils(_logger);
  TextEditingController _titleController = TextEditingController();
  // final TextEditingController _userNameController = TextEditingController();
  // final TextEditingController _passwordController = TextEditingController();
  // final TextEditingController _websiteController = TextEditingController();

  // final TextEditingController _categoryController = TextEditingController();
  bool _isFavourite = false;
  String addFiledText = '';

  File? _selectedImage;
  List<String> fileList = [];

  void _handleIconSelection(String iconPath) async {
    try {
      File imageFile = await createFileFromAsset(iconPath);
      setState(() {
        _selectedImage = imageFile;
      });
      // Now you have the File representing the selected image
      print("Selected image file path: ${imageFile.path}");
    } catch (e) {
      print("Error creating file: $e");
    }
  }

  Future<File> createFileFromAsset(String assetPath) async {
    // Load the asset image
    ByteData data = await rootBundle.load(assetPath);
    List<int> bytes = data.buffer.asUint8List();

    // Create a temporary directory
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;

    // Write the image to a temporary file
    File file = File(
        '$tempPath/${DateTime.now().millisecondsSinceEpoch}.${assetPath.split(".").last}');
    await file.writeAsBytes(bytes);

    return file;
  }

  _showLibraryDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          contentPadding: EdgeInsets.zero,
          backgroundColor: Colors.white,
          content: Container(
            width: 400,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                color: const Color(0xFFFFFFFF),
                borderRadius: BorderRadius.circular(24)),
            height: 180,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const SizedBox(),
                DialogOption(
                  icon: Image.asset('assets/images/library.png',
                      color: const Color(0xFFA9C4D8)),
                  text: "Icon Library",
                  onTap: () async {
                    Navigator.of(context).pop();
                    final filePath = await Get.toNamed(Routers.iconGallery,
                        arguments: await ImagePickerCotroller().initImages());
                    _selectedImage = await createFileFromAsset(filePath);
                    setState(() {});
                  },
                ),
                DialogOption(
                  icon: Image.asset('assets/images/camera-fill.png',
                      color: const Color(0xFFA9C4D8)),
                  text: "Camera",
                  onTap: () async {
                    Navigator.of(context).pop();
                    _selectedImage =
                        await ImagePickerCotroller().captureImageFromCamera() ??
                            _selectedImage;
                    setState(() {});
                  },
                ),
                DialogOption(
                  icon: Image.asset(
                    'assets/images/gallery-fill.png',
                    color: const Color(0xFFA9C4D8),
                  ),
                  text: "Gallery",
                  onTap: () async {
                    Navigator.of(context).pop();
                    _selectedImage =
                        await ImagePickerCotroller().getImageFromGallery() ??
                            _selectedImage;
                    setState(() {});
                  },
                ),
                const SizedBox(),
                const SizedBox(),
              ],
            ),
          ),
        );
      },
    );
  }

  _addFieldButton({required Widget child}) => DottedBorder(
      padding: const EdgeInsets.only(top: 12),
      borderType: BorderType.RRect,
      radius: const Radius.circular(10),
      color: kPrimaryColor,
      child: child);

  // _buildDashedBorder({required Widget child}) => DottedBorder(
  //     borderType: BorderType.RRect,
  //     radius: const Radius.circular(25),
  //     color: kPrimaryColor,
  //     child: child);

  List<DragDropData> dragDropDataList = [];

  List<ImageData> addFiledList = [
    ImageData(
      imagePath: "assets/images/user-fill.png",
      name: "Username",
    ),
    ImageData(
      imagePath: "assets/images/key-fill.png",
      name: "Password",
    ),
    ImageData(
      imagePath: "assets/images/pin-fill.png",
      name: "Pin",
      type: "pin",
    ),
    ImageData(
      imagePath: "assets/images/global-fill.png",
      name: "Website",
    ),
    ImageData(
      imagePath: "assets/images/email-fill.png",
      name: "Email",
    ),
    ImageData(
      imagePath: "assets/images/number.png",
      name: "Number",
      type: "number",
    ),
    ImageData(
        imagePath: "assets/images/calendar-fill.png",
        name: "Date[MM/YY]",
        type: "date"),
    ImageData(
        imagePath: "assets/images/calendar-fill.png",
        name: "Date[YYYY/MM/DD]",
        type: "date"),
    ImageData(
        imagePath: "assets/images/paragraph.png",
        name: "Multiline-text",
        type: "multiline-text"),

    ImageData(
      imagePath: "assets/images/text.png",
      name: "Text",
    ),
    ImageData(
        imagePath: "assets/images/time-fill.png", name: "Time", type: "time"),

    ImageData(
      imagePath: "assets/images/map-pin-2-fill.png",
      name: "Address",
    ),
    // ImageData(
    //   imagePath: "assets/images/group.png",
    //   name: "Attachment",
    // ),
  ];

  List<String> categoryDataList = [
    "Personal",
    "Work",
    "Finance",
    "Shopping",
    "Social",
    "Educational",
    "Private",
    "Others",
  ];
  // void reorderData(int oldIndex, int newIndex) {
  //   setState(() {
  //     if (newIndex > oldIndex) {
  //       newIndex -= 1;
  //     }
  //     final items = imageDataList.removeAt(oldIndex);
  //     imageDataList.insert(newIndex, items);
  //   });
  // }
  Future<File> _createFileFromString(
      {required String encodedStr, required String ext}) async {
    Uint8List bytes = base64.decode(encodedStr);
    String dir = (await getApplicationDocumentsDirectory()).path;
    File file = File("$dir/${DateTime.now().millisecondsSinceEpoch}$ext");
    await file.writeAsBytes(bytes);
    return file;
  }

  String _getFileExtension({required String name}) {
    switch (name) {
      case 'P':
        return '.svg';
      case '/':
        return '.jpg';
      // case '/':
      //   return '.jpg';
      default:
        return '.png';
    }
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      if (widget.isEdit) {
        if (widget.login!.profilePic != null &&
            widget.login!.profilePic!.isNotEmpty) {
          _selectedImage = await _createFileFromString(
            encodedStr: widget.login!.profilePic!,
            ext: _getFileExtension(name: widget.login!.profilePic![0]),
          );
        }
        _titleController = TextEditingController(text: widget.login!.title);
        _isFavourite = widget.login!.isFavorite;
        selectedCategory = widget.login!.category ?? 'Personal';
        dragDropDataList = [
          DragDropData(
              controller: TextEditingController(
                text: widget.login!.website,
              ),
              imagePath: "assets/images/global-fill.png",
              name: "Website",
              editable: false),
          DragDropData(
              controller: TextEditingController(
                text: widget.login!.userName,
              ),
              imagePath: "assets/images/user-fill.png",
              name: "Username",
              editable: false),
          DragDropData(
              controller: TextEditingController(
                text: widget.login!.password,
              ),
              imagePath: "assets/images/key-fill.png",
              name: "Password",
              type: "password",
              isObscure: true,
              editable: false),
        ];
        for (var element in widget.login!.additionalFields!) {
          final d = DragDropData.fromJson(jsonEncode(element));

          dragDropDataList.add(d);
        }
        setState(() {});
      }
    });

    // WidgetsBinding.instance.addPostFrameCallback((_) async {
    // await _initImages();
    // });
    if (!widget.isEdit) {
      dragDropDataList = [
        DragDropData(
            controller: TextEditingController(),
            imagePath: "assets/images/global-fill.png",
            name: "Website",
            editable: false),
        DragDropData(
            controller: TextEditingController(),
            imagePath: "assets/images/user-fill.png",
            name: "Username",
            editable: false),
        DragDropData(
            controller: TextEditingController(),
            imagePath: "assets/images/key-fill.png",
            name: "Password",
            type: "password",
            isObscure: true,
            editable: false),
      ];
    }
  }

  String? selectedCategory;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: const Color.fromARGB(255, 250, 249, 249),
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 250, 249, 249),
        elevation: 0,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios,
            size: 16,
            color: Color(0xFF585858),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: const Text(
          'New Login',
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.bold,
            color: Color(0xFF585858),
            fontFamily: 'Poppins',
          ),
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // Top section with gradient background
                Center(
                  child: SizedBox(
                    height: 96,
                    width: 96,
                    child: IconButton(
                      onPressed: () {
                        _showLibraryDialog();
                      },
                      icon: _selectedImage != null
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(10.0),
                              child: _selectedImage!.path.endsWith(".svg")
                                  ? SvgPicture.file(
                                      _selectedImage!,
                                      height: 96,
                                      width: 96,
                                      fit: BoxFit.cover,
                                    )
                                  : Image.file(
                                      _selectedImage!,
                                      height: 96,
                                      width: 96,
                                      fit: BoxFit.cover,
                                    ),
                            )
                          : Image.asset('assets/images/file_upload.png'),
                    ),
                  ),
                ),

                FileUploadContainer(
                  fileList: fileList,
                ),
                const SizedBox(height: 10),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 48,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                          boxShadow: const [
                            BoxShadow(
                                color: Color(
                                  0xB3ECF4FF,
                                ),
                                blurRadius: 20,
                                offset: Offset(0, 4))
                          ],
                        ),
                        child: TextField(
                          controller: _titleController,
                          onChanged: (value) {
                            setState(() {});
                          },
                          decoration: const InputDecoration(
                            fillColor: Colors.white70,
                            filled: true,
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10, horizontal: 8),
                            border: InputBorder.none,
                            hintText: 'Title',
                            hintStyle: TextStyle(
                              fontSize: 14,
                              color: Color(0xFF9498AF),
                              fontFamily: 'Poppins',
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(width: 8),
                    InkWell(
                      onTap: () {
                        setState(() {
                          _isFavourite = !_isFavourite;
                        });
                      },
                      child: Container(
                        height: 48,
                        width: 48,
                        padding: const EdgeInsets.symmetric(
                            vertical: 10, horizontal: 8),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.white,
                          boxShadow: const [
                            BoxShadow(
                                color: Color(
                                  0xB3ECF4FF,
                                ),
                                blurRadius: 20,
                                offset: Offset(0, 4))
                          ],
                        ),
                        child: SvgPicture.asset(
                          'assets/icons/Star-Yellow.svg',
                          fit: BoxFit.none,
                          // ignore: deprecated_member_use
                          color: _isFavourite ? null : const Color(0xFFD1DBE7),
                        ),
                      ),
                    )
                  ],
                ),
                const SizedBox(height: 10),
                CategoryPickerExpansionTile(
                  categoryDataList: categoryDataList,
                  onSelect: (selectedItem) => selectedCategory = selectedItem,
                ),
                const SizedBox(height: 10),
                ReorderableListView(
                    shrinkWrap: true,
                    physics: const NeverScrollableScrollPhysics(),
                    onReorder: (oldIndex, newIndex) {
                      setState(() {
                        _reorderItems(oldIndex, newIndex);
                      });
                    },
                    children: List.generate(
                        dragDropDataList.length,
                        (index) => Container(
                              key: ValueKey(dragDropDataList[index]),
                              child: CustomAddDetailsField(
                                item: dragDropDataList[index],
                                onChanged: (p0) => setState(() {}),
                              ),
                            ))),
                const SizedBox(height: 130),
              ],
            ),
          ),
        ),
      ),
      bottomSheet: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            InkWell(
              onTap: () async {
                showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return CustomAddNewFieldDialogue(
                        itemList: addFiledList,
                        onSelect: (dragDropData) {
                          dragDropDataList.add(dragDropData);

                          setState(() {});
                        });
                  },
                );
              },
              child: SizedBox(
                height: 48,
                width: double.infinity,
                child: _addFieldButton(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.green,
                          border: Border.all(style: BorderStyle.none),
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: const Icon(
                          Icons.add,
                          color: Colors.white,
                          size: 20,
                        ),
                      ),
                      const SizedBox(width: 8),
                      Text(
                        'Add Field'.toUpperCase(),
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          color: kPrimaryColor,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: _titleController.text.isEmpty ||
                          dragDropDataList[0].controller.text.isEmpty ||
                          dragDropDataList[1].controller.text.isEmpty ||
                          dragDropDataList[2].controller.text.isEmpty
                      ? const Color(0XFFD1DBE7)
                      : const Color(0XFF2EBA0B),
                  padding: const EdgeInsets.symmetric(vertical: 16),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                ),
                onPressed: () async {
                  if (_titleController.text.isEmpty ||
                      dragDropDataList[0].controller.text.isEmpty ||
                      dragDropDataList[1].controller.text.isEmpty ||
                      dragDropDataList[2].controller.text.isEmpty) {
                  } else {
                    List<DragDropData> newFields = [];
                    // Handle Agree & Continue
                    for (var i = 3; i < dragDropDataList.length; i++) {
                      newFields.add(dragDropDataList[i]);
                    }
                    List<String> addFiles = [];
                    for (var element in fileList) {
                      addFiles
                          .add(base64Encode(File(element).readAsBytesSync()));
                    }
                    setState(() {});
                    LoginModel login = LoginModel(
                        id: DateTime.now().toString(),
                        title: _titleController.text,
                        website: dragDropDataList[0].controller.text,
                        userName: dragDropDataList[1].controller.text,
                        password: dragDropDataList[2].controller.text,
                        category: selectedCategory,
                        profilePic: _selectedImage == null
                            ? ''
                            : base64Encode(_selectedImage!.readAsBytesSync()),
                        icons: addFiles,
                        // icons: _selectedImage == null
                        //     ? []
                        //     : [''].addAllT(fileList
                        //         .map((e) =>
                        //             base64Encode(File(e).readAsBytesSync()))
                        //         .toList()),
                        isFavorite: _isFavourite,
                        additionalFields: newFields
                            .map((e) => {
                                  "name": e.name,
                                  "controller": e.controller.text,
                                  "type": e.type,
                                  "editable": e.editable,
                                  "imagePath": e.imagePath,
                                })
                            .toList());
                    if (widget.isEdit) {
                      Boxes.getLogInsData().putAt(widget.boxItemIndex!, login);
                    } else {
                      Boxes.getLogInsData().add(login);
                    }

                    login.save();
                    if (widget.isEdit) {
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    } else {
                      Get.back();
                    }
                  }
                },
                child: const Text(
                  'Save & Continue',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool isEditing = false;
  // List<Widget> _buildFieldListItems() {
  //   return dragDropDataList
  //       .map((item) => Container(
  //             key: ValueKey(item),
  //             child: CustomAddDetailsField(item: item),
  //           ))
  //       .toList();
  // }

  void _reorderItems(int oldIndex, int newIndex) {
    if (newIndex > oldIndex) {
      newIndex -= 1;
    }
    final DragDropData item = dragDropDataList.removeAt(oldIndex);
    dragDropDataList.insert(newIndex, item);
  }

  // generateTag() {
  //   return ListView.builder(
  //     itemCount: addFiledList.length,
  //     itemBuilder: (BuildContext context, int index) {
  //       return FilterChip(
  //         labelStyle: const TextStyle(color: Color(0xFF6E8A9E)),
  //         label: Text(addFiledList[index].name,
  //             style: const TextStyle(
  //               fontSize: 14,
  //               color: Color(0xFF6E8A9E),
  //               fontFamily: 'Poppins',
  //             )),
  //         onSelected: (bool selected) {
  //           setState(() {
  //             _addItem(addFiledList[index].name);

  //             Navigator.of(context).pop(addFiledList[index].name);
  //           });
  //         },
  //         shape: RoundedRectangleBorder(
  //           borderRadius: BorderRadius.circular(24.0),
  //           side: const BorderSide(
  //             color: Color(0xFFA7C3D7),
  //             width: 0.5,
  //           ),
  //         ),
  //       );
  //     },
  //   );

  //   // tags.map((imageDataList) => getChip(imageDataList)).toList();
  // }

  // getChip(name) {
  //   return FilterChip(
  //     labelStyle: const TextStyle(color: Color(0xFF6E8A9E)),
  //     label: Text("# $name",
  //         style: const TextStyle(
  //           fontSize: 14,
  //           color: Color(0xFF6E8A9E),
  //           fontFamily: 'Poppins',
  //         )),
  //     onSelected: (bool selected) {
  //       setState(() {
  //         _addItem(name);

  //         Navigator.of(context).pop(name);
  //       });
  //     },
  //     shape: RoundedRectangleBorder(
  //       borderRadius: BorderRadius.circular(24.0),
  //       side: const BorderSide(
  //         color: Color(0xFFA7C3D7),
  //         width: 0.5,
  //       ),
  //     ),
  //   );
  // }
}

// // Custom dropdown text field (reusing from previous example)
// class CustomDropdownTextField extends StatelessWidget {
//   final String selectedValue;
//   final void Function(String?) onChanged;
//   final List<String> options;

//   const CustomDropdownTextField({
//     super.key,
//     required this.selectedValue,
//     required this.onChanged,
//     required this.options,
//   });

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       decoration: BoxDecoration(
//         border: Border.all(color: Colors.grey),
//         borderRadius: BorderRadius.circular(5),
//       ),
//       child: DropdownButtonHideUnderline(
//         child: TextField(
//           controller: TextEditingController(text: selectedValue),
//           readOnly: true,
//           decoration: InputDecoration(
//             border: InputBorder.none,
//             suffixIcon: DropdownButton<String>(
//               value: selectedValue,
//               items: [
//                 // Static first option (disabled):
//                 const DropdownMenuItem<String>(
//                   value: 'Category',
//                   enabled: false,
//                   child: Text('Category'),
//                 ),
//                 ...options.map((String value) {
//                   return DropdownMenuItem<String>(
//                     value: value,
//                     child: Text(value),
//                   );
//                 }),
//               ],
//               onChanged: onChanged,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }

// class CustomInputField extends StatelessWidget {
//   final String label;

//   const CustomInputField({Key? key, required this.label}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return TextField(
//       decoration: InputDecoration(
//         labelText: label,
//         border: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(8),
//           borderSide: BorderSide(color: Colors.grey.shade300),
//         ),
//         enabledBorder: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(8),
//           borderSide: BorderSide(color: Colors.grey.shade300),
//         ),
//         focusedBorder: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(8),
//           borderSide: const BorderSide(color: Colors.green),
//         ),
//       ),
//     );
//   }
// }

// class CustomDropdownButtonFormField extends StatelessWidget {
//   final String label;
//   final List<String> items;
//   final ValueChanged<String?> onChanged;

//   const CustomDropdownButtonFormField({
//     Key? key,
//     required this.label,
//     required this.items,
//     required this.onChanged,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return DropdownButtonFormField<String>(
//       decoration: InputDecoration(
//         labelText: label,
//         border: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(8),
//           borderSide: BorderSide(color: Colors.grey.shade300),
//         ),
//         enabledBorder: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(8),
//           borderSide: BorderSide(color: Colors.grey.shade300),
//         ),
//         focusedBorder: OutlineInputBorder(
//           borderRadius: BorderRadius.circular(8),
//           borderSide: const BorderSide(color: Colors.green),
//         ),
//       ),
//       value: items.first,
//       onChanged: onChanged,
//       items: items.map<DropdownMenuItem<String>>((String value) {
//         return DropdownMenuItem<String>(
//           value: value,
//           child: Text(value),
//         );
//       }).toList(),
//     );
//   }
// }

// class Item {
//   Item({
//     required this.expandedValue,
//     required this.headerValue,
//     this.isExpanded = false,
//   });

//   String expandedValue;
//   String headerValue;
//   bool isExpanded;
// }
