import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gb_private/models/login/login.dart';
import 'package:gb_private/screens/widgets/login_card.dart';
import 'package:gb_private/screens/widgets/login_gird.dart';

class LoginListPage extends StatefulWidget {
  final List<LoginModel> logins;

  const LoginListPage({Key? key, required this.logins}) : super(key: key);

  @override
  State<LoginListPage> createState() => _LoginListPageState();
}

class _LoginListPageState extends State<LoginListPage>
    with SingleTickerProviderStateMixin {
  bool isListViewSelected = true;
  bool isGridViewSelected = false;
  bool isFilterSelected = false;
  // List<LoginModel> loginsList = [];
  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    tabController!.addListener(tabChange);
    cardCount = widget.logins.length;

    super.initState();
  }

  int cardCount = 0;

  tabChange() {
    setState(() {
      cardCount = tabController!.index == 0
          ? widget.logins.length
          : widget.logins
              .where((element) => element.isFavorite)
              .toList()
              .length;
    });
  }

  String searchVal = "";

  TabController? tabController;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text(
              'Logins',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.w500,
                fontFamily: "Poppins",
                color: Color(0xff2e2e2e),
              ),
            ),
            const SizedBox(width: 6),
            Container(
              height: 24,
              width: 24,
              decoration: BoxDecoration(
                  color: const Color(0xFF6E8A9E),
                  borderRadius: BorderRadius.circular(50)),
              child: Center(
                child: Text(
                  '$cardCount',
                  style: const TextStyle(color: Colors.white, fontSize: 16),
                ),
              ),
            )
          ],
        ),
        // actions: const [
        //   Padding(
        //     padding: EdgeInsets.only(right: 8),
        //     child: SizedBox(
        //       height: 35,
        //       width: 35,
        //       child: CircleAvatar(
        //         backgroundImage: AssetImage(
        //           'assets/images/user-fill.png',
        //         ),
        //       ),
        //     ),
        //   )
        // ],
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(kToolbarHeight),
          child: TabBar(
            controller: tabController,
            tabAlignment: TabAlignment.start,
            isScrollable: true, // Make tabs scrollable
            tabs: const [
              Tab(
                child: Text(
                  'All',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              Tab(
                child: Text(
                  'Favorites',
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: const Color(0XFFD1DDED), width: 1),
                borderRadius: BorderRadius.circular(8),
              ),
              child: TextField(
                onChanged: (value) {
                  setState(() {
                    searchVal = value;
                  });
                },
                decoration: InputDecoration(
                  hintText: 'Search here',
                  hintStyle: const TextStyle(
                    fontFamily: 'Poppins',
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color(0xff9498AF),
                  ),
                  suffixIcon: Transform.scale(
                    scale: 0.5,
                    child: SvgPicture.asset(
                      'assets/icons/Search.svg',
                      width: 10,
                    ),
                  ),
                  contentPadding: const EdgeInsets.symmetric(horizontal: 10),
                  border: OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ),
            const SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  'My Logins',
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                      fontFamily: "Poppins-Regular"),
                ),
                Row(
                  children: [
                    IconButton(
                      icon: SvgPicture.asset(
                        'assets/icons/list-check.svg',
                        colorFilter: ColorFilter.mode(
                          isListViewSelected ? Colors.green : Colors.grey,
                          BlendMode.srcIn,
                        ),
                      ),
                      onPressed: () {
                        // Handle list view button pressed
                        setState(() {
                          isListViewSelected = true;
                          isGridViewSelected = false;
                          isFilterSelected = false;
                        });
                      },
                    ),
                    IconButton(
                      icon: SvgPicture.asset(
                        'assets/icons/Grid-view.svg',
                        colorFilter: ColorFilter.mode(
                          isGridViewSelected ? Colors.green : Colors.grey,
                          BlendMode.srcIn,
                        ),
                      ),
                      onPressed: () {
                        // Handle grid view button pressed
                        setState(() {
                          isListViewSelected = false;
                          isGridViewSelected = true;
                          isFilterSelected = false;
                        });
                      },
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.filter_list,
                        color: isFilterSelected ? Colors.green : Colors.grey,
                      ),
                      onPressed: () {
                        // Handle filter button pressed
                        setState(() {
                          isListViewSelected = false;
                          isGridViewSelected = false;
                          isFilterSelected = true;
                        });
                      },
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 16),
            // Adjust spacing as needed
            Expanded(
              child: isGridViewSelected
                  ? TabBarView(
                      controller: tabController,
                      children: [
                        LoginGridView(
                          logins: searchVal.isEmpty
                              ? widget.logins
                              : widget.logins
                                  .where((element) => element.title
                                      .toLowerCase()
                                      .contains(searchVal.toLowerCase()))
                                  .toList(),
                        ),
                        LoginGridView(
                          logins: searchVal.isEmpty
                              ? widget.logins
                                  .where((element) => element.isFavorite)
                                  .toList()
                              : widget.logins
                                  .where((element) =>
                                      element.isFavorite &&
                                      element.title
                                          .toLowerCase()
                                          .contains(searchVal.toLowerCase()))
                                  .toList(),
                        ),
                      ],
                    )
                  : TabBarView(
                      controller: tabController,
                      children: [
                        LoginCard(
                          logins: searchVal.isEmpty
                              ? widget.logins
                              : widget.logins
                                  .where((element) => element.title
                                      .toLowerCase()
                                      .contains(searchVal.toLowerCase()))
                                  .toList(),
                        ),
                        LoginCard(
                          logins: searchVal.isEmpty
                              ? widget.logins
                                  .where((element) => element.isFavorite)
                                  .toList()
                              : widget.logins
                                  .where((element) =>
                                      element.isFavorite &&
                                      element.title
                                          .toLowerCase()
                                          .contains(searchVal.toLowerCase()))
                                  .toList(),
                        ),
                      ],
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
