import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gb_private/main.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:syncfusion_flutter_sliders/sliders.dart';

class OpenCameraPage extends StatefulWidget {
  const OpenCameraPage({super.key});

  @override
  State<OpenCameraPage> createState() => _OpenCameraPageState();
}

class _OpenCameraPageState extends State<OpenCameraPage> {
  late CameraController _controller;
  bool isCapturing = false;

  // for switch camera
  int _selectedCameraIndex = 0;
  bool _isFrontCamera = false;
  bool _isFlashOn = false;
  Offset? _focusPoint;
  double _currentZoom = 1.0;
  File? _capturedImage;

  // AssetsAudioPlayer audioPlayer = AssetsAudioPlayer

  @override
  void initState() {
    super.initState();
    _controller = CameraController(cameras.first, ResolutionPreset.max);
    _controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    }).catchError((Object error) {
      if (error is CameraException) {
        switch (error.code) {
          case 'CameraAccessDenied':
            if (kDebugMode) {
              print('Access was denied');
            }
            break;
          default:
            if (kDebugMode) {
              print(error.description);
            }
            break;
        }
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _toggleFlashLight() {
    if (_isFlashOn) {
      _controller.setFlashMode(FlashMode.off);
      setState(() {
        _isFlashOn = false;
      });
    } else {
      _controller.setFlashMode(FlashMode.torch);
      setState(() {
        _isFlashOn = true;
      });
    }
  }

  void _switchCamera() async {
    //Dispose the current controller to release the camera resource
    await _controller.dispose();
    // Increment or reset the selected camera index
    _selectedCameraIndex = (_selectedCameraIndex + 1) % cameras.length;

    // Initialize the new camera
    _initCamera(_selectedCameraIndex);
  }

  Future<void> _initCamera(int cameraIndex) async {
    _controller = CameraController(cameras[cameraIndex], ResolutionPreset.max);
    try {
      await _controller.initialize();
      setState(() {
        if (cameraIndex == 0) {
          _isFrontCamera = false;
        } else {
          _isFrontCamera = true;
        }
      });
    } catch (e) {
      if (kDebugMode) {
        print('Error message: $e');
      }
    }

    if (mounted) {
      setState(() {});
    }
  }

  void _capturePhoto() async {
    if (!_controller.value.isInitialized) {
      return;
    }
    final Directory appDir =
        await path_provider.getApplicationSupportDirectory();
    final String capturePath = path.join(appDir.path, '${DateTime.now()}');
    if (_controller.value.isTakingPicture) {
      return;
    }
    try {
      setState(() {
        isCapturing = true;
      });
      final XFile capturedImage = await _controller.takePicture();
      String imagePath = capturedImage.path;
      if (kDebugMode) {
        print(imagePath);
      }

      final String filePath =
          '$capturePath/${DateTime.now().millisecondsSinceEpoch}.jpg';
      _capturedImage = File(capturedImage.path);
      _capturedImage!.renameSync(filePath);
    } catch (error) {
      if (kDebugMode) {
        print('Error capturing photo: $error');
      }
    } finally {
      isCapturing = false;
    }
  }

  void _zoomCamera(double value) {
    setState(() {
      _currentZoom = value;
      _controller.setZoomLevel(value);
    });
  }

  Future<void> _setFocusPoint(Offset point) async {
    if (_controller.value.isInitialized) {
      try {
        final double x = point.dx.clamp(0.0, 1.0);
        final double y = point.dx.clamp(0.0, 1.0);
        await _controller.setFocusPoint(Offset(x, y));
        await _controller.setFocusMode(FocusMode.auto);
        setState(() {
          _focusPoint = Offset(x, y);
        });
        // Reset _focus point after a short delay to remove the square
        await Future.delayed(const Duration(seconds: 2));
        setState(() {
          _focusPoint = null;
        });
      } catch (error) {
        if (kDebugMode) {
          print('Failed to set focus: $error');
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: LayoutBuilder(
        builder: (context, constraints) => Stack(
          children: [
            Positioned(
                top: 0,
                left: 0,
                right: 0,
                child: Container(
                  height: 50,
                  decoration: const BoxDecoration(color: Colors.black),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                          onTap: () {
                            _toggleFlashLight();
                          },
                          child: _isFlashOn
                              ? const Icon(
                                  Icons.flash_auto,
                                  color: Colors.white,
                                )
                              : const Icon(
                                  Icons.flash_off,
                                  color: Colors.white,
                                )),
                      GestureDetector(
                          onTap: () {},
                          child: const Icon(
                            Icons.qr_code_scanner,
                            color: Colors.white,
                          ))
                    ],
                  ),
                )),
            Positioned.fill(
                top: 50,
                bottom: _isFrontCamera ? 150 : 0,
                child: AspectRatio(
                  aspectRatio: 2,
                  child: GestureDetector(
                      onTapDown: (details) {
                        final Offset tapPosition = details.localPosition;
                        final Offset relativeTapPosition = Offset(
                            tapPosition.dx / constraints.maxWidth,
                            tapPosition.dy / constraints.maxHeight);
                        _setFocusPoint(relativeTapPosition);
                      },
                      child: CameraPreview(_controller)),
                )),
            Positioned(
              top: 50,
              right: 10,
              child: SfSlider.vertical(
                  min: 1.0,
                  max: 5.0,
                  activeColor: Colors.white,
                  value: _currentZoom,
                  onChanged: (value) {
                    setState(() {
                      _zoomCamera(value);
                    });
                  }),
            ),
            if (_focusPoint != null)
              Positioned.fill(
                top: 50,
                child: Align(
                  alignment: Alignment(
                      _focusPoint!.dx * 2 - 1, _focusPoint!.dy * 2 - 1),
                  child: Container(
                    height: 80,
                    width: 80,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 2)),
                  ),
                ),
              ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 150,
                decoration: BoxDecoration(
                    color: _isFrontCamera ? Colors.black : Colors.black45),
                child: Column(
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                            child: Center(
                              child: Text(
                                'Video',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Center(
                              child: Text(
                                'Photo',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Center(
                              child: Text(
                                'Pro Mode',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Row(
                            children: [
                              Expanded(
                                  child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  _capturedImage != null
                                      ? SizedBox(
                                          width: 50,
                                          height: 50,
                                          child: Image.file(
                                            _capturedImage!,
                                            fit: BoxFit.cover,
                                          ),
                                        )
                                      : Container()
                                ],
                              )),
                              Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    _capturePhoto();
                                  },
                                  child: Center(
                                    child: Container(
                                      height: 70,
                                      width: 70,
                                      decoration: BoxDecoration(
                                          color: Colors.transparent,
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          border: Border.all(
                                              width: 4,
                                              color: Colors.white,
                                              style: BorderStyle.solid)),
                                    ),
                                  ),
                                ),
                              ),
                              Expanded(
                                  child: GestureDetector(
                                onTap: () {
                                  _switchCamera();
                                },
                                child: const Icon(
                                  Icons.cameraswitch_sharp,
                                  color: Colors.white,
                                  size: 40,
                                ),
                              ))
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
